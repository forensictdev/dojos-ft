from flask import Flask, render_template
from flask_basicauth import BasicAuth

app = Flask(__name__)

app.config['BASIC_AUTH_USERNAME'] = 'FT'
app.config['BASIC_AUTH_PASSWORD'] = 'Dojos'
basic_auth = BasicAuth(app)


@app.route('/')
def index():
    return render_template("index.html")


@app.route('/lab1')
def lab1():
    #message = ''
    return render_template("lab1.html")

@app.route('/lab2')
def lab2():
    #message = ''
    return render_template("lab2.html")


@app.route('/lab3')
def lab3():
    #message = ''
    return render_template("lab3.html")


@app.route('/lab4')
def lab4():
    #message = ''
    return render_template("lab4.html")

@app.route('/lab5')
@basic_auth.required
def lab5():
    message = 'Has accedido correctamente al recurso. Felicitaciones :)'
    return render_template("lab5.html", message=message)


app.run(host='0.0.0.0', port=8080)
