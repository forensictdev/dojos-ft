# FUNDAMENTOS DE HTTP                                                             <img src="images/ft_technology-dojos.png" alt="FT_TECHNOLOGY_DOJOS" style="zoom:150%;" />

El Protocolo de transferencia de hipertexto (HTTP) es un protocolo a nivel de aplicación para sistemas de información hipermedia distribuidos y colaborativos. Esta es la base para la comunicación de datos para la World Wide Web (es decir, Internet) desde 1990. HTTP es un protocolo genérico y sin estado que se puede utilizar para otros fines, además de utilizar extensiones de sus métodos de solicitud, códigos de error y cabeceras.

Básicamente, HTTP es un protocolo de comunicación basado en TCP / IP, que se utiliza para entregar datos (archivos HTML, archivos de imagen, resultados de consultas, etc.) en la World Wide Web. El puerto predeterminado es TCP 80, pero también se pueden usar otros puertos. Proporciona una forma estandarizada para que los dispositivos se comuniquen entre sí. El estándar HTTP especifica cómo se construirán y enviarán los datos de solicitud de los clientes al servidor, y cómo los servidores responderán a estas solicitudes.

## Características básicas

Hay tres características básicas que hacen de HTTP un protocolo simple pero poderoso:

* HTTP no tiene conexión: el cliente HTTP, un navegador por ejemplo, inicia una solicitud HTTP y, después de realizar una solicitud, el cliente espera la respuesta. El servidor procesa la solicitud y envía una respuesta, después de lo cual el cliente desconecta la conexión. Por lo tanto, el cliente y el servidor se conocen entre sí solo durante la solicitud y la respuesta actuales. Se realizan más solicitudes en la nueva conexión, ya que el cliente y el servidor son nuevos entre sí.
* HTTP es independiente del medio: es decir, cualquier tipo de datos puede ser enviado por HTTP siempre que tanto el cliente como el servidor sepan cómo manejar el contenido de los datos. Es necesario que el cliente y el servidor especifiquen el tipo de contenido utilizando el tipo MIME apropiado.
* HTTP no tiene estado: como se mencionó anteriormente, HTTP no tiene conexión y es un resultado directo de que HTTP es un protocolo sin estado. El servidor y el cliente se conocen entre sí solo durante una solicitud actual. Después, ambos se olvidan el uno del otro. Debido a esta naturaleza del protocolo, ni el cliente ni el navegador pueden retener información entre diferentes solicitudes en las páginas web.

> HTTP / 1.0 usa una nueva conexión para cada intercambio de solicitud / respuesta, mientras que la conexión HTTP / 1.1 se puede usar para uno o más intercambios de solicitud / respuesta.



## Arquitectura básica

El siguiente diagrama muestra una arquitectura muy básica de una aplicación web y describe dónde se encuentra HTTP:

![Arquitectura Básica](images/cgiarch.gif)



El protocolo HTTP es un protocolo de solicitud / respuesta basado en la arquitectura basada en cliente / servidor donde los navegadores web, robots y motores de búsqueda, etc. actúan como clientes HTTP, y el servidor web actúa como servidor.

### Cliente

El cliente HTTP envía una solicitud al servidor en forma de método de solicitud, URI y versión de protocolo, seguida de un mensaje similar a MIME que contiene modificadores de solicitud, información del cliente y posible contenido del cuerpo a través de una conexión TCP / IP.

### Servidor 

El servidor HTTP responde con una línea de estado, que incluye la versión del protocolo del mensaje y un código de éxito o error, seguido de un mensaje similar a MIME que contiene información del servidor, metainformación de la entidad y posible contenido del cuerpo de la entidad.



## Parámetros HTTP

Listaremos algunos de los parámetros importantes del protocolo HTTP con el fin de ser identificados dentro de una comunicación cliente y servidor usando el protocolo.

### Versión HTTP 

HTTP utiliza un esquema de numeración <mayor>. <menor> para indicar versiones del protocolo. La versión de un mensaje HTTP se indica mediante un campo Versión HTTP en la primera línea. Aquí está la sintaxis general para especificar el número de versión HTTP:

```http
HTTP-Version   = "HTTP" "/" 1*DIGIT "." 1*DIGIT
```

Ejemplo:

```pseudocode
HTTP/1.0

ó

HTTP/1.1
```

### Identificadores Uniformes de Recursos (URI)

Los identificadores uniformes de recursos (URI) tienen un formato simple, una cadena que no distingue entre mayúsculas y minúsculas que contiene el nombre, la ubicación, etc. para identificar un recurso, por ejemplo, un sitio web, un servicio web, etc. Una sintaxis general de URI utilizada para HTTP es la siguiente:

```pseudocode
URI = "http:" "//" host [ ":" port ] [ abs_path [ "?" query ]]
```

Aquí, si el **puerto** está vacío o no se proporciona, se asume el puerto 80 para HTTP y un abs_path vacío es equivalente a un abs_path de "/". Los caracteres distintos a los de los conjuntos reservados e inseguros son equivalentes a su codificación ""% "HEX HEX".

Ejemplos:

```http
http://ft.com:80/~smith/home.html
http://FT.com/%7Esmith/home.html
http://FT.com:/%7esmith/home.html
```

### Formatos de Fecha y Hora

Todas las marcas de fecha / hora HTTP **DEBEN** estar representadas en la hora media de Greenwich (GMT), sin excepción. Las aplicaciones HTTP pueden utilizar cualquiera de las siguientes tres representaciones de marcas de fecha / hora:

```http
Sun, 06 Nov 1994 08:49:37 GMT  ; RFC 822, updated by RFC 1123
Sunday, 06-Nov-94 08:49:37 GMT ; RFC 850, obsoleted by RFC 1036
Sun Nov  6 08:49:37 1994       ; ANSI C's asctime() format
```

### Conjunto de caracteres

Se utilizan los conjuntos de caracteres para especificar el esquema de caracteres que prefiere el cliente. Se pueden enumerar varios conjuntos de caracteres separados por comas. Si no se especifica un valor, el valor predeterminado es US-ASCII.

Ejemplo:

```pseudocode
US-ASCII

ó

ISO-8859-1

ó 

ISO-8859-7
```

### Codificaciones de contenido

Un valor de codificación de contenido indica que se ha utilizado un algoritmo de codificación específico para transformar el contenido antes de pasarlo por la red. La codificación de contenido se utiliza principalmente para permitir que un documento se comprima o se transforme de manera útil sin perder la identidad.

Todos los valores de codificación de contenido no distinguen entre mayúsculas y minúsculas. HTTP / 1.1 utiliza valores de codificación de contenido en los campos de cabecera Accept-Encoding y Content-Encoding.

Ejemplo:

```pseudocode
Accept-encoding: gzip

ó

Accept-encoding: compress

ó 

Accept-encoding: deflate
```

### Tipos de medios

HTTP utiliza tipos de medios de Internet en los campos de cabecera **Content-Type** y **Accept** para proporcionar escritura de datos abierta y extensible y negociación de tipos. Todos los valores de tipo de medios están registrados con la Autoridad de números asignados de Internet (IANA). La sintaxis general para especificar el tipo de medio es la siguiente:

```http
media-type     = type "/" subtype *( ";" parameter )
```

Los nombres de atributo de tipo, subtipo y parámetro no distinguen entre mayúsculas y minúsculas.

Ejemplos:

```http
Accept: image/gif
```

### Etiquetas de idioma

HTTP utiliza etiquetas de idioma dentro de los campos Accept-Language y Content-Language. Una etiqueta de idioma se compone de una o más partes: una etiqueta de idioma principal y una serie posiblemente vacía de subetiquetas:

```http
language-tag  = primary-tag *( "-" subtag )
```

No se permiten espacios en blanco dentro de la etiqueta y todas las etiquetas no distinguen entre mayúsculas y minúsculas.

Ejemplo:

```http
en, en-US, en-cockney, i-cherokee, x-pig-latin
```

donde cualquier etiqueta principal de dos letras es una abreviatura de idioma ISO-639 y cualquier subetiqueta inicial de dos letras es un código de país ISO-3166.



## Mensajes HTTP                                                               <img src="images/ft_technology-dojos.png" alt="FT_TECHNOLOGY_DOJOS" style="zoom:150%;" />

HTTP se basa en el modelo de arquitectura cliente-servidor y un protocolo de solicitud / respuesta sin estado que opera mediante el intercambio de mensajes a través de una conexión TCP / IP confiable.

Un "cliente" HTTP es un programa (navegador web o cualquier otro cliente) que establece una conexión a un servidor con el propósito de enviar uno o más mensajes de solicitud HTTP. Un "servidor" HTTP es un programa (generalmente un servidor web como Apache Web Server o Internet Information Services IIS, etc.) que acepta conexiones para atender solicitudes HTTP mediante el envío de mensajes de respuesta HTTP.

HTTP utiliza el identificador uniforme de recursos (**URI**) para identificar un recurso determinado y establecer una conexión. Una vez que se establece la conexión, los mensajes HTTP se pasan en un formato similar al utilizado por el correo de Internet **[RFC5322**] y las Extensiones de correo de Internet multipropósito (**MIME**) [**RFC2045**]. Estos mensajes incluyen solicitudes de cliente a servidor y respuestas de servidor a cliente que tendrán el siguiente formato:

```http
 HTTP-message   = <Request> | <Response> ; HTTP/1.1 messages
```

Las solicitudes HTTP y las respuestas HTTP utilizan un formato de mensaje genérico de **RFC 822** para transferir los datos necesarios. Este formato de mensaje genérico consta de los siguientes cuatro elementos.

```pseudocode
* Una línea de inicio.
* Cero o más campos de cabeceras seguidos por CRLF.
* Una línea vacía. (Es decir, una línea sin nada que preceda el CRLF) indicando el final de las cabeceras.
* Opcionalmente un cuerpo del mensaje.
```

### Línea de inicio del mensaje

Una línea de inicio (start-line) tiene el siguiente formato genérico:

```pseudocode
start-line = Request-Line | Status-Line
```

Analizaremos Request-Line y Status-Line mientras hablamos de los mensajes HTTP Request y HTTP Response respectivamente. Por ahora, veamos los ejemplos de la línea de inicio en caso de solicitud y respuesta:

```http
GET /hello.htm HTTP/1.1     (Esta línea de solicitud es enviada por el Cliente)

HTTP/1.1 200 OK             (Esta línea de estado es enviada por el Servidor)
```

### Cabeceras (Encabezados)

Los cabeceras HTTP proporcionan información necesaria sobre la solicitud o respuesta, o sobre el objeto enviado en el cuerpo del mensaje. Hay cuatro tipos de cabeceras en mensajes HTTP:

* **Cabecera general:** estas cabeceras tienen aplicabilidad general tanto para mensajes de solicitud como de respuesta.

* **Cabecera de solicitud:** estas cabeceras solo se aplican a los mensajes de solicitud.

* **Cabecera de respuesta:** estas cabeceras solo se aplican a los mensajes de respuesta.

* **Cabecera de entidad:** estas cabeceras definen metainformación sobre el cuerpo de la entidad o, si no hay ningún cuerpo presente, sobre el recurso identificado por la solicitud.

Todos las cabeceras mencionadas anteriormente siguen el mismo formato genérico y cada uno de los campos  consta de un nombre seguido de dos puntos (:) y el valor del campo de la siguiente manera:

```http
message-header = field-name ":" [ field-value ]
```

Los siguientes son ejemplos de cabeceras de mensajes http válidos:

```http
User-Agent: curl/7.16.3 libcurl/7.16.3 OpenSSL/0.9.7l zlib/1.2.3
Host: www.example.com
Accept-Language: en, mi
Date: Mon, 27 Jul 2009 12:28:53 GMT
Server: Apache
Last-Modified: Wed, 22 Jul 2009 19:15:56 GMT
ETag: "34aa387-d-1568eb00"
Accept-Ranges: bytes
Content-Length: 51
Vary: Accept-Encoding
Content-Type: text/plain
```

### Cuerpo del mensaje

La parte del cuerpo del mensaje es opcional para un mensaje HTTP, pero si está disponible, se usa para llevar el cuerpo de la entidad asociado con la solicitud o respuesta. Si el cuerpo de la entidad está asociado, normalmente las cabeceras **Content-Type** y **Content-Length** especifican la naturaleza del cuerpo asociado.

El cuerpo de un mensaje es el   que contiene los datos de la solicitud HTTP real (incluidos los datos del formulario y los cargados, etc.) y los datos de respuesta HTTP del servidor (incluidos archivos, imágenes, etc.). A continuación se muestra el contenido simple del cuerpo de un mensaje:

```html
<html>
   <body>
   
      <h1>Hello, World!</h1>
   
   </body>
</html>
```



## Peticiones HTTP                                                             <img src="images/ft_technology-dojos.png" alt="FT_TECHNOLOGY_DOJOS" style="zoom:150%;" />

Un cliente HTTP envía una solicitud HTTP a un servidor en forma de mensaje de solicitud que incluye el siguiente formato:

```pseudocode
* Una línea de inicio.

* Cero o más campos de cabeceras (Generales | Solicitud | Entidad) seguidos por CRLF.

* Una línea vacía. (Es decir, una línea sin nada que preceda el CRLF) indicando el final de las cabeceras.

* Opcionalmente un cuerpo del mensaje.
```

### Línea de solicitud (Request-Line)

Request-Line comienza con un token de método, seguido por Request-URI y la versión del protocolo, y termina con CRLF. Los elementos están separados por caracteres SP de espacio.

```pseudocode
Request-Line = Method SP Request-URI SP HTTP-Version CRLF
```

#### Método de solicitud

El método de solicitud indica el método a realizar en el recurso identificado por el URI de solicitud dado. El método distingue entre mayúsculas y minúsculas y siempre debe mencionarse en mayúsculas. La siguiente tabla enumera todos los métodos admitidos en HTTP / 1.1.

| N.S  | Método y Descripción                                         |
| ---- | ------------------------------------------------------------ |
| 1    | **GET** El método GET se usa para recuperar información del servidor dado usando un URI dado. Las solicitudes que utilizan GET solo deben recuperar datos y no deben tener ningún otro efecto en los datos. |
| 2    | **HEAD** Igual que GET, pero transfiere solo la línea de estado y la sección de cabecera. |
| 3    | **POST** Una solicitud POST se utiliza para enviar datos al servidor, por ejemplo, información del cliente, carga de archivos, etc. utilizando formularios HTML. |
| 4    | **PUT **Reemplaza todas las representaciones actuales del recurso de destino con el contenido cargado. |
| 5    | **DELETE **Elimina todas las representaciones actuales del recurso de destino proporcionadas por URI. |
| 6    | **CONNECT **Establece un túnel al servidor identificado por un URI determinado. |
| 7    | **OPTIONS **Describe las opciones de comunicación para el recurso de destino. |
| 8    | **TRACE **Realiza una prueba de bucle de mensajes junto con la ruta al recurso de destino. |

#### Petición URI (Request-URI)

El Request-URI es un identificador uniforme de recursos e identifica el recurso sobre el cual aplicar la solicitud. A continuación, se muestran los formularios más utilizados para especificar un URI:

```pseudocode
Request-URI = "*" | absoluteURI | abs_path | authority
```

| N.S  | Método y Descripción                                         |
| ---- | ------------------------------------------------------------ |
| 1    | El asterisco ***** se utiliza cuando una solicitud HTTP no se aplica a un recurso en particular, sino al servidor mismo, y solo se permite cuando el método utilizado no se aplica necesariamente a un recurso. Por ejemplo: **OPTIONS \* HTTP/1.1** |
| 2    | La URL absoluta se utiliza cuando se realiza una solicitud HTTP a un proxy. Se solicita al proxy que reenvíe la solicitud o el servicio desde un caché válido y devuelva la respuesta. Por ejemplo: **GET http://www.w3.org/pub/WWW/TheProject.html HTTP/1.1** |
| 3    | La forma más común de Request-URI es la que se usa para identificar un recurso en un servidor de origen o puerta de enlace. Por ejemplo, un cliente que desee recuperar un recurso directamente del servidor de origen crearía una conexión TCP al puerto 80 del host "www.w3.org" y enviaría las siguientes líneas:                                                                                                           **GET /pub/WWW/TheProject.html HTTP/1.1**                                                                                                 **Host:** www.w3.org                                                                                                                                            Tenga en cuenta que la ruta absoluta no puede estar vacía; si no hay ninguno presente en el URI original, DEBE darse como "/" (la raíz del servidor). |

#### Cabeceras de una solicitud

Los campos de cabecera de solicitud permiten al cliente pasar información adicional sobre la solicitud y sobre el cliente mismo al servidor. Estos campos actúan como modificadores de solicitud. Aquí hay una lista de algunos campos de cabecera de solicitud importantes que se pueden usar según el requisito:

- Accept-Charset
- Accept-Encoding
- Accept-Language
- Authorization
- Expect
- From
- Host
- If-Match
- If-Modified-Since
- If-None-Match
- If-Range
- If-Unmodified-Since
- Max-Forwards
- Proxy-Authorization
- Range
- Referer
- TE
- User-Agent

Se puede introducir campos personalizados en caso de que vaya a escribir su propio cliente y servidor web personalizados.

### Ejemplos de Mensajes de solicitud

Ahora unamos todo para formar una solicitud HTTP para recuperar la página hello.html desde el servidor.

```http
GET /hello.htm HTTP/1.1
User-Agent: Mozilla/4.0 (compatible; MSIE5.01; Windows NT)
Host: www.ft.com
Accept-Language: en-us
Accept-Encoding: gzip, deflate
Connection: Keep-Alive
```

Aquí no enviamos ningún dato de solicitud al servidor porque estamos obteniendo una página HTML sin formato del servidor. La conexión es un cabecera general y el resto de las cabeceras son de solicitud. El siguiente ejemplo muestra cómo enviar datos de formulario al servidor utilizando el cuerpo del mensaje de solicitud:

```http
POST /cgi-bin/process.cgi HTTP/1.1
User-Agent: python-requests (3.11)
Host: www.ft.com
Content-Type: application/x-www-form-urlencoded
Content-Length: length
Accept-Language: en-us
Accept-Encoding: gzip, deflate
Connection: Keep-Alive

licenseID=string&content=string&/paramsXML=string
```

Aquí, la URL proporcionada /cgi-bin/process.cgi se utilizará para procesar los datos pasados y, en consecuencia, se devolverá una respuesta. Aquí el tipo de contenido le dice al servidor que los datos pasados son datos de un formulario web simple y la longitud será la longitud real de los datos colocados en el cuerpo del mensaje. 

El siguiente ejemplo muestra cómo puede pasar XML simple a su servidor web:

```http
POST /cgi-bin/process.cgi HTTP/1.1
User-Agent: python-requests (3.11)
Host: www.ft.com
Content-Type: text/xml; charset=utf-8
Content-Length: length
Accept-Language: en-us
Accept-Encoding: gzip, deflate
Connection: Keep-Alive

<?xml version="1.0" encoding="utf-8"?>
<string xmlns="http://clearforest.com/">string</string>
```



## Respuestas HTTP                                                           <img src="images/ft_technology-dojos.png" alt="FT_TECHNOLOGY_DOJOS" style="zoom:150%;" />

Después de recibir e interpretar un mensaje de solicitud, un servidor responde con un mensaje de respuesta HTTP:

```pseudocode
* Una línea de estado.

* Cero o más campos de cabeceras (Generales | Respuesta| Entidad) seguidos por CRLF.

* Una línea vacía. (Es decir, una línea sin nada que preceda el CRLF) indicando el final de las cabeceras.

* Opcionalmente un cuerpo del mensaje.
```

### Línea de estado (Status-Line)

Una línea de estado consta de la versión del protocolo seguida de un código de estado numérico y su frase textual asociada. Los elementos están separados por caracteres SP de espacio.

```http
Status-Line = HTTP-Version SP Status-Code SP Reason-Phrase CRLF
```

#### Versión HTTP

Un servidor que admita HTTP versión 1.1 devolverá la siguiente información de versión:

```http
HTTP-Version = HTTP/1.1
```

#### Código de estado

El elemento Status-Code es un número entero de 3 dígitos donde el primer dígito del Status-Code define la clase de respuesta y los dos últimos dígitos no tienen ningún rol de categorización. Hay 5 valores para el primer dígito:

| N.S  | Código y Descripción                                         |
| ---- | ------------------------------------------------------------ |
| 1    | **1xx: Informativo** Significa que se recibió la solicitud y el proceso continúa. |
| 2    | **2xx: Éxito** Significa que la acción se recibió, comprendió y aceptó correctamente. |
| 3    | **3xx: Redirección** Significa que se deben realizar más acciones para completar la solicitud. |
| 4    | **4xx: Error del cliente** Significa que la solicitud contiene una sintaxis incorrecta o que no se puede cumplir. |
| 5    | **5xx: Error del servidor** Significa que el servidor no cumplió con una solicitud aparentemente válida. |

Los códigos de estado HTTP son extensibles y no se requieren aplicaciones HTTP para comprender el significado de todos los códigos de estado registrados.

#### Cabeceras de respuesta

Las cabeceras de respuesta permiten al servidor pasar información adicional sobre la respuesta que no se puede colocar en la línea de estado. Estos campos de cabecera brindan información sobre el servidor y sobre el acceso adicional al recurso identificado por el Request-URI.

- Accept-Ranges
- Age
- ETag
- Location
- Proxy-Authenticate
- Retry-After
- Server
- Vary
- WWW-Authenticate

Se pueden introducir campos personalizados en caso de que vaya a escribir su propio servidor y cliente web personalizado.

### Ejemplos de Mensajes de respuesta

Ahora unamos todo para formar una respuesta HTTP a una solicitud para obtener la página hello.html del servidor web.

```http
HTTP/1.1 200 OK
Date: Mon, 27 Jul 2019 12:28:53 GMT
Server: Apache/2.4.14
Last-Modified: Wed, 22 Jul 2019 19:15:56 GMT
Content-Length: 88
Content-Type: text/html
Connection: Closed
```

```html
<html>
<body>
<h1>Hello, World!</h1>
</body>
</html>
```

El siguiente ejemplo muestra un mensaje de respuesta HTTP que muestra una condición de error cuando el servidor web no pudo encontrar la página solicitada:

```http
HTTP/1.1 404 Not Found
Date: Sun, 18 Oct 2021 10:36:20 GMT
Server: Apache/2.4.14
Content-Length: 230
Connection: Closed
Content-Type: text/html; charset=iso-8859-1
```

```html
<!DOCTYPE HTML PUBLIC "-//IETF//DTD HTML 2.0//EN">
<html>
<head>
   <title>404 Not Found</title>
</head>
<body>
   <h1>Not Found</h1>
   <p>The requested URL /t.html was not found on this server.</p>
</body>
</html>
```

A continuación se muestra un ejemplo de mensaje de respuesta HTTP que muestra una condición de error cuando el servidor web encontró una versión HTTP incorrecta en la solicitud HTTP dada:

```http
HTTP/1.1 400 Bad Request
Date: Sun, 18 Oct 2012 10:36:20 GMT
Server: Apache/2.2.14 (Win32)
Content-Length: 230
Content-Type: text/html; charset=iso-8859-1
Connection: Closed
```

```html
<!DOCTYPE HTML PUBLIC "-//IETF//DTD HTML 2.0//EN">
<html>
<head>
   <title>400 Bad Request</title>
</head>
<body>
   <h1>Bad Request</h1>
   <p>Your browser sent a request that this server could not understand.</p>
   <p>The request line contained invalid characters following the protocol string.</p>
</body>
</html>
```



## Métodos HTTP <img src="images/ft_technology-dojos.png" alt="FT_TECHNOLOGY_DOJOS" style="zoom:150%;" />

El conjunto de métodos comunes para HTTP / 1.1 se define a continuación y este conjunto puede ampliarse según los requisitos. Estos nombres de métodos distinguen entre mayúsculas y minúsculas y deben utilizarse en mayúsculas.

| N.S  | Método y Descripción                                         |
| ---- | ------------------------------------------------------------ |
| 1    | **GET** El método GET se usa para recuperar información del servidor dado usando un URI dado. Las solicitudes que utilizan GET solo deben recuperar datos y no deben tener ningún otro efecto en los datos. |
| 2    | **HEAD** Igual que GET, pero transfiere solo la línea de estado y la sección de cabecera. |
| 3    | **POST** Una solicitud POST se utiliza para enviar datos al servidor, por ejemplo, información del cliente, carga de archivos, etc. utilizando formularios HTML. |
| 4    | **PUT **Reemplaza todas las representaciones actuales del recurso de destino con el contenido cargado. |
| 5    | **DELETE **Elimina todas las representaciones actuales del recurso de destino proporcionadas por URI. |
| 6    | **CONNECT **Establece un túnel al servidor identificado por un URI determinado. |
| 7    | **OPTIONS **Describe las opciones de comunicación para el recurso de destino. |
| 8    | **TRACE **Realiza una prueba de bucle de mensajes junto con la ruta al recurso de destino. |

### Método GET

Una solicitud GET recupera datos de un servidor web al especificar parámetros en la parte de la URL de la solicitud. Este es el método principal utilizado para la recuperación de documentos. El siguiente ejemplo utiliza el método GET para recuperar hello.html:

```http
GET /hello.html HTTP/1.1
User-Agent: python-requests(3.11)
Host: www.ft.com
Accept-Language: en-us
Accept-Encoding: gzip, deflate
Connection: Keep-Alive
```

La respuesta del servidor contra la solicitud GET anterior será la siguiente:

```http
HTTP/1.1 200 OK
Date: Mon, 27 Jul 2019 12:28:53 GMT
Server: Apache/2.4.14
Last-Modified: Wed, 22 Jul 2019 19:15:56 GMT
ETag: "34aa387-d-1568eb00"
Vary: Authorization,Accept
Accept-Ranges: bytes
Content-Length: 88
Content-Type: text/html
Connection: Closed
```

```html
<html>
<body>
<h1>Hello, World!</h1>
</body>
</html>
```

### Método HEAD

El método HEAD es funcionalmente similar a GET, excepto que el servidor responde con una línea de respuesta y cabeceras, pero sin cuerpo de entidad. El siguiente ejemplo utiliza el método HEAD para obtener información de cabecera sobre hello.html:

```http
HEAD /hello.html HTTP/1.1
User-Agent: python-requests(3.11)
Host: www.ft.com
Accept-Language: en-us
Accept-Encoding: gzip, deflate
Connection: Keep-Alive
```

La respuesta del servidor contra la solicitud HEAD anterior será la siguiente:

```http
HTTP/1.1 200 OK
Date: Mon, 27 Jul 2019 12:28:53 GMT
Server: Apache/2.4.14
Last-Modified: Wed, 22 Jul 2019 19:15:56 GMT
ETag: "34aa387-d-1568eb00"
Vary: Authorization,Accept
Accept-Ranges: bytes
Content-Length: 88
Content-Type: text/html
Connection: Closed
```

Puede notar que aquí el servidor no envía ningún dato después de las cabeceras.

### Método POST

El método POST se usa cuando desea enviar algunos datos al servidor, por ejemplo, actualización de archivo, datos de formulario, etc. El siguiente ejemplo utiliza el método POST para enviar datos de formulario al servidor, que serán procesados por un process.cgi y finalmente se devolverá una respuesta:

```http
POST /cgi-bin/process.cgi HTTP/1.1
User-Agent: python-requests(3.11)
Host: www.ft.com
Content-Type: text/xml; charset=utf-8
Content-Length: 88
Accept-Language: en-us
Accept-Encoding: gzip, deflate
Connection: Keep-Alive
```

```xml
<?xml version="1.0" encoding="utf-8"?>
<string xmlns="http://clearforest.com/">string</string>
```

El script del lado del servidor process.cgi procesa los datos pasados y envía la siguiente respuesta:

```http
HTTP/1.1 200 OK
Date: Mon, 27 Jul 2019 12:28:53 GMT
Server: Apache/2.4.14
Last-Modified: Wed, 22 Jul 2019 19:15:56 GMT
ETag: "34aa387-d-1568eb00"
Vary: Authorization,Accept
Accept-Ranges: bytes
Content-Length: 88
Content-Type: text/html
Connection: Closed
```

```html
<html>
<body>
<h1>Request Processed Successfully</h1>
</body>
</html>
```

### Método PUT

El método PUT se utiliza para solicitar al servidor que almacene el cuerpo de entidad incluido en una ubicación especificada por la URL dada. El siguiente ejemplo solicita al servidor que guarde el cuerpo de entidad dado en hello.html en la raíz del servidor:

```http
PUT /hello.html HTTP/1.1
User-Agent: python-requests(3.11)
Host: www.ft.com
Accept-Language: en-us
Connection: Keep-Alive
Content-type: text/html
Content-Length: 182
```

```html
<html>
<body>
<h1>Hello, World!</h1>
</body>
</html>
```

El servidor almacenará el cuerpo de entidad dado en el archivo hello.html y enviará la siguiente respuesta al cliente:

```http
HTTP/1.1 201 Created
Date: Mon, 27 Jul 2019 12:28:53 GMT
Server: Apache/2.4.14
Content-type: text/html
Content-length: 30
Connection: Closed
```

```html
<html>
<body>
<h1>The file was created.</h1>
</body>
</html>
```

### Método DELETE

El método DELETE se utiliza para solicitar al servidor que elimine un archivo en una ubicación especificada por la URL dada. El siguiente ejemplo solicita al servidor que elimine el archivo hello.html en la raíz del servidor:

```http
DELETE /hello.html HTTP/1.1
User-Agent: python-requests (3.11)
Host: www.ft.com
Accept-Language: en-us
Connection: Keep-Alive
```

El servidor eliminará el archivo hello.html mencionado y enviará la siguiente respuesta al cliente:

```http
HTTP/1.1 200 OK
Date: Mon, 27 Jul 2019 12:28:53 GMT
Server: Apache/2.4.14
Content-type: text/html
Content-length: 30
Connection: Closed
```

```html
<html>
<body>
<h1>URL deleted.</h1>
</body>
</html>
```

### Método CONNECT

El cliente utiliza el método CONNECT para establecer una conexión de red a un servidor web a través de HTTP. El siguiente ejemplo solicita una conexión con un servidor web.

```http
CONNECT www.ft.com HTTP/1.1
User-Agent: python-requests (3.11)
```

La conexión se establece con el servidor y la siguiente respuesta se envía al cliente:

```http
HTTP/1.1 200 Connection established
Date: Mon, 27 Jul 2019 12:28:53 GMT
Server: Apache/2.4.14
```

### Método OPTIONS

El cliente utiliza el método OPTIONS para averiguar los métodos HTTP y otras opciones admitidas por un servidor web. El cliente puede especificar una URL para el método OPTIONS, o un asterisco (*) para referirse a todo el servidor. El siguiente ejemplo solicita una lista de métodos admitidos por un servidor web.

```http
OPTIONS * HTTP/1.1
User-Agent: python-requests (3.11)
```

El servidor enviará una información basada en la configuración actual del servidor, por ejemplo:

```http
HTTP/1.1 200 OK
Date: Mon, 27 Jul 2019 12:28:53 GMT
Server: Apache/2.4.14
Allow: GET,HEAD,POST,OPTIONS,TRACE
Content-Type: httpd/unix-directory
```

### Método TRACE

El método TRACE se utiliza para enviar el contenido de una solicitud HTTP al solicitante, que se puede utilizar con fines de depuración en el momento del desarrollo. El siguiente ejemplo muestra el uso del método TRACE:

```http
TRACE / HTTP/1.1
Host: www.ft.com
User-Agent: python-requests (3.11) 
```

El servidor enviará el siguiente mensaje en respuesta a la solicitud anterior:

```http
HTTP/1.1 200 OK
Date: Mon, 27 Jul 2019 12:28:53 GMT
Server: Apache/2.4.14
Connection: close
Content-Type: message/http
Content-Length: 39

TRACE / HTTP/1.1
Host: www.ft.com
User-Agent: python-requests (3.11)
```



## Códigos de estado HTTP                                              <img src="images/ft_technology-dojos.png" alt="FT_TECHNOLOGY_DOJOS" style="zoom:150%;" />

El elemento Status-Code es un número entero de 3 dígitos donde el primer dígito del Status-Code define la clase de respuesta y los dos últimos dígitos no tienen ningún rol de categorización. Hay 5 valores para el primer dígito:

| N.S  | Código y Descripción                                         |
| ---- | ------------------------------------------------------------ |
| 1    | **1xx: Informativo** Significa que se recibió la solicitud y el proceso continúa. |
| 2    | **2xx: Éxito** Significa que la acción se recibió, comprendió y aceptó correctamente. |
| 3    | **3xx: Redirección** Significa que se deben realizar más acciones para completar la solicitud. |
| 4    | **4xx: Error del cliente** Significa que la solicitud contiene una sintaxis incorrecta o que no se puede cumplir. |
| 5    | **5xx: Error del servidor** Significa que el servidor no cumplió con una solicitud aparentemente válida. |

Los códigos de estado HTTP son extensibles y no se requieren aplicaciones HTTP para comprender el significado de todos los códigos de estado registrados. A continuación se muestra una lista de todos los códigos de estado.

### 1xx: Información

| Mensaje                  | Descripción                                                  |
| ------------------------ | ------------------------------------------------------------ |
| 100 Continúe             | El servidor solo ha recibido una parte de la solicitud, pero mientras no haya sido rechazada, el cliente debe continuar con la solicitud. |
| 101 Cambio de protocolos | El servidor cambia de protocolos.                            |

### 2xx: Éxito

| Mensaje                         | Descripción                                                  |
| ------------------------------- | ------------------------------------------------------------ |
| 200 OK                          | La petición fue correcta.                                    |
| 201 Creado                      | La solicitud se completo y un nuevo recurso fue creado.      |
| 202 Aceptado                    | La solicitud fue aceptada para procesamiento pero el procesamiento no ha sido completado. |
| 203 Información no autoritativa | La información en la cabecera de entidad local o una copia de un tercero no del servidor original. |
| 204 Sin Contenido               | Un código de estado y una cabecera se entregan en la respuesta pero no hay respuesta en el cuerpo de la entidad. |
| 205 Contenido reiniciado        | El navegador debe borrar el formulario utilizado para esta transacción para obtener información adicional. |
| 206 Contenido Parcial           | El servidor está devolviendo datos parciales del tamaño solicitado. Se usa en respuesta a una solicitud que especifica un cabecera de rango. El servidor debe especificar el rango incluido en la respuesta con la cabecera Content-Range. |

### 3xx: Redirección

| Mensaje                    | Descripción                                                  |
| -------------------------- | ------------------------------------------------------------ |
| 300 Múltiples escogencias  | Una lista de enlaces. El usuario puede seleccionar un enlace e ir a esa ubicación. Máximo cinco direcciones. |
| 301 Movido Permanentemente | La página solicitada se ha movido a una nueva URL.           |
| 302 Encontrado             | La página solicitada se ha movido temporalmente a una nueva URL. |
| 303 Ver Otro               | La página solicitada se puede encontrar en una URL diferente. |
| 304 No Modificado          | Este es el código de respuesta a una cabecera If-Modified-Since o If-None-Match, donde la URL no se ha modificado desde la fecha especificada. |
| 305 Use Proxy              | Se debe acceder a la URL solicitada a través del proxy mencionado en la cabecera Ubicación. |
| 306 *No se utiliza*        | Este código se utilizó en una versión anterior. Ya no se usa, pero el código está reservado. |
| 307 Redirección Temporal   | La página solicitada se ha movido temporalmente a una nueva URL. |

### 4xx: Error del Cliente

| Mensaje                                   | Descripción                                                  |
| ----------------------------------------- | ------------------------------------------------------------ |
| 400 Mala Solicitud                        | El servidor no entendió la solicitud.                        |
| 401 No autorizado                         | La página solicitada necesita credenciales de autenticación. |
| 402 Pago Requerido                        | *No se puede utilizar este código aún.*                      |
| 403 Prohibido                             | El acceso es prohibido a la página solicitada.               |
| 404 No Encontrado                         | El servidor no puede encontrar la página solicitada.         |
| 405 Método no permitido                   | El método especificado en la solicitud no está permitido.    |
| 406 No Aceptable                          | El servidor solo puede generar una respuesta que no es aceptada por el cliente. |
| 407 Autenticación de Proxy necesaria      | Debe autenticarse con un servidor proxy antes de que se pueda atender esta solicitud. |
| 408 Solicitud expirada                    | La solicitud tardó más de lo que el servidor estaba preparado para esperar. |
| 409 Conflicto                             | La solicitud no se pudo completar debido a un conflicto.     |
| 410 Ido                                   | La página solicitada ya no está disponible.                  |
| 411 Longitud requerida.                   | La "Longitud del contenido" no está definida. El servidor no aceptará la solicitud sin ella. |
| 412 Precondición fallida                  | La condición previa dada en la solicitud ha sido evaluada como falsa por el servidor. |
| 413 Solicitud de entidad demasiado larga. | El servidor no aceptará la solicitud porque la entidad de la solicitud es demasiado grande. |
| 414 URL de solicitud demasiado larga.     | El servidor no aceptará la solicitud porque la URL es demasiado larga. Ocurre cuando convierte una solicitud de "publicación" en una solicitud de "obtención" con una información de consulta larga. |

### 4xx: Error del Cliente

| Mensaje                                   | Descripción                                                  |
| ----------------------------------------- | ------------------------------------------------------------ |
| 415 Tipo de medio no soportado.           | El servidor no aceptará la solicitud porque el tipo de medio no es compatible. |
| 416 Rango Solicitado no satisfactorio.    | El rango de bytes solicitado no está disponible y está fuera de los límites. |
| 417 Expectativa Fallida                   | Este servidor no pudo cumplir la expectativa dada en la cabecera de solicitud Expect. |

### 5xx: Error del Servidor

| Mensaje                        | Descripción                                                  |
| ------------------------------ | ------------------------------------------------------------ |
| 500 Error interno del servidor | La solicitud no se completó. El servidor encontró una condición inesperada. |
| 501 No Implementado            | La solicitud no se completó. El servidor no admitía la funcionalidad requerida. |
| 502 Enlace malo                | La solicitud no se completó. El servidor recibió una respuesta no válida del servidor ascendente. |
| 503 Servicio no disponible     | La solicitud no se completó. El servidor se está sobrecargando o cayendo temporalmente. |
| 504 Enlace expirado            | Se agotó el tiempo de espera de la puerta de enlace.         |
| 505 Versión HTTP no soportada  | El servidor no admite la versión "protocolo http".           |



## Campos de cabeceras                                                   <img src="images/ft_technology-dojos.png" alt="FT_TECHNOLOGY_DOJOS" style="zoom:150%;" />

Los cabeceras HTTP proporcionan información necesaria sobre la solicitud o respuesta, o sobre el objeto enviado en el cuerpo del mensaje. Hay cuatro tipos de cabeceras en mensajes HTTP:

* **Cabecera general:** estas cabeceras tienen aplicabilidad general tanto para mensajes de solicitud como de respuesta.
* **Cabecera de solicitud:** estas cabeceras solo se aplican a los mensajes de solicitud.
* **Cabecera de respuesta:** estas cabeceras solo se aplican a los mensajes de respuesta.
* **Cabecera de entidad:** estas cabeceras definen metainformación sobre el cuerpo de la entidad o, si no hay ningún cuerpo presente, sobre el recurso identificado por la solicitud.

### Cabeceras Generales

#### Cache-Control

La cabecera general de Cache-Control se utiliza para especificar directivas que DEBEN ser obedecidas por todo el sistema de almacenamiento en caché. La sintaxis es la siguiente:

```http
Cache-Control : cache-request-directive|cache-response-directive
```

Un cliente o servidor HTTP puede utilizar la cabecera general de control de caché para especificar parámetros para la caché o para solicitar ciertos tipos de documentos de la caché. Las directivas de almacenamiento en caché se especifican en una lista separada por comas. Por ejemplo:

```http
Cache-control: no-cache
```

La siguiente tabla enumera las directivas de solicitud de caché importantes que puede utilizar el cliente en su solicitud HTTP:

| N.S  | Cache Request Directive and Description                      |
| ---- | ------------------------------------------------------------ |
| 1    | **no-cache **La caché no debe utilizar la respuesta para satisfacer una solicitud posterior sin una revalidación exitosa con el servidor de origen. |
| 2    | **no-store **La caché no debe almacenar nada sobre la solicitud del cliente o la respuesta del servidor. |
| 3    | **max-age = seconds** Indica que el cliente está dispuesto a aceptar una respuesta cuya antigüedad no supere el tiempo especificado en segundos. |
| 4    | **max-stale [ = seconds ] **Indica que el cliente está dispuesto a aceptar una respuesta que ha excedido su tiempo de vencimiento. Si se dan segundos, no debe caducar más de ese tiempo. |
| 5    | **min-fresh = seconds** Indica que el cliente está dispuesto a aceptar una respuesta cuya vida útil de actualización no sea inferior a su antigüedad actual más el tiempo especificado en segundos. |
| 6    | **no-transform **No convierte el cuerpo de la entidad.       |
| 7    | **only-if-cached **No recupera datos nuevos. La caché puede enviar un documento solo si está en la caché y no debe contactar al servidor de origen para ver si existe una copia más nueva. |

Las siguientes directivas importantes de respuesta de caché que pueden ser utilizadas el servidor en su respuesta HTTP:

| S.N. | Cache Response Directive and Description                     |
| ---- | ------------------------------------------------------------ |
| 1    | **public** Indica que cualquier caché puede almacenar la respuesta en caché. |
| 2    | **private **Indica que todo o parte del mensaje de respuesta está destinado a un solo usuario y no debe almacenarse en la memoria caché compartida. |
| 3    | **no-cache** Una caché no debe utilizar la respuesta para satisfacer una solicitud posterior sin una revalidación exitosa con el servidor de origen. |
| 4    | **no-store **La caché no debe almacenar nada sobre la solicitud del cliente o la respuesta del servidor. |
| 5    | **no-transform **No convierte el cuerpo de la entidad.       |
| 6    | **must-revalidate** La caché debe verificar el estado de los documentos obsoletos antes de usarlo y no se deben usar los vencidos. |
| 7    | **proxy-revalidate** La directiva proxy-revalidate tiene el mismo significado que la directiva must-revalidate, excepto que no se aplica a las cachés de agentes de usuario no compartidos. |
| 8    | **max-age = seconds** Indica que el cliente está dispuesto a aceptar una respuesta cuya antigüedad no supere el tiempo especificado en segundos. |
| 9    | **s-maxage = seconds** La edad máxima especificada por esta directiva anula la edad máxima especificada por la directiva max-age o la cabecera Expires. La directiva s-maxage siempre es ignorada por un caché privado. |

#### Connection

La cabecera general de la conexión permite al remitente especificar las opciones deseadas para esa conexión en particular y no deben ser comunicadas por servidores proxy a través de conexiones adicionales. A continuación se muestra la sintaxis simple para usar la cabecera de conexión:

```http
Connection : "Connection"
```

 HTTP / 1.1 define la opción de conexión "cerrar" para que el remitente indique que la conexión se cerrará una vez completada la respuesta. Por ejemplo:

```http
Connection: close
```

De forma predeterminada, HTTP 1.1 utiliza conexiones persistentes, donde la conexión no se cierra automáticamente después de una transacción. HTTP 1.0, por otro lado, no tiene conexiones persistentes por defecto. Si un cliente 1.0 desea usar conexiones persistentes, usa el parámetro de keep-alive de la siguiente manera:

```http
Connection: keep-alive
```

#### Date

Todas las marcas de fecha / hora HTTP DEBEN estar representadas en la hora media de Greenwich (GMT), sin excepción. Las aplicaciones HTTP pueden utilizar cualquiera de las siguientes tres representaciones de marcas de fecha / hora:

```http
Sun, 06 Nov 1994 08:49:37 GMT  ; RFC 822, updated by RFC 1123
Sunday, 06-Nov-94 08:49:37 GMT ; RFC 850, obsoleted by RFC 1036
Sun Nov  6 08:49:37 1994       ; ANSI C's asctime() format
```

El primer formato es el más utilizado.

#### Pragma

La cabecera general de Pragma se utiliza para incluir directivas específicas de implementación que pueden aplicarse a cualquier destinatario a lo largo de la cadena de solicitud / respuesta. Por ejemplo:

```http
Pragma: no-cache
```

La única directiva definida en HTTP / 1.0 es la directiva no-cache y se mantiene en HTTP 1.1 para compatibilidad con versiones anteriores. No se definirán nuevas directivas de Pragma en el futuro.

#### Trailer

El valor de la cabecera general Trailer indica que el conjunto dado de campos de cabecera está presente en el final de un mensaje codificado con una codificación de transferencia fragmentada. A continuación se muestra la sintaxis del campo de cabecera Trailer:

```http
Trailer : field-name
```

Las cabeceras de mensaje listados en la cabecera Trailer no deben incluir los siguientes campos:

- Transfer-Encoding
- Content-Length
- Trailer

#### Transfer-Encoding

La cabecera general Transfer-Encoding indica qué tipo de transformación se ha aplicado al cuerpo del mensaje para transferirlo de forma segura entre el remitente y el destinatario. Esto no es lo mismo que la codificación de contenido porque las codificaciones de transferencia son una propiedad del mensaje, no del cuerpo de la entidad. La sintaxis la cabecera Transfer-Encoding es la siguiente:

```http
Transfer-Encoding: chunked
```

Todos los valores de codificación de transferencia no distinguen entre mayúsculas y minúsculas.

#### Upgrade

La cabecera general Upgrade permite al cliente especificar qué protocolos de comunicación adicionales admite y le gustaría usar si el servidor lo considera apropiado para cambiar de protocolo. Por ejemplo:

```http
Upgrade: HTTP/2.0, SHTTP/1.3, IRC/6.9, RTA/x11
```

La cabecera general Upgrade está destinada a proporcionar un mecanismo simple para la transición de HTTP / 1.1 a algún otro protocolo incompatible.

#### Via

Las puertas de enlace y los proxies deben utilizar la cabecera general Via para indicar los protocolos y destinatarios intermedios. Por ejemplo, se podría enviar un mensaje de solicitud desde un agente de usuario HTTP / 1.0 a un proxy interno llamado en código "fred", que usa HTTP / 1.1 para reenviar la solicitud a un proxy público en nowhere.com, que completa la solicitud mediante el reenvio al servidor de origen en www.ics.uci.edu. La solicitud recibida por www.ics.uci.edu tendría entonces la siguiente cabecera Via:

```http
Via: 1.0 fred, 1.1 nowhere.com (Apache/1.1)
```

#### Warning

La cabecera general de Warning se utiliza para llevar información adicional sobre el estado o la transformación de un mensaje que podría no reflejarse en el mensaje. Una respuesta puede llevar más de una cabecera Warning.

```http
Warning : warn-code SP warn-agent SP warn-text SP warn-date
```

### Cabeceras de Solicitud de Cliente

#### Accept

La cabecera de solicitud Accept se puede usar para especificar ciertos tipos de medios que son aceptables para la respuesta. La sintaxis general es la siguiente:

```http
Accept: type/subtype [q=qvalue]
```

Se pueden enumerar varios tipos de medios separados por comas y el qvalue opcional representa un nivel de calidad aceptable para aceptar tipos en una escala de 0 a 1. A continuación, se muestra un ejemplo:

```http
Accept: text/plain; q=0.5, text/html, text/x-dvi; q=0.8, text/x-c
```

Esto se interpretaría como **text / html** y **text / x-c** y son los tipos de medios preferidos, pero si no existen, envíe la entidad **text / x-dvi**, y si no existe, envíe la entidad **text / plain**.

#### Accept-Charset

La cabecera de solicitud Accept-Charset se puede utilizar para indicar qué conjuntos de caracteres son aceptables para la respuesta. A continuación se muestra la sintaxis general:

```http
Accept-Charset: character_set [q=qvalue]
```

Se pueden enumerar varios conjuntos de caracteres separados por comas y el qvalue opcional representa un nivel de calidad aceptable para los conjuntos de caracteres no preferidos en una escala de 0 a 1. A continuación se muestra un ejemplo:

```http
Accept-Charset: iso-8859-5, unicode-1-1; q=0.8
```

El valor especial "*", si está presente en el campo Accept-Charset, coincide con cada conjunto de caracteres y si no hay una cabecera Accept-Charset, el valor predeterminado es que cualquier conjunto de caracteres es aceptable.

#### Accept-Encoding

La cabecera Accept-Encoding es similar a Accept, pero restringe las codificaciones de contenido que son aceptables en la respuesta. La sintaxis general es:

```http
Accept-Encoding: encoding types
```

Ejemplos:

```http
Accept-Encoding: compress, gzip
Accept-Encoding:
Accept-Encoding: *
Accept-Encoding: compress;q=0.5, gzip;q=1.0
Accept-Encoding: gzip;q=1.0, identity; q=0.5, *;q=0
```

#### Accept-Language

La cabecera de solicitud Accept-Language es similar a Accept, pero restringe el conjunto de lenguajes naturales que se prefieren como respuesta a la solicitud. La sintaxis general es:

```http
Accept-Language: language [q=qvalue]
```

Se pueden enumerar varios idiomas separados por comas y el qvalue opcional representa un nivel de calidad aceptable para los idiomas no preferidos en una escala de 0 a 1. A continuación, se muestra un ejemplo:

```http
Accept-Language: da, en-gb;q=0.8, en;q=0.7
```

#### Authorization

La cabecera de solicitud de autorización consta de credenciales que contienen la información de autenticación del agente de usuario para el ámbito del recurso que se solicita. La sintaxis general es:

```http
Authorization : credentials
```

La especificación HTTP / 1.0 define el esquema de autorización BÁSICO, donde el parámetro de autorización es la cadena de nombre de usuario: contraseña codificada en base64. A continuación se muestra un ejemplo:

```http
Authorization: BASIC ZnQ6ZnRyb2NrdGhpc3NoaXQhIQ==
```

#### Cookie

La cabecera de solicitud de cookie contiene un par de nombre / valor de información almacenada para esa URL. A continuación se muestra la sintaxis general:

```http
Cookie: name=value
```

Se pueden especificar varias cookies separadas por punto y coma de la siguiente manera:

```http
Cookie: name1=value1;name2=value2;name3=value3
```

#### Expect

La cabecera de solicitud Expect se usa para indicar que el cliente requiere un conjunto particular de comportamientos del servidor. La sintaxis general es:

```http
Expect : 100-continue | expectation-extension
```

Si un servidor recibe una solicitud que contiene un campo Expect que incluye una extensión de expectativa que no admite, debe responder con un estado 417 (Expectativa fallida).

#### From

La cabecera de solicitud From contiene una dirección de correo electrónico de Internet para el usuario humano que controla el agente de usuario solicitante. A continuación se muestra un ejemplo sencillo:

```http
From: webmaster@w3.org
```

Esta cabecera se puede utilizar para fines de registro y como un medio para identificar la fuente de solicitudes no válidas o no deseadas.

#### Host

La cabecera de solicitud de host se utiliza para especificar el host de Internet y el número de puerto del recurso que se solicita. La sintaxis general es:

```http
Host : "Host" ":" host [ ":" port ] ;
```

Un host sin información de puerto final implica el puerto predeterminado, que es 80. Por ejemplo, una solicitud en el servidor de origen para http://www.w3.org/pub/WWW/ sería:

```http
GET /pub/WWW/ HTTP/1.1
Host: www.w3.org
```

#### If-Match

La cabecera de solicitud If-Match se usa con un método para hacerlo condicional. Este cabecera solicita al servidor que realice el método solicitado solo si el valor dado en esta etiqueta coincide con las etiquetas de entidad dadas representadas por ETag. La sintaxis general es:

```http
If-Match : entity-tag
```

Un asterisco (*) coincide con cualquier entidad y la transacción continúa solo si la entidad existe. A continuación se muestran posibles ejemplos:

```http
If-Match: "xyzzy"
If-Match: "xyzzy", "r2d2xxxx", "c3piozzzz"
If-Match: *
```

Si ninguna de las etiquetas de entidad coincide, o si se da "*" y no existe una entidad actual, el servidor no debe realizar el método solicitado y debe devolver una respuesta 412 (Precondición fallida).

#### If-Modified-Since

La cabecera de solicitud If-Modified-Since se usa con un método para hacerlo condicional. Si la URL solicitada no se ha modificado desde el momento especificado en este campo, no se devolverá una entidad desde el servidor; en su lugar, se devolverá una respuesta 304 (no modificada) sin ningún cuerpo de mensaje. La sintaxis general de if-modified-since es:

```http
If-Modified-Since : HTTP-date
```

Un ejemplo del campo es:

```http
If-Modified-Since: Sat, 29 Oct 1994 19:43:31 GMT
```

Si ninguna de las etiquetas de entidad coincide, o si se da "*" y no existe una entidad actual, el servidor no debe realizar el método solicitado y debe devolver una respuesta 412 (Precondición fallida).

#### If-None-Match

La cabecera de solicitud If-None-Match se usa con un método para hacerlo condicional. Esta cabecera solicita al servidor que realice el método solicitado solo si uno de los valores dados en esta etiqueta coincide con las etiquetas de entidad dadas representadas por ETag. La sintaxis general es:

```htt
If-None-Match : entity-tag
```

Un asterisco (*) coincide con cualquier entidad y la transacción continúa solo si la entidad no existe. A continuación se muestran los posibles ejemplos:

```http
If-None-Match: "xyzzy"
If-None-Match: "xyzzy", "r2d2xxxx", "c3piozzzz"
If-None-Match: *
```

#### If-Range

La cabecera de solicitud If-Range se puede usar con un GET condicional para solicitar solo la parte de la entidad que falta, si no se ha cambiado, y la entidad completa si se ha cambiado. La sintaxis general es la siguiente:

```http
If-Range : entity-tag | HTTP-date
```

Se puede usar una etiqueta de entidad o una fecha para identificar la entidad parcial ya recibida. Por ejemplo:

```http
If-Range: Sat, 29 Oct 1994 19:43:31 GMT
```

Aquí, si el documento no ha sido modificado desde la fecha dada, el servidor devuelve el rango de bytes dado por la cabecera Range; de lo contrario, devuelve todo el documento nuevo.

#### If-Unmodified-Since

La cabecera de solicitud If-Unmodified-Since se usa con un método para hacerlo condicional. La sintaxis general es:

```http
If-Unmodified-Since : HTTP-date
```

Si el recurso solicitado no se ha modificado desde el momento especificado en este campo, el servidor debe realizar la operación solicitada como si la cabecera If-Unmodified-Since no estuviera presente. Por ejemplo:

```http
If-Unmodified-Since: Sat, 29 Oct 1994 19:43:31 GMT
```

Si la solicitud da como resultado un estado distinto de 2xx o 412, se debe ignorar la cabecera If-Unmodified-Since.

#### Max-Forwards

La cabecera de solicitud Max-Forwards proporciona un mecanismo con los métodos TRACE y OPTIONS para limitar el número de proxies o puertas de enlace que pueden reenviar la solicitud al siguiente servidor entrante. Aquí está la sintaxis general:

```http
Max-Forwards : n
```

El valor de Max-Forwards es un número entero decimal que indica el número restante de veces que se puede reenviar este mensaje de solicitud. Esto es útil para depurar con el método TRACE, evitando bucles infinitos. Por ejemplo:

```http
Max-Forwards : 5
```

La cabecera Max-Forwards puede ignorarse para todos los demás métodos definidos en la especificación HTTP.

#### Proxy-Authorization

La cabecera de solicitud de autorización de proxy permite al cliente identificarse a sí mismo (o su usuario) ante un proxy que requiere autenticación. Aquí está la sintaxis general:

```http
Proxy-Authorization : credentials
```

El valor del campo Proxy-Authorization consta de credenciales que contienen la información de autenticación del agente de usuario para el proxy y / o dominio del recurso que se solicita.

#### Range

La cabecera de solicitud de rango especifica los rangos parciales del contenido solicitado del documento. La sintaxis general es:

```http
Range: bytes-unit=first-byte-pos "-" [last-byte-pos]
```

El valor de posición del primer byte en una especificación de rango de bytes da el desplazamiento de bytes del primer byte de un rango. El valor de posición del último byte da el desplazamiento de bytes del último byte del rango; es decir, las posiciones de bytes especificadas son inclusivas. Puede especificar una unidad de bytes como bytes. Las compensaciones de bytes comienzan en cero. Algunos ejemplos simples son los siguientes:

```pseudocode
- Los primeros 500 bytes 
Range: bytes=0-499

- Los segundos 500 bytes
Range: bytes=500-999

- Los 500 bytes finales
Range: bytes=-500

- El primer y último byte solamente
Range: bytes=0-0,-1
```

Se pueden enumerar varios rangos, separados por comas. Si falta el primer dígito del rango de bytes separados por comas, se supone que el rango cuenta desde el final del documento. Si falta el segundo dígito, el rango es el byte n hasta el final del documento.

#### Referer

El campo Referer request-header permite al cliente especificar la dirección (URI) del recurso desde el cual se solicitó la URL. La sintaxis general es la siguiente:

```http
Referer : absoluteURI | relativeURI
```

A continuación se muestra un ejemplo sencillo:

```http
Referer: http://www.ft.com/http/index.htm
```

Si el valor del campo es un URI relativo, debe interpretarse en relación con el URI de solicitud.

#### TE

La cabecera de solicitud de TE indica qué extensión de codificación de transferencia está dispuesto a aceptar en la respuesta y si está dispuesto a aceptar campos de cola en una codificación de transferencia fragmentada. A continuación se muestra la sintaxis general:

```http
TE   : t-codings
```

La presencia de la palabra clave "trailers" indica que el cliente está dispuesto a aceptar campos de trailers en una codificación de transferencia fragmentada y se especifica de cualquiera de las formas:

```http
TE: deflate
TE:
TE: trailers, deflate;q=0.5
```

Si el valor del campo TE está vacío o si no hay ningún campo TE presente, solo se fragmenta la codificación de transferencia. Un mensaje sin codificación de transferencia siempre es aceptable.

#### User-Agent

La cabecera de solicitud de agente de usuario contiene información sobre el agente de usuario que origina la solicitud. A continuación se muestra la sintaxis general:

```http
User-Agent : product | comment
```

Ejemplo:

```http
User-Agent: python-requests (3.11)
```

### Cabeceras de Respuesta del Servidor

#### Accept-Ranges

La cabecera de respuesta *Accept-Ranges* permite al servidor indicar su aceptación de solicitudes de rango para un recurso. La sintaxis general es:

```http
Accept-Ranges  : range-unit | none
```

Por ejemplo, un servidor que acepta solicitudes de rango de bytes puede enviar:

```http
Accept-Ranges: bytes
```

Los servidores que no acepten ningún tipo de solicitud de rango para un recurso pueden enviar:

```http
Accept-Ranges: none
```

Esto le avisará al cliente que no intente una solicitud de rango.

#### Age

La cabecera de respuesta de *Age* transmite la estimación del remitente de la cantidad de tiempo desde que la respuesta fue generada (o su revalidación) en el servidor de origen. La sintaxis general es:
```http
Age : delta-seconds
```

Los valores de Age son números enteros decimales no negativos, que representan el tiempo en segundos. A continuación se muestra un ejemplo sencillo:

```http
Age: 1030
```

Un servidor HTTP/1.1 que incluye una caché debe incluir una cabecera Age en cada respuesta generada desde su propia caché.

#### ETag

La cabecera de respuesta de *ETag* proporciona el valor actual de la etiqueta de entidad para la variante solicitada. La sintaxis general es:

```http
ETag :  entity-tag
```

A continuación se muestran algunos ejemplos sencillos:

```http
ETag: "xyzzy"  
ETag: W/"xyzzy"  
ETag: ""
```

### Location

La cabecera de respuesta *Location* se utiliza para redirigir al destinatario a una ubicación que no sea el Request-URI para completarlo. La sintaxis general es:

```http
Location : absoluteURI
```

A continuación se muestra un ejemplo sencillo:

```http
Location: http://www.example.com/http/index.htm
```

La cabecera Content-Location difiere de Location en que Content-Location identifica la ubicación original de la entidad incluida en la solicitud.

#### Proxy-Authenticate

La cabecera de respuesta *Proxy-Authenticate* debe ser incluido como parte de una respuesta 407 (Proxy Authentication Required). La sintaxis general es:

```http
Proxy-Authenticate  : challenge
```

#### Retry-After

La cabecera de respuesta *Retry-After* puede ser usado con una respuesta 503 (Service Unavailable) para indicar cuánto tiempo es esperado para que el servicio no esté disponible para el cliente solicitante. La sintaxis general es:

```http
Retry-After : HTTP-date | delta-seconds
```

Ejemplos:

```http
Retry-After: Fri, 31 Dec 1999 23:59:59 GMT  
Retry-After: 120
```

En el último ejemplo, el retraso es de 2 minutos.

#### Server

La cabecera de respuesta *Server* contiene información sobre el software utilizado por el servidor de origen para manejar la solicitud. La sintaxis general es:

```http
Server : product | comment
```
A continuación se muestra un ejemplo sencillo:

```http
Server: Apache/2.2.14 (Win32)
```

Si la respuesta se reenvía a través de un proxy, la aplicación del proxy no debe modificar el encabezado de respuesta Server.

#### Set-Cookie

La cabecera de respuesta *Set-Cookie* contiene un par de información de nombre/valor para guardar para esta URL. La sintaxis general es:

```http
Set-Cookie: NAME=VALUE; OPTIONS
```

La cabecera de respuesta Set-Cookie comprende el token Set-Cookie, seguido de una lista separada por comas de una o más cookies. Estos son los valores posibles que puede especificar como opciones:

| S.N. | Opciones y Descripción                                       |
| ---- | ------------------------------------------------------------ |
| 1    | **Comment=comment**  Esta opción puede ser usada para especificar cualquier comentario asociado con la cookie. |
| 2    | **Domain=domain** El atributo Domain especifica el dominio para el que la cookie es válida. |
| 3    | **Expires=Date-time**  La fecha de caducidad de la cookie. Si está en blanco, la cookie caducará cuando el visitante salga del navegador. |
| 4    | **Path=path**  El atributo Path especifica el subconjunto de URL a las que se aplica esta cookie. |
| 5    | **Secure** Indica al agente de usuario que devuelva la cookie solo bajo una conexión segura. |

A continuación, se muestra un ejemplo de un encabezado de cookie simple generado por el servidor:

```http
Set-Cookie: name1=value1,name2=value2; Expires=Wed, 09 Jun 2021 10:18:14 GMT
```

#### Vary

La cabecera de respuesta *Vary* especifica que la entidad tiene múltiples fuentes y, por lo tanto, puede variar de acuerdo con la lista especificada de cabeceras de solicitud. A continuación se muestra la sintaxis general:

```http
Vary : field-name
```

Puede especificar varios cabeceras separados por comas y un valor de asterisco "*" indica que los parámetros no especificados no se limitan a los cabeceras de solicitud. A continuación se muestra un ejemplo sencillo:

```http
Vary: Accept-Language, Accept-Encoding
```

Aquí los nombres de campo no distinguen entre mayúsculas y minúsculas.

#### WWW-Authenticate

La cabecera de respuesta *WWW-Authenticate* debe ser incluido en los mensajes de respuesta 401 (Unauthorized). El valor del campo consta de al menos un desafío que indica los esquemas de autenticación y los parámetros aplicables al Request-URI. La sintaxis general es:

```http
WWW-Authenticate : challenge
```
El valor del campo WWW-Authenticate puede contener más de un desafío, o si se proporciona más de una cabecera WWW-Authenticate, el contenido de un desafío en sí puede contener una lista de parámetros de autenticación separados por comas. A continuación se muestra un ejemplo sencillo:

```http
WWW-Authenticate: BASIC realm="Admin"
```
### Cabeceras de Entidad

#### Allow

La cabecera de entidad *Allow* enumera el conjunto de métodos admitidos por el recurso identificado por el Request-URI. La sintaxis general es:

```http
Allow : Method
```
Puede especificar varios métodos separados por comas. A continuación se muestra un ejemplo sencillo:

```http
Allow: GET, HEAD, PUT
```

Este campo no puede evitar que un cliente pruebe otros métodos.

#### Content-Encoding

La cabecera de entidad *Content-Encoding* es usado como un modificador del tipo de medio. La sintaxis general es:

```http
Content-Encoding : content-coding
```

El content-coding es una característica de la entidad identificada por el Request-URI. A continuación se muestra un ejemplo sencillo:

```http
Content-Encoding: gzip
```

Si el content-coding de una entidad en un mensaje de solicitud no es aceptable para el servidor de origen, el servidor debe responder con un código de estado 415 (Unsupported Media Type).

#### Content-Language

La cabecera de la entidad *Content-Language* describe el o los lenguajes naturales de la audiencia prevista para la entidad adjunta. A continuación se muestra la sintaxis general:

```http
Content-Language : language-tag
```

Es posible que se enumeren varios idiomas para el contenido destinado a múltiples audiencias. A continuación se muestra un ejemplo sencillo:

```http
Content-Language: mi, en
```

El propósito principal de Content-Language es permitir a un usuario identificar y diferenciar entidades de acuerdo con el idioma preferido del usuario.

#### Content-Length

La cabecera de entidad *Content-Length* indica el tamaño del cuerpo de entidad, en número decimal de OCTET, enviado al destinatario o, en el caso del método HEAD, el tamaño del cuerpo de entidad que habría sido enviado, si la solicitud hubiera sido un GET. La sintaxis general es:

```http
Content-Length : DIGITS
```

A continuación se muestra un ejemplo sencillo:

```http
Content-Length: 3495
```

Cualquier Content-Length mayor o igual a cero es un valor válido.

#### Content-Location

La cabecera de entidad *Content-Location* puede ser usado para proporcionar la ubicación del recurso para la entidad incluida en el mensaje cuando esa entidad es accesible desde una ubicación separada del URI del recurso solicitado. La sintaxis general es:

```http
Content-Location:  absoluteURI | relativeURI 
```

A continuación se muestra un ejemplo sencillo:

```http
Content-Location: http://www.example.com/http/index.htm
```

El valor de Content-Location también define el URI base de la entidad.

#### Content-MD5

La cabecera de entidad *Content-MD5* se puede utilizar para proporcionar un hash MD5 de la entidad para verificar la integridad del mensaje al recibirlo. La sintaxis general es:

```http
Content-MD5  : md5-digest using base64 of 128 bit MD5 digest as per RFC 1864
```

A continuación se muestra un ejemplo sencillo:

```http
Content-MD5  : 8c2d46911f3f5a326455f0ed7a8ed3b3
```

El hash MD5 es calculado en base al contenido del cuerpo de la entidad, incluida cualquier codificación de contenido que ha sido aplicado, pero sin incluir ninguna codificación de transferencia aplicada al cuerpo del mensaje.

#### Content-Range

La cabecera de entidad *Content-Range* es enviado con un cuerpo de entidad parcial para especificar en qué parte del cuerpo de entidad completo debería ser aplicado el cuerpo parcial. La sintaxis general es:

```http
Content-Range : bytes-unit SP first-byte-pos "-" last-byte-pos
```

Ejemplos de valores de byte-content-range-spec, asumiendo que la entidad contiene un total de 1234 bytes:

```
- Los primeros 500 bytes:
Content-Range : bytes 0-499/1234

- Los segundos 500 bytes:
Content-Range : bytes 500-999/1234

- Todos excepto los primeros 500 bytes:
Content-Range : bytes 500-1233/1234

- Los últimos 500 bytes:
Content-Range : bytes 734-1233/1234
```

Cuando un mensaje HTTP incluye el contenido de un solo rango, este contenido es transmitido con un encabezado Content-Range y un encabezado Content-Length que muestra el número de bytes realmente transferidos. Por ejemplo,

```http
HTTP/1.1 206 Partial content  
Date: Wed, 15 Nov 1995 06:25:24 GMT  
Last-Modified: Wed, 15 Nov 1995 04:58:08 GMT  
Content-Range: bytes 21010-47021/47022  
Content-Length: 26012  
Content-Type: image/gif
```

#### Content-Type

La cabecera de entidad *Content-Type* indica el tipo de medio del cuerpo de entidad enviado al destinatario o, en el caso del método HEAD, el tipo de medio que habría sido enviado si la solicitud hubiera sido un GET. La sintaxis general es:

```http
Content-Type : media-type
```

A continuación se muestra un ejemplo:

```http
Content-Type: text/html; charset=ISO-8859-4
```

#### Expires

La cabecera de entidad *Expires* proporciona la fecha/hora después de la cual la respuesta es considerada obsoleta. La sintaxis general es:

```http
Expires : HTTP-date
```

A continuación se muestra un ejemplo:

```http
Expires: Thu, 01 Dec 1994 16:00:00 GMT
```

#### Last-Modified

La cabecera de entidad *Last-Modified* indica la fecha y hora en que el servidor de origen cree que la variante fue modificada por última vez. La sintaxis general es:

```http
Last-Modified: HTTP-date
```

A continuación se muestra un ejemplo:

```http
Last-Modified: Tue, 15 Nov 1994 12:45:26 GMT
```



## HTTPS                                                                                <img src="images/ft_technology-dojos.png" alt="FT_TECHNOLOGY_DOJOS" style="zoom:150%;" />

Es una extensión segura de HTTP enfocada a la instalación y configuración de un certificado digital (SSL o TLS) para hacer uso del protocolo y establecer un canal de conexión segura con un servidor.

La importancia del uso del protocolo HTTPS radica en la seguridad que se debe establecer al compartir datos confidenciales (usuarios, contraseñas, números de tarjetas de crédito, etc.) por medio de un canal de comunicación ya que toda la información viaja cifrada, lo que la hace ilegible dado el caso que sea comprometida por un atacante.

### Conceptos básicos

#### Codificación:
Es la modificación de un mensaje cambiando todos sus caracteres por medio de una representación o patrón definido, con este mismo sistema se realiza la decodificación del mensaje para regresarlo a su forma original y poder leerlo. Su uso se fundamenta en algunas restricciones que tiene el protocolo http para el envío de caracteres especiales.

#### Hashing:
Es la representación de mensajes de entrada a mensajes de salida de una longitud fija o cortada. Un hash identifica de manera única al mensaje de entrada y no puede ser devuelto al mensaje original, la única manera de encontrar su forma original es por medio de bases de datos que relacionen hashes con sus respectivos textos.

#### Salting:
Es el proceso de adicionar caracteres aleatorios al hash con el propósito de hacerlo más complejo de encontrar en caso de que sea comprometido.

#### Stretching:
Es el número de repeticiones que se realiza el proceso del hashing o salting; es decir, se tiene el hash de una contraseña, se le adicionan caracteres aleatorios y a este nuevo dato se le realiza el procedimiento de hashing nuevamente tantas veces como desee.

#### Cifrado:
Es la representación de la información de manera ilegible la cual puede ser leída mediante el uso de una llave o contraseña secreta.

#### Cifrado Simétrico:
Es el procedimiento criptográfico en el que se usa la misma llave o contraseña tanto para cifrar como para descifrar la información. No es tan seguro ya que si un atacante compromete dicha llave puede leer los datos que viajan en el canal de comunicación. Se usa para enviar grandes cantidades de información cifrada.

#### Cifrado Asimétrico:
En este procedimiento se utiliza un par de llaves (pública y privada) lo que hace que el algoritmo sea más seguro. Si se requiere enviar un mensaje confidencial de A hacia B, primero se deben intercambiar las llaves públicas de cada lado, “A” cifra la información con la llave pública de B y se la envía para que sea descifrada con su llave privada, esto garantiza que si el mensaje es comprometido no pueda ser leído por un atacante y que solo la persona que custodia la llave privada pueda descifrar la información. Se usa para compartir datos confidenciales de menor tamaño como números de tarjetas de crédito, contraseñas y las llaves del cifrado simétrico.

#### Firma Digital:
Es el la combinación de los procedimientos de hash y cifrado a un mensaje que se desee enviar, en términos generales, si A desea enviar un mensaje firmado digitalmente a B, primero se le ejecuta el procedimiento del hashing a la información original y la salida de éste se cifra de manera asimétrica pero usando la llave privada de la persona que envía el mensaje, esta nueva salida se denomina firma digital, se envía junto con el mensaje original y la llave pública de A para que B pueda descifrar el mensaje, obtener el hash del mensaje original y compararlos.
![img](https://lh6.googleusercontent.com/nM5boAcCyNNgntzVSIIy_imMrsn_kcRiNW4wG4ThctcpMeQDa7kmVCJSNE4ykQFYg9IovIlU-NXUyHR2SGej_0d45iRp0MD2Ua4Aq4ALWlM6r-vVXneX1sUNPmnwButdNJrKfB-f)

#### Confidencialidad: 
Busca garantizar que la información solo sea leíble por la persona que usted desea.

#### Integridad:
Garantiza que los datos enviados no sean alterados en el canal de comunicación.

#### Autenticidad:
Garantiza que el mensaje ha sido enviado por la persona que dice ser.

Según los conceptos vistos y las últimas 3 definiciones, se tiene un resumen de cuáles procedimientos garantizan los 3 pilares de la seguridad:

* **Codificación:** No garantiza ninguno.

* **Hashing:** Garantiza sólo la integridad ya que me confirma si un mensaje llegó completo a su destinatario.

* **Cifrado:** Garantiza sólo la confidencialidad ya que no permite que el mensaje sea leído por otra persona que no tenga la llave privada para descifrarlo.

* **Firma Digital:** Garantiza integridad y autenticidad ya que especifica que el mensaje llegó completo y viene de la persona que me compartió la llave pública.

  

### Seguridad

#### Certificado SSL: 
Responde al acrónimo de Secure Sockets Layer (Capa de Conexión Segura) y es uno de los protocolos criptográficos que proporcionan una comunicación cifrada entre un sitio web y un navegador web. Al día de hoy esta tecnología es obsoleta ya que se le hallaron problemas de seguridad y ha sido reemplazada por el protocolo TLS.

#### Certificado TLS:
Responde al acrónimo de Transport Layer Security (Seguridad de la Capa de Transporte) y el es protocolo de seguridad encargado de garantizar la seguridad de los datos que viajan en el túnel de comunicación cifrado entre un cliente y un servidor.
  ![img](https://lh3.googleusercontent.com/Qvlir7Dj-q9ymsJa8Eu-zPi4LizVDjhD6KNAJz4zKH0Mbc8jYXF1o6JO4BPj_6hDN6XHYHGds3E9zQSWWyX7OSqo9eiOdOq4V1_U_bBiPT8ff1lrXQz-PZpHMmVQ3FCLRaF7tTRi)
#### TLS/SSL Handshake:
Es el protocolo encargado de describir los pasos requeridos para establecer el canal de comunicación cifrado entre un cliente y un servidor o entre 2 servidores:
  ![img](https://lh6.googleusercontent.com/0_lkx178HZvNkR5cbG5VsFx70GgvXrkvpnSoIpsl_OQ-Av7WeBlXtdBxgXzlMBDJWyL6Son4Gp7wEDXrH4fdFeWJegL-qbaIf9dW14T15rl1Ktjn9pnbJMKQv-7sYgRSK9GvCdY_)

1. El cliente inicia la conversación enviando al servidor un mensaje “Client Hello” con una lista de información que lleva: versión del certificado usado, algoritmos criptográficos, cadena de valores aleatorios y otras opciones de tecnología.
     ![img](https://lh6.googleusercontent.com/a71wgMjlxr3TE07UIJklwctMRfZiqHUQRZQ_C6JnekUMb9AFmiB3Cd3p_iSQ_g3KOzhDAjMtVKARpUTlBZAOg9DQM3fejDghCId3bx7LlZ3ZKbFRUEFd5FN5SWGP0yRhBn498hqU)

2. El servidor responde al cliente con otro mensaje “Server Hello” indicando las opciones seleccionadas de la lista del cliente y adicional envía el certificado digital del servidor, una cadena valores aleatorios y su llave pública. (De manera opcional el servidor puede enviar una petición para que cliente le comparta su certificado digital, esto se denomina Mutual TLS).
     ![img](https://lh5.googleusercontent.com/CQ8qUdP0atAd2cpSbhOuwbfX9AA3kQwfSzuTkvzgdwVsxGdblWkWeGB0uZ2V-CnGMIFlToib7uJE_kfZUDrb_LmnHzvbhkUIrg9UHoqQOEQq57CyDEgJqkLVdIw0AlbRUQmyoCu0)

3. El cliente verifica el certificado digital con una entidad certificadora de confianza.
     ![img](https://lh6.googleusercontent.com/WTJrrxNN_Q1e8rejq9f1AqauaOEyGPTJedQlo3OONWj5fbfk4iHZqmVyDHFGjmVpyfvTp0yS0UNPcLcAWEC_qM8k9VPLzgYwstHTvOldGpHPVx6yB-_Fd9FEuT0Wx1pl8WlrDt3r)

4. Una vez validado el certificado y utilizando las cadenas de valores aleatorios del cliente y del servidor, el cliente genera una llave secreta compartida cifrada con la llave pública del servidor y se la envía al servidor junto con la llave pública del cliente.

5. Si el servidor realizó la petición para el Mutual TLS, el cliente le envía una cadena de valores aleatorios cifrados con su llave privada junto con su certificado digital.

6. El servidor valida el certificado digital del cliente ante una entidad certificadora de confianza.

7. El cliente le envía al servidor un mensaje cifrado con la llave secreta compartida indicando que su parte del handshake ha finalizado.
     ![img](https://lh4.googleusercontent.com/wKYk8Zm-Y4BtroFgBt2ssipnKzRsBguMNyocy1Pke6w1nZT013C78q7V7Ivm4O0DsMbVWCq1dkb4TFApicau77vObw8L7MRSash4EiQiKAyEMja5gAtGHQfADa1pn8p-kPhfek7-)

8. El servidor también le envía al cliente un mensaje de finalización del handshake cifrado con la llave secreta compartida.
     ![img](https://lh5.googleusercontent.com/aHiauGPL2Jw7eeI4rUgxRytH7DzejZ6EH2iMicw6ZZ9BPZJriCFmb0uV1jlKLOyXR2ZLwy8MytzS-pkkgrZcS0T4CehKOe54KodhP4Ms_4ctwoJPGpOEz7mCZ5JlY75UxOAdPCJq)

9. De esta manera se establece un túnel cifrado para iniciar una comunicación segura entre cliente y servidor o entre servidores y toda la información enviada en el tiempo de duración de la sesión TLS viaja cifrada de manera simétrica con la llave secreta compartida.
     ![img](https://lh5.googleusercontent.com/fUoX7ynUM3XzRN-EiWmy0pg6BEAjulaqvAXiR7ewW-uv6P_jwDF8drn-Q-CFzZkAGsofui_bhZtOFRasmcjoYaSNZtQ0nCvY2sve1PGLtKyFiFqHnnIpJEOTkFMA8b5j7MSOESaR)

   #### Mutual TLS:

   También conocido como autenticación bidireccional, es el procedimiento de seguridad que garantiza que el tráfico sea seguro y confiable en ambas direcciones mediante la autenticación entre un cliente y un servidor o entre 2 servidores (siendo ésta la más común) durante el procedimiento del handshake mencionado anteriormente.

   Vale aclarar que es un procedimiento opcional y lo solicita el servidor que recibe la primera petición en el inicio de la construcción del canal de comunicación cifrado.

![img](https://lh5.googleusercontent.com/CMBdUMo-D336jwEe5h6v_HYIXt6aw-2u7INg-A_FZkFPr0lkioorZAR8wADzNds_HdekPWd52nLt833lbaPJhm_95j5W_sKH_ntHP6-HzzywGAnCKLd16E38LFIyihPrmGtZ25Xj)





### REFERENCIAS

https://www.ibm.com/support/knowledgecenter/SSFKSJ_9.0.0/com.ibm.mq.sec.doc/q009930_.htm

https://www.hostinger.co/tutoriales/ssl-tls-https/

https://medium.com/@umeshae/what-happens-in-ssl-tls-handshake-f42a3c906062

https://developers.cloudflare.com/access/service-auth/mtls/

https://www.wirelessphreak.com/2020/05/mutual-authentication-using-tls.html

https://www.tutorialspoint.com/http/index.htm
