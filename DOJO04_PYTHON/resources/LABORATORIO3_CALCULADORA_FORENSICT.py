#!/usr/bin/env python
# -*- coding: utf-8 -*-

Num1_Ingresado = input("Introduce un número: ")
Num2_Ingresado = input("Introduce otro número: ")

opcion = 0

print("""
¿Cúal operación desea realizar?
1) Sumar los dos números
2) Restar los dos números
3) Multiplicar los dos números
4) Dividir el primer número ingresado entre el segundo número ingresado
""")

opcion = input("Introduce un número: ")

if opcion == 1:
   print("La suma de",Num1_Ingresado,"+",Num2_Ingresado,"es",Num1_Ingresado+Num2_Ingresado)
elif opcion == 2:
   print("La resta de",Num1_Ingresado,"-",Num2_Ingresado,"es",Num1_Ingresado-Num2_Ingresado)
elif opcion == 3:
   print("La multiplicacion de",Num1_Ingresado,"*",Num2_Ingresado,"es",Num1_Ingresado*Num2_Ingresado)
elif opcion == 4:
   print("La division entre ",Num1_Ingresado,"/",Num2_Ingresado,"es",Num1_Ingresado/Num2_Ingresado)
else:
   print("El valor ingresado es incorrecto")