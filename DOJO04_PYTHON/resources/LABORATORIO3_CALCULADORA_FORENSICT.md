# **LABORATORIO 3 - EJERCICIO DE CALCULADORA**

## Calculadora

En el siguiente laboratorio se busca crear, a partir de un código en python, una calculadora que realice ejercicios básicos como suma, resta, multiplicación y división. Para este ejercicio se utilizarán condicionales y los datos ingresados por el usuario para la toma de decisiones.

Para comenzar, se deben inicializar las variables con las cuales se va a trabajar, es decir los números que el usuario va a ingresar para que sean tratados. Para tal fin, se declaran las variables `Num1_Ingresado` y `Num2_Ingresado` las cuales almacenarán los datos ingresados por el usuario. Para capturar estos datos se utiliza la función `input()`, ya que al momento de invocarla el programa se detiene esperando que se escriba algo y se pulse la tecla “enter”. Además, a la función `input` se le enviará un argumento que se escribirá en la pantalla, es decir un mensaje que acompañará la solicitud del dato que se va a ingresar.

Así las cosas, para ingresar los números a trabajar se realiza de la siguiente manera:

``` python
Num1_Ingresado = input("Introduce un número: ")
Num2_Ingresado = input("Introduce otro número: ")
```

En este momento ya se tienen los datos con los cuales la calculadora va a operar, ahora se debe crear un método por el cual se pueda elegir qué operación aritmética realizar, suma - resta - multiplicación - división, para tal fin se va a utilizar condicionales. Por medio del condicional `if` se controlará cual acción realizar, por lo tanto, se debe definir una variable que sea el parámetro que regulará el condicional.

Se procede a crear y definir la variable `opcion` la cual será un valor ingresado por el usuario final.

``` python
opcion = 0
```

Seguidamente, se le debe dar a conocer al usuario las opciones que puede elegir, por lo tanto, por medio de la función `print` se imprime el mensaje que se le debe presentar al usuario, de la siguiente manera:

``` python
print("""
¿Cúal operación desea realizar?
1) Sumar los dos números
2) Restar los dos números
3) Multiplicar los dos números
4) Dividir el primer número ingresado entre el segundo número ingresado
""")

opcion = input("Introduce un número: ")
```

Hasta este punto, el usuario ya ingresó un valor para la variable `opción` el cual puede ser 1,2,3 o 4, si la persona eligió un número diferente se debe generar un mensaje que anuncie que el valor ingresado no es una opción válida.

Finalmente, únicamente falta utilizar el condicional `if` para decidir qué acción realizar, por lo tanto se deben generar 4 condicionales donde la variable `opcion` será el factor de decisión para realizar la tarea. En otras palabras, lo que se busca es ingresar un condicional que analice, por ejemplo, si el valor ingresado por el usuario es el número 1 se debe realizar la suma entre las variables `Num1_Ingresado` y `Num2_Ingresado`. Lo cual se traduce en python así:

``` python
if opcion == 1:
   print("La suma de",Num1_Ingresado,"+",Num2_Ingresado,"es",Num1_Ingresado+Num2_Ingresado)
elif opcion == 2:
   print("La resta de",Num1_Ingresado,"-",Num2_Ingresado,"es",Num1_Ingresado-Num2_Ingresado)
elif opcion == 3:
   print("La multiplicacion de",Num1_Ingresado,"*",Num2_Ingresado,"es",Num1_Ingresado*Num2_Ingresado)
elif opcion == 4:
   print("La division entre ",Num1_Ingresado,"/",Num2_Ingresado,"es",Num1_Ingresado/Num2_Ingresado)
else:
   print("El valor ingresado es incorrecto")
```

Finalmente, el código completo quedaría de la siguiente manera:

``` python
#!/usr/bin/env python
# -*- coding: utf-8 -*-

Num1_Ingresado = input("Introduce un número: ")
Num2_Ingresado = input("Introduce otro número: ")

opcion = 0

print("""
¿Cúal operación desea realizar?
1) Sumar los dos números
2) Restar los dos números
3) Multiplicar los dos números
4) Dividir el primer número ingresado entre el segundo número ingresado
""")

opcion = input("Introduce un número: ")

if opcion == 1:
   print("La suma de",Num1_Ingresado,"+",Num2_Ingresado,"es",Num1_Ingresado+Num2_Ingresado)
elif opcion == 2:
   print("La resta de",Num1_Ingresado,"-",Num2_Ingresado,"es",Num1_Ingresado-Num2_Ingresado)
elif opcion == 3:
   print("La multiplicacion de",Num1_Ingresado,"*",Num2_Ingresado,"es",Num1_Ingresado*Num2_Ingresado)
elif opcion == 4:
   print("La division entre ",Num1_Ingresado,"/",Num2_Ingresado,"es",Num1_Ingresado/Num2_Ingresado)
else:
   print("El valor ingresado es incorrecto")
```

> Si desea ver y ejecutar los ejercicios, puede hacerlo en el archivo de [Ejemplos Laboratorio 3](LABORATORIO3_CALCULADORA_FORENSICT.py).
