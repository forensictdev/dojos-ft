#!/usr/bin/env python
# -*- coding: utf-8 -*-

class Estudiante:
    "Esta es la clase llamada Estudiante "

        # Constructor de la clase
    def __init__(self, nombre, altura, peso, color_cabello):
        self.nombre = nombre
        self.altura = altura
        self.peso = peso
        self.color_cabello = color_cabello

    def estudiar(self):
        print('Estoy estudiando')
    def comer(self):
        print('Estoy comiendo')
    def dormir(self):
        print('zzz zzz zzz')

#instancia de la clase Estudiante
estudiante1 = Estudiante(nombre="Pepito Perez", altura=1.75, peso=80,color_cabello= "negro")


# Imprimir = Atributos
print(estudiante1.nombre)
print(estudiante1.altura)
print(estudiante1.peso)
print(estudiante1.color_cabello)
# Llamar Métodos
estudiante1.estudiar()
estudiante1.comer()
estudiante1.dormir()