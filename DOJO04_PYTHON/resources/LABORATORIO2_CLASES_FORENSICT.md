## **LABORATORIO 2 - CLASE ESTUDIANTE**

El objetivo de este laboratorio es afianzar los conocimientos en el tema “clases” en python, para tal fin, se generará un script en el cual se utilizaran los conceptos mencionados en la documentación previamente entregada.

Para comenzar se debe tener claro los elementos que conforman una clase, nombre de la clase - objetos - atributos - métodos, debido a que se deben comenzar a definir.

Inicialmente se comienza asignando el nombre a la clase, en este caso va a ser `Estudiante` y se define con la palabra `class` seguido del nombre de la clase, a su vez se le puede asignar un comentario para dar mayor claridad al momento de analizar el código.  Por lo anterior, la definición de la clase queda de la siguiente manera:

``` python
class Estudiante:
    "Esta es la clase llamada Estudiantes "
```

Luego de tener definida la clase se procede  a generar el constructor de la misma el cual va a ser la plantilla o molde que definirá los campos que se encuentran dentro de la clase. El constructor se define con la palabra `def` seguido del método `_init_` que se encarga de  inicializar los atributos del objeto que se crearán.

Posteriormente, dentro de los paréntesis, se enuncian los atributos que se utilizarán en la clase generada, para este laboratorio son: nombre, altura, peso y color de cabello. En este caso se incluye el argumento `self` el cual hace referencia a la instancia de la clase y permite que las funciones definidas dentro de la función puedan ser utilizadas con la notación self.Nombre_de_Variable, además indica el contexto en el que se está trabajando.

``` python
        # Constructor de la clase
    def __init__(self, nombre, altura, peso, color_cabello):
        self.nombre = nombre
        self.altura = altura
        self.peso = peso
        self.color_cabello = color_cabello
```

En este momento ya se tiene creada una clase con 4 atributos y únicamente falta definir los métodos o funciones que se utilizaran en ella. Estos métodos son acciones que un estudiante realiza de manera cotidiana, por ejemplo: estudiar, comer y dormir.  Para definir las funciones se debe colocar el parámetro `def` seguido del nombre de la función seguido de los paréntesis `()`.

Luego, dentro del método, se define la acción que se desea realizar, en caso se va a imprimir un mensaje que esté relacionado con la función que realiza el estudiante.

Por lo tanto, la definición de los métodos quedaría de la siguiente manera:

``` python
    def estudiar(self):
        print('Estoy estudiando')
    def comer (self):
        print('Estoy comiendo')
    def dormir(self):
        print('zzz zzz zzz')
```

En este punto del código se tiene una clase que está compuesta por atributos y métodos y únicamente falta instanciar los atributos definidos. Para tal fin, se crea la VARIABLE `estudiante1` el cual estará compuesto por unos valores definidos para los 4 atributos previamente expuestos. El instanciamiento de la variable queda de la siguiente manera:

``` python
#instancia de la clase Estudiante
estudiante1 = Estudiante(nombre="Pepito Perez", altura=1.75, peso=80,color_cabello= "negro")
```

Finalmente, se procede a utilizar la información que se encuentra en la clase. En este caso simplemente se van a imprimir los valores de los atributos y los métodos.

``` python
# Imprimir = Atributos
print(estudiante1.nombre)
print(estudiante1.altura)
print(estudiante1.peso)
print(estudiante1.color_cabello)
# Llamar Métodos
estudiante1.estudiar()
estudiante1.comer()
estudiante1.dormir()
```

Para terminar, la clase Estudiante quedaría de la siguiente forma:

``` python
#!/usr/bin/env python
# -*- coding: utf-8 -*-

class Estudiante:
    "Esta es la clase llamada Estudiante "

        # Constructor de la clase
    def __init__(self, nombre, altura, peso, color_cabello):
        self.nombre = nombre
        self.altura = altura
        self.peso = peso
        self.color_cabello = color_cabello

    def estudiar(self):
        print('Estoy estudiando')
    def comer(self):
        print('Estoy comiendo')
    def dormir(self):
        print('zzz zzz zzz')

#instancia de la clase Estudiante
estudiante1 = Estudiante(nombre="Pepito Perez", altura=1.75, peso=80,color_cabello= "negro")


# Imprimir = Atributos
print(estudiante1.nombre)
print(estudiante1.altura)
print(estudiante1.peso)
print(estudiante1.color_cabello)
# Llamar Métodos
estudiante1.estudiar()
estudiante1.comer()
estudiante1.dormir()
```

> Si desea ver y ejecutar los ejercicios, puede hacerlo en el archivo de [Ejemplos Laboratorio 2](LABORATORIO2_CLASES_FORENSICT.py).
