# **LABORATORIO 1 - FUNCIONES BÁSICAS**

## Función Welcome

Se desea construir una función simple que haga un saludo a un nombre ingresado por consola. Para ello definimos una función sin argumentos de entrada que llamaremos `welcome`.

``` python
def welcome():
```

El paso siguiente será llamar a la función nativa de python llamada `input()`, la cual permite ingresar texto desde la consola, el cual será entregado como `string`. Adicionalmente, la función `input` permite ingresar como parámetro una cadena de texto que se va a mostrar en consola, la cual se usará para indicar al usuario que es lo que requerimos que escriba. Finalmente, se asigna el valor de la función `input` en una variable que se llama `name`.

``` python
name = input("Mi nombre:")
```

Por último, lo que desea mostrar el texto que el usuario ingresó previamente en consola, ello se hace invocando la función `print()`, donde se le pasa un mensaje que aparecerá constantemente acompañando el texto que contiene la variable `name` y después se le agrega un salto de línea (\n).

``` python
print('Bienvenido', name, '\n')
```

Uniendo todas las líneas de código, queda la siguiente función:

``` python
def welcome():
   name = input("Mi nombre:")
   print('Bienvenido', name, '\n')
```

Recuerden que es necesario llamar la función para que se ejecute.

``` python
welcome()
``` 

## Función de numeros pares

Lo siguiente a construir será una función que identifique si un número ingresado por el usuario es par o impar. Esta función ayudará a conocer un poco los condicionales y el casting o  especificación de tipos de variables. Empezaremos por definir una función que se llama `number_is_even`.

``` python
def number_is_even():
```

Después, se invoca la clase `int()`, la cual es la clase para el manejo de números enteros, donde le pasaremos por parámetro la función `input()` con su respectivo mensaje al usuario. Todo esto se lo asignamos a la variable que se llama `number`.

``` python
number = int(input('Escriba un número:'))
```

Lo que hará `int` es tomar el valor que recibió de la función `input` y lo convertirá en un objeto de la clase `int`, en otras palabras, lo hará un número operable (suma, resta, multiplicación, etc).

Ahora se define un condicional con las palabras reservada `if` y `else`, donde la condición será que si al aplicarle el módulo 2 `(el módulo es denotado por el carácter “%”)` al número ingresado por el usuario, el resultado es `igual (denotado por “==”)` a cero, lo que significa que es par. Impar en caso contrario (`else`).

``` python
if number % 2 == 0:
     print('Es par \n')
else:
     print('Es impar \n')
```

Uniendo todo el código, queda lo siguiente:

``` python
def number_is_even():
   number = int(input('Escriba un número:'))
   if number % 2 == 0:
       print('Es par \n')
   else:
       print('Es impar \n')
```

Recuerden que es necesario llamar la función para que se ejecute.

``` python
number_is_even()
``` 

## Función para conocer el tamaño de una cadena de texto

Ahora la función que se quiere codificar, es una que muestre cual es el tamaño de un texto ingresado por el usuario. Para ello, se define la función sin argumentos que se `llamará len_of_text()`

``` python
def len_of_text():
```

Después, se invoca a la clase `str()` que contendrá entre sus argumentos el valor que obtenga de la función `input()` y todo esto se lo asignamos a una variable llamada `my_text.`

``` python
my_text = str(input('Escribe un texto cualquiera: '))
```

La clase `str()`, al igual que la clase int() convertirá todo valor ingresado en la consola y capturado por el `input()`, en un objeto de la clase (clase `String`), al cual se le podrán realizar operaciones propias de los objetos tipo string como concatenar a otro string, iterarlo en una lista, etc.

Lo siguiente es invocar una función nativa de Python llamada `len()`, la cual recibe por argumentos una `lista`, una `tupla` o un `String` y calcula el número de elementos contenidos en estos objetos. A dicha función se le pasa como argumento la variable `my_text` y el resultado se le asigna a una variable `len_text`.

``` python
len_text = len(my_text)
```

Por último, pinta en consola el contenido de `len_text` y la función completa queda de la siguiente forma.

``` python
def len_of_text():
   my_text = str(input('Escribe un texto cualquiera: '))
   len_text = len(my_text)
   print('El tamaño de la cadena es:', len_text, '\n')
```

Recuerden que es necesario llamar la función para que se ejecute.

``` python
len_of_text()
``` 

## Función para detectar un palindromo 

Lo siguiente a construir será una función que identifique si una cadena de texto ingresada es un palindromo `is_palindrome`.

``` python
def is_palindrome():
```

> Un palíndromo es una palabra o frase que se lee igual en un sentido que en otro (por ejemplo, "Ana" o "Ana lava lana"). 

Lo primero que debemos hacer es solictar al usuario que ingrese su cadena de caracteres, realizando un casting para garantizar que se guarde como tipo `str`. 

``` python
   word = str(input("Mi palabra:"))
```
Debido a que en los palindromos no se tiene en cuenta los espacios, procesaremos la cadena de texto recibida para omitirlos usando el método `replace()` y se almacenará en `chain_of_letter`.

``` python
    chain_of_letter = word.replace(' ', '')
```

Se definirá una nueva cadena de texto `reversed_word` donde se almacenará el texto en la dirección opuesta.

``` python
    reversed_word = ''
```

Mediante el ciclo for recorremos `chain_of_letter` generar el reverso de la cadena y almacenarla en `reversed_word`.

``` python
    for i in chain_of_letter:
        reversed_word = i + reversed_word
```

Ahora usaremos el condicional if para comparar la cadena de texto original `chain_of_letter` con la cadena opuesta `reversed_word`, si coincide informamos que es palindromo y si no, decimos que no lo es.

``` python
    if reversed_word == chain_of_letter:
        print('Es palíndromo \n')
    else:
        print('No es palíndromo \n')
```

Finalmente la función quedaría de la siguiente manera:

``` python
def is_palindrome():
    word = str(input("Mi palabra:"))
    chain_of_letter = word.replace(' ', '')
    reversed_word = ''
    for i in chain_of_letter:
        reversed_word = i + reversed_word
    if reversed_word == chain_of_letter:
        print('Es palíndromo \n')
    else:
        print('No es palíndromo \n')
```
Recuerden que es necesario llamar la función para que se ejecute.

``` python
is_palindrome()
``` 

## Función para detectar un palindromo usando reversed

Lo siguiente a construir será una función que identifique si una cadena de texto ingresada es un palindromo `is_palindrome` usando el método `reversed`.

``` python
def is_palindrome_two():
```

Lo primero que debemos hacer es solictar al usuario que ingrese su cadena de caracteres, realizando un casting para garantizar que se guarde como tipo `str`. 

``` python
   word = str(input("Mi palabra:"))
```
Debido a que en los palindromos no se tiene en cuenta los espacios, procesaremos la cadena de texto recibida para omitirlos usando el método `replace()` y se almacenará en `chain_of_letter`.

``` python
    chain_of_letter = word.replace(' ', '')
```

Se definirá una nueva cadena de texto `reversed_word` donde se almacenará el texto en la dirección opuesta.

``` python
    reversed_word = ''
```

Mediante el método `reversed` podemos generar el reverso de la cadena `chain_of_letter` y unirla a `reversed_word`.

``` python
reversed_word.join(reversed(chain_of_letter)
```

Ahora usaremos el condicional if para comparar la cadena de texto original `chain_of_letter` con la cadena opuesta, si coincide informamos que es palindromo y si no, decimos que no lo es.

``` python
   if chain_of_letter == reversed_word.join(reversed(chain_of_letter)):
         print('Es palíndromo \n')
      else:
         print('No es palíndromo \n')
```

Finalmente la función quedaría de la siguiente manera:

``` python
def is_palindrome_two():
    word = str(input("Mi palabra:"))
    chain_of_letter = word.replace(' ', '')
    reversed_word = ''
    if chain_of_letter == reversed_word.join(reversed(chain_of_letter)):
        print('Es palíndromo \n')
    else:
        print('No es palíndromo \n')

```

Recuerden que es necesario llamar la función para que se ejecute.

``` python
is_palindrome_two()
```

> Si desea ver y ejecutar los ejercicios, puede hacerlo en el archivo de [Ejemplos Laboratorio 1](LABORATORIO1_METODOS-BASICOS_FORENSICT.py).
