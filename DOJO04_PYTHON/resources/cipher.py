from Crypto.PublicKey import RSA
from Crypto.Cipher import PKCS1_v1_5
from base64 import decode, encode,b64encode, b64decode
from requests import get

def get_pub_key_from_server():
    try:
        url = 'https://Servidor-de-login.josedaviddavi34.repl.co/pubKey'
        response = get(url)
        public_key = response.text
    except Exception as error:
        print('Ocurrió un error al obtener la llave pública del servidor:',error)
        return None
    else:
        return public_key 

def store_pub_key(public_key):
    try:
      with open('pubKey.pem','w') as file_pub_key:        
        file_pub_key.write(public_key)
    except Exception as error:
      print('Ocurrió un error al guardar la llave pública:',error)
      
def get_public_key():
  try:
    with open('pubKey.pem') as pub_key_file:
      public_key = pub_key_file.read()
  except Exception as error:
    print('Ocurrio un error inesperado:', error)
    return None, None
  else:
    return public_key

def encrypt_message(password):
    try: 
        message = str(password)
        message = message.encode("utf-8")
        recipient_key = RSA.import_key(get_public_key())
        cipher = PKCS1_v1_5.new(recipient_key)
        ciphertext = cipher.encrypt(message)
        text = b64encode(ciphertext).decode()
        return text
    except Exception as error:
        print('Ocurrió un error al cifrar el dato', error)
        return None







