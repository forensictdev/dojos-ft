# GUIA DE CONFIGURACIÓN DE PYTHON                                                           <img src="images/ft_technology-dojos.png" alt="FT_TECHNOLOGY_DOJOS" style="zoom:150%;" />

## Introducción

Python es un lenguaje de programación que nació en 1991 de la mano de Guido van Rossum, quien tomó como influencia lenguajes muy conocidos como  C, Java, Perl, Haskell, entre otros., y que ha influido en la creación de otros lenguajes de programación como Javascript, Go y Ruby. Actualmente, Python es uno de los lenguajes más importantes en lo que a tecnología se refiere, ya que es usado para distintas labores desde el uso en machine learning pasando por la construcción de programas de seguridad como SQLMap, hasta crear una simple aplicación Rest.

Aunque actualmente Python está en la versión 3.9 (simplificado python 3), Python ha pasado por diversas transformaciones que lo han hecho un lenguaje de programación versátil, multiparadigma y multiplataforma, características que lo han llevado a ser ampliamente adoptado en la construcción y por qué no, la destrucción de software.

En esta guía se verá un poco de las capacidades de este poderoso lenguaje desde el punto de vista del desarrollo y  la seguridad de la información, donde se aprenderá conceptos básico pero esenciales que ayudarán a crear soluciones rápidas pero eficientes a diversos problemas.

## Instalación y configuración de Python 3

Para empezar, se debe tener ciertas herramientas y características listas para comenzar la construcción de código sin problemas. Lo primero será instalar la versión más reciente (estable) de python, el cual se puede descargar del siguiente link e instalarla.

>**Nota:** Si se está usando una distribución basada en Linux, no es necesario descargar e instalar nada, puesto que python viene incluido por defecto en Linux.

[https://www.python.org/downloads/](https://www.python.org/downloads/)

Para comprobar que Python funciona correctamente, puede ejecutar los siguientes comandos:

Para S.O  Windows o MAC: `python --version o python -v`

Para S.O basados en Linux: `python3 --version o python3 -v`

Si todo está funcionando perfectamente, se mostrará la versión disponible de python.

### IDE de Desarrollo

El/los IDE de desarrollo que se recomiendan para empezar la codificación serán [Pycharm](https://www.jetbrains.com/es-es/pycharm/download) (Community Edition) o [Visual Studio Code](https://code.visualstudio.com/download), las cuales permiten desarrollar código de una manera más fluida, realizar recomendaciones sobre el código así como la personalización del desarrollo por medio de extensiones que facilitan aún más esta tarea.

Una alternativa, es el uso de IDE's online como [repl.it](https://repl.it), los cuales permiten ejecutar el código en nube y evitar todo el proceso de instalación y configuración. Para este DOJO se hará uso de esta herramienta.

>**Nota:** La ejecución de código en IDE en nube pueden llegar a tener problemas al ejecutar algunas caracteristicas de python como puede ser el manejo de archivos.

<div class="pull-center"  align="center"><img src="images/img-1.png" width="210" height="210"><img src="images/img-2.png" width="210" height="210">
<img src="images/img-3.png" width="210" height="210"></div>

>**Nota:** Para realizar la instalación de estas herramientas en una distribución de linux basada en Debian, se recomienda instalarlas por medio del gestor de paquetes `snap`.

### Configuración del Entorno Virtual (Virtual Environment)

La configuración de un entorno virtual es de suma importancia pues permite separar ambientes de trabajo de Python, donde en cada entorno se puede manejar una versión de intérprete diferente a otro entorno virtual, así como permite manejar dependencias con total libertad, sin la preocupación de que la modificación de una librería pueda afectar otro proyecto.

Para la creación de entornos virtuales existen 2 herramientas, las cuales son:

1. **virtualenv**

    Para usar virtualenv es necesario tener instalado en nuestro sistema el paquete de éste. Con el siguiente comando se podrá realizar la instalación de `virtualenv`:

    S.O Windows:

        $ curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py

        $ python get-pip.py

        $ py -m venv env

    S.O Linux ó MAC:

        $ sudo apt-get install python-virtualenv

    Una vez instalados los paquetes, se podrá usar por medio del siguiente comando:

    S.O Windows:

        $ py -m venv env

    S.O Linux ó MAC:

        $ virtualenv -p /usr/bin/python3 mi_entorno_virtual

    Donde la opción `-p` permite realizar la selección de la versión Python. Una vez terminado el proceso, se creará una carpeta con una configuración inicial, tal como se ven en el siguiente código.

        $ ls
        mi_entorno_virtual/

2. **venv**

    Para usar venv es necesario tener instalado en nuestro sistema el paquete de éste. Con el siguiente comando se podrá realizar la instalación de `venv` en sistemas operativos basados en Debian y Mac. Para Windows es suficiente con el comando del punto anterior.

        $ sudo apt-get install python3-venv

    Después de instalados, se usa el comando.

        $ python3 -m venv mi_entorno

    Al finalizar el proceso, se creará una carpeta con el nombre `mi_entorno`, la cual será el entorno virtual inicial y listo para empezar, tal como se aprecia en la siguiente porción de código.

        $ ls
        mi_entorno/

Para la **activación** de los entornos virtuales, se debe asegurar de que la carpeta del entorno virtual existe dentro del proyecto, en este caso es `entorno2`. Después, se procede a la activación del entorno virtual por medio del siguiente comando.

    $ source entorno2/bin/activate

Si la activación fue correcta, en la consola aparecerá un prefijo con el nombre del entorno virtual entre paréntesis, tal como se muestra a continuación:

    (entorno)$ ls 

Para **desactivar** el entorno virtual, solo basta con copiar el comando `deactivate` en la consola y desaparecerá el nombre del entorno virtual de la consola.

>**Nota:** IDE como Pycharm, crean, activan y desactivan automáticamente los entornos, solo basta con crear un proyecto desde cero en este IDE.

## Estilo de código

Se recomienda que todo código que se escriba utilizando el lenguaje de programación Python sea escrito de la siguiente manera:

* **Clases:** la convención para la codificación de los nombres de las clases se recomienda sea con **pascalcase**.

  - **Ejemplo:** TextoDeEjemploPascalCase

* **Funciones y lógica asociada:**  la codificación para los nombres de las  funciones, así como para la lógica contenida en ésta, se recomienda que sea tipo **snake_case.** Los nombres de las funciones deben describir qué hacen exactamente, no deben ser muy extensas y deben ser definidas para una tarea simple de ser posible.

  - **Ejemplo:** texto_ejemplo_snake_case.

* **Variables:** las estructuras deben ser concisas, es decir que debe describir muy bien para que fue definida la variable. Además, se debe evitar crear variables que no se van a usar. Las variables que se definan deben ser nombradas en **snake_case**.

  - **Ejemplo:** se necesita una variable que contenga una suma acumulativa, entonces se llama `suma_total`

La estructura de las clases, funciones y variables deben ser de la siguiente forma.

``` python
class Math:
    def sum_of_two_numbers(self, number_one, number_two):
        total_sum = number_one + number_two
        return sum
```

>**Nota:** para ver más información de los estilos en la codificación de variables, funciones y clases vea el **anexo 2**.

## ANEXOS

1. [Case Styles: Camel, Pascal, Snake, and Kebab Case](https://medium.com/better-programming/string-case-styles-camel-pascal-snake-and-kebab-case-981407998841)
2. [PEP 8](https://www.python.org/dev/peps/pep-0008/)
