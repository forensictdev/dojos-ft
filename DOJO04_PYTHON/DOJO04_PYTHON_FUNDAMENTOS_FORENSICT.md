# FUNDAMENTOS DE PYTHON                                                           <img src="images/ft_technology-dojos.png" alt="FT_TECHNOLOGY_DOJOS" style="zoom:150%;" />

## ¡Hola Mundo!

Teniendo [instalado Python](DOJO04_PYTHON_GUIA_CONFIGURACION_FORENSICT.md),
 ahora se puede crear el primer archivo que ejecute una breve instrucción, para ello se crea un archivo `saludo.py`, en el cual, se debe escribir el siguiente código:

`print('Hola mundo')`

Guardar y ejecutar el archivo escribiendo en consola (Windows, Linux o Mac) `python saludo.py`, se mostrará la salida que se ve a continuación.

<p align="center"><img src="images/Salida1.png" /></p>

>**Nota**: en windows se puede ejecutar el script con el comando py o python.

## Comentarios De Líneas De Código

Antes de empezar a programar, es necesario explicar cómo comentar líneas de código tanto específicas como un bloque de líneas.  Para comentar una línea de código específica como la línea 2, anteponga al código el carácter numeral (`#`) y para comentar un bloque de código encierre el código a comentar entre comillas dobles o sencillas (**”** o **‘**), un ejemplo de comentarios de código es el siguiente:

<p align="center"><img src="images/Salida2.png" /></p>

## Tipos De Datos

Python maneja cualquier dato como un objeto, esto lo hace sumamente versátil y fácil de implementar, ya que no es necesario especificar el tipo de dato esperado para una variable, sin embargo, existen tipos de datos sumamente importantes los cuales se describen a continuación:


### Primitivos

Existen  4 tipos de datos primitivos, que son:

* **Cadenas:** es un tipo de dato que se compone por cualquier cadena de texto, las cuales son delimitadas por comillas dobles (“) o por comillas simples (‘) y los cuales son manejados por la **clase**  `str`, que contiene diversos métodos/funciones para el procesado de estas cadenas.
* **Enteros:** es un tipo de dato que se compone solo de números enteros, donde no se recibe otro dato más que un número entero positivo o negativo (sin punto o comas que representan decimales pero si acepta el símbolo de menos) y que son manejados por la **clase**  `int`, que contiene diversos métodos/funciones para el procesado de los números.
* **Flotantes:** es un tipo de dato que se compone tanto de números enteros como de números decimales, donde no se recibe otro dato más que un número positivo o negativo (con puntos que representan los decimales) y que son manejados por la **clase**  `float`, que contiene diversos métodos/funciones para el procesado de los números.
* **Booleanos:** es un tipo de dato que se compone de dos valores booleanos Falso o Verdadero (`True` o `False`), los cuales son manejados por la **clase**  `bool`, que contiene diversos métodos/funciones para el procesado de los valores booleanos.

A continuación se deja un ejemplo de código con la implementación de los distintos tipos de datos primitivos.

<table>
  <tr>
  </tr>
</table>

``` python
# Recibe cualquier carácter, siempre y cuando esté dentro de comillas.

cadena = str("ejemplo de un String")
print(cadena)

# Solo recibe números enteros positivos o negativos.
numero = int(-2)
print(numero)

# Solo recibe números decimales y enteros, positivos o negativos.
numero_decimal = float(-4.7)
print(numero_decimal)

# Recibe dos valores True o False.
booleano = bool(True)
print(booleano)
```

### Otros tipos de datos

Adicional a los tipos de datos primitivos, existen otros tipos de datos ampliamente usados en el dia a dia, alguno de ellos son:

* `list:` sirve para representar la estructura de datos de una **lista**, la cual contiene una una colección o secuencia de datos.Se dice que una lista es una estructura mutable porque además de permitir el acceso a los elementos, pueden suprimirse o agregarse nuevos.
* `tuple:` similar a la lista pero para representar una estructura de datos en una **tupla**. Permite tener agrupados un conjunto inmutable de elementos, es decir, no es posible agregar ni eliminar elementos.
* `set:` sirve para representar una estructura de **conjunto** de datos, el cual es una colección no ordenada de objetos únicos.
* `dict:` también llamado **diccionario**, es la forma de representar una estructura clave-valor, es la forma en que se puede representar un JSON en Python.

Acá un ejemplo práctico con este tipo de datos:

``` python
lista = [1, 2, 3, 8, 9]
tupla = (1, 4, 8, 0, 5)
conjunto = set([1, 3, 1, 4])
diccionario = {'a': 1, 'b': 3, 'z': 8}
```

## Operadores

Python a diferencia de otros lenguajes, maneja la definición (sintaxis) de los operadores (lógicos, aritméticos y de bit) de forma distinta, siendo ésta, un poco más explícita, a continuación se muestra los tipos de operadores de Python:

### Lógicos

Se caracterizan porque retornan un valor booleano.

<table>
  <tr>
   <td><strong>Operador</strong>
   </td>
   <td><strong>Descripción</strong>
   </td>
   <td><strong>Ejemplo</strong>
   </td>
  </tr>
  <tr>
   <td><strong>and</strong>
   </td>
   <td>¿se cumple a y b?
   </td>
   <td>True <strong>and</strong> False # es False
   </td>
  </tr>
  <tr>
   <td><strong>or</strong>
   </td>
   <td>¿se cumple a o b?
   </td>
   <td>True <strong>or</strong> False # es True
   </td>
  </tr>
  <tr>
   <td><strong>not</strong>
   </td>
   <td>Niega el valor
   </td>
   <td><strong>not</strong> True # es False
   </td>
  </tr>
</table>

**_Tabla 1. Operadores Lógicos_**

### Relacionales

Se caracterizan porque comparan dos o más sentencias y devuelven un valor booleano.

<table>
  <tr>
   <td><strong>Operador</strong>
   </td>
   <td><strong>Descripción</strong>
   </td>
   <td><strong>Ejemplo</strong>
   </td>
  </tr>
  <tr>
   <td><strong>==</strong>
   </td>
   <td>¿Son iguales a y b?
   </td>
   <td>5<strong>==</strong>3 # es False
   </td>
  </tr>
  <tr>
   <td><strong>!=</strong>
   </td>
   <td>¿Son distintos a y b?
   </td>
   <td>5<strong>!=</strong>3 # es True
   </td>
  </tr>
  <tr>
   <td><strong>&lt;</strong>
   </td>
   <td>¿Es a menor que b?
   </td>
   <td>5<strong>&lt;</strong>3 # es False
   </td>
  </tr>
  <tr>
   <td><strong>></strong>
   </td>
   <td>¿Es a mayor que b?
   </td>
   <td>5<strong>></strong>3  # es True
   </td>
  </tr>
  <tr>
   <td><strong>&lt;=</strong>
   </td>
   <td>¿Es a menor o igual que b?
   </td>
   <td>5<strong>&lt;=</strong>5 #  es True
   </td>
  </tr>
  <tr>
   <td><strong>>=</strong>
   </td>
   <td>¿Es a mayor o igual que b?
   </td>
   <td>5<strong>>=</strong>3 # es True
   </td>
  </tr>
</table>

**_Tabla 2. Operadores Relacionales_**

### Aritméticos

Se caracterizan por realizar operaciones aritméticas y retornar un valor entero o real.

<table>
  <tr>
   <td><strong>Operador</strong>
   </td>
   <td><strong>Descripción</strong>
   </td>
   <td><strong>Ejemplo</strong>
   </td>
  </tr>
  <tr>
   <td><strong>+</strong>
   </td>
   <td>Suma
   </td>
   <td>3<strong>+</strong>2 # el resultado es 5
   </td>
  </tr>
  <tr>
   <td><strong>-</strong>
   </td>
   <td>Resta
   </td>
   <td>4<strong>-</strong>7 # el resultado es -3
   </td>
  </tr>
  <tr>
   <td><strong>-</strong>
   </td>
   <td>Negación
   </td>
   <td><strong>-</strong>7 # el resultado es -7
   </td>
  </tr>
  <tr>
   <td><strong>*</strong>
   </td>
   <td>Multiplicación
   </td>
   <td>2<strong>*</strong>6 # el resultado es 12
   </td>
  </tr>
  <tr>
   <td><strong>**</strong>
   </td>
   <td>Exponente
   </td>
   <td>2<strong>**</strong>6 # el resultado es 64
   </td>
  </tr>
  <tr>
   <td><strong>/</strong>
   </td>
   <td>División
   </td>
   <td>3.5<strong>/</strong>2 # el resultado es 1.75
   </td>
  </tr>
  <tr>
   <td><strong>//</strong>
   </td>
   <td>División Entera
   </td>
   <td>3.5<strong>//</strong>2 # el resultado es 1.0
   </td>
  </tr>
  <tr>
   <td><strong>%</strong>
   </td>
   <td>Módulo
   </td>
   <td>7<strong>%</strong>2 # el resultado es 1
   </td>
  </tr>
</table>

**_Tabla 3. Operadores Aritméticos_**

>**Nota:** Algunos operadores sirven también para tipo `string`, por ejemplo ‘Ho’ + ‘la’, se corresponde con el string “Hola”. En este caso decimos que el operador “+” está sobrecargado.

>**Nota**: Existen otros tipos de operadores como los que operan a nivel de bit, los cuales reciben valores enteros y retornan otro valor entero.

## Estructuras De Control

Una estructura de control, es un bloque de código que permite agrupar instrucciones de manera controlada. Existen estructuras de control de decisión y estructuras de control de repetición, también llamadas condicionales e iterativas respectivamente.

### Estructuras de control de decisión o condicionales

#### Expresiones condicionales

Existen distintos tipos de expresiones condicionales que pueden ser usados en los condicionales `if` que veremos en este capítulo.

<table>
  <tr>
   <td><strong>Operador</strong>
   </td>
   <td><strong>Descripción</strong>
   </td>
   <td><strong>Ejemplo</strong>
   </td>
  </tr>
  <tr>
   <td><strong>Operadores Relacionales</strong>
   </td>
   <td>[Ver operadores relacionales](DOJO04_PYTHON_FUNDAMENTOS_FORENSICT.md#relacionales)
   </td>
   <td>[Ver operadores relacionales](DOJO04_PYTHON_FUNDAMENTOS_FORENSICT.md#relacionales)
   </td>
  </tr>
  <tr>
   <td><strong>in</strong>
   </td>
   <td>El operador <strong>in</strong>, significa, para cualquier colección del valor del lado izquierdo contenga el valor del lado derecho.
   </td>
   <td>$ b = [1, 2, 3]
<p>
$ 2 <strong>in</strong> b
<p>
True
<p>
$ 5 <strong>in</strong> b
<p>
False
   </td>
  </tr>
  <tr>
   <td><strong>not in</strong>
   </td>
   <td>El operador <strong>not in</strong>, el contrario de operador <strong>in</strong>, devuelve <strong>True</strong> cuando un elemento no está en una secuencia.
   </td>
   <td>$ b = [1, 2, 3]
<p>
$ 5 <strong>not in</strong> b 
<p>
True
<p>
$ 3 <strong>not in</strong> b 
<p>
False
   </td>
  </tr>
  <tr>
   <td><strong>is</strong>
   </td>
   <td>El operador <strong>is</strong>, significa, que prueba identidad: ambos lados de la expresión condicional deben ser el mismo objeto.
   </td>
   <td>$ a=3 
<p>
$ b=3 
<p>
$ a <strong>is</strong> b 
<p>
True
   </td>
  </tr>
</table>

**_Tabla 4. Expresiones Condiciones_**

#### Condicional if

La sentencia condicional `if` se usa para tomar decisiones, este evalúa básicamente una operación lógica, es decir una expresión que dé como resultado `True` o `False`.

La sintaxis de la construcción `if` es la siguiente:

``` python
if condición:
    aquí van las órdenes que se ejecutan si la condición es cierta
    y que pueden ocupar varias líneas
```

La ejecución de esta construcción es la siguiente:

* La condición se evalúa siempre.
  * Si el resultado es `True` se ejecuta el bloque de sentencias
  * Si el resultado es `False` no se ejecuta el bloque de sentencias.

<p align="center"><img src="images/f1_condicional_if.png" /></p>

<p align="center"><b>Figura 1</b>. Ciclo condicional If 

La primera línea contiene la condición a evaluar y es una expresión lógica. Esta línea debe terminar siempre por dos puntos ( : ). Es importante señalar que el bloque siguiente debe ser indentado, puesto que Python utiliza la indentación para reconocer las líneas que forman un bloque de instrucciones. Al escribir dos puntos ( : ) al final de una línea, el editor, generalmente,  indentará automáticamente las líneas siguientes. Para terminar un bloque, basta con volver al principio de la línea.

**Ejemplo if**

Escriba un programa que reciba un número y determine si el número es positivo:

``` python
numero = int(input("Escriba un número positivo: "))
if numero < 0:
    print("¡Le he dicho que escriba un número positivo!")
print(f"Ha escrito el número {numero}")
```

>**Nota**: La función `input` permite ingresar como parámetro una cadena de texto que se va a mostrar en consola, la cual se usará para indicar al usuario que es lo que requerimos que escriba.

>**Nota**: El parametro `f` que acompaña la cadena de texto dentro de la función `print` permite imprimir el valor de la variable dentro de las llaves `{}`.

El programa siguiente pide un número positivo al usuario y almacena la respuesta en la variable `numero`. Después comprueba si el número es negativo. Si lo es, el programa avisa que no era eso lo que se había pedido. Finalmente, el programa imprime siempre el valor introducido por el usuario.

Cuando ingresamos un número negativo obtenemos lo siguiente:

``` python
Escriba un número positivo: -4
¡Le he dicho que escriba un número positivo!
Ha escrito el número -4
```

Por el contrario, si escribimos un número positivo obtenemos lo siguiente:

``` python
Escriba un número positivo: 4
Ha escrito el número 4
```

#### Condicional If … else

La estructura de control `if` ... `else` ... permite que un programa ejecute unas instrucciones cuando se cumple una condición y otras instrucciones cuando no se cumple esa condición. En inglés "if" significa "si" (condición) y "else" significa "si no". La orden en Python se escribe así:

La sintaxis de la construcción `if` ... `else` ... es la siguiente:

``` python
if condición:
    aquí van las órdenes que se ejecutan si la condición es cierta
    y que pueden ocupar varias líneas
else:
    y aquí van las órdenes que se ejecutan si la condición es
    falsa y que también pueden ocupar varias líneas
```

La ejecución de esta construcción es la siguiente:

* La condición se evalúa siempre.
  * Si el resultado es `True` se ejecuta solamente el bloque de sentencias 1
  * Si el resultado es `False` se ejecuta solamente el bloque de sentencias 2.

<p align="center"><img src="images/f2_condicional_if_else.png" /></p>

<p align="center"><b>Figura 2</b>. Ciclo condicional If else

La primera línea contiene la condición a evaluar. Esta línea debe terminar siempre por dos puntos ( : ).

A continuación viene el bloque de órdenes que se ejecutan cuando la condición se cumple (es decir, cuando la condición es verdadera). Es importante señalar que este bloque debe ir indentado, puesto que Python utiliza la indentación para reconocer las líneas que forman un bloque de instrucciones.  El editor, generalmente,  indentará automáticamente las líneas siguientes.

Después viene la línea con la orden `else`, que indica a Python que el bloque que viene a continuación se tiene que ejecutar cuando la condición no se cumpla (es decir, cuando sea falsa). Esta línea también debe terminar siempre por dos puntos ( : ). La línea con la orden `else` no debe incluir nada más que el `else` y los dos puntos.

En último lugar está el bloque de instrucciones indentado que corresponde al else.

**Ejemplo if ... else …**

Escriba un programa que pregunte la edad al usuario y compruebe si es mayor de edad.

``` python
edad = int(input("¿Cuántos años tiene? "))
if edad < 18:
    print("Es usted menor de edad")
else:
    print("Es usted mayor de edad")
print("¡Hasta la próxima!")
```

El programa siguiente pregunta la edad al usuario y almacena la respuesta en la variable `edad`. Después comprueba si la edad es inferior a 18 años. Si esta comparación es cierta, el programa escribe que es menor de edad y si es falsa escribe que es mayor de edad.

Finalmente el programa siempre se despide, ya que la última instrucción está fuera de cualquier bloque y por tanto se ejecuta siempre.

Cuando ingresamos el número 17 obtenemos lo siguiente:

``` python
¿Cuántos años tiene? 17
Es usted menor de edad
¡Hasta la próxima!
```

Por el contrario, cuando ingresamos el número 22 obtenemos lo siguiente:

``` python
¿Cuántos años tiene? 22
Es usted mayor de edad
¡Hasta la próxima!
```

Aunque no es aconsejable, en vez de un bloque `if` ... `else` ... se podría escribir un programa con dos bloques `if` ...

``` python
edad = int(input("¿Cuántos años tiene? "))
if edad < 18:
    print("Es usted menor de edad")
if edad >= 18:
    print("Es usted mayor de edad")
print("¡Hasta la próxima!")
```

Es mejor no hacerlo así por varios motivos:

* Al poner dos bloques `if` estamos obligando a Python a evaluar siempre las dos condiciones, mientras que en un bloque `if` ... `else` ... sólo se evalúa una condición. En un programa sencillo la diferencia no es apreciable, pero en programas que ejecutan muchas condiciones, el impacto en el rendimiento puede ser considerable.
* Utilizando `else` nos ahorramos escribir una condición (además, escribiendo la condición nos podemos equivocar, pero escribiendo `else` no).
* Utilizando `if` ... `else` nos aseguramos de que se ejecute uno de los dos bloques de instrucciones. Utilizando dos `if` puede existir la posibilidad de que no se cumpliera ninguna de las dos condiciones y no se ejecute ninguno de los dos bloques de instrucciones.

#### Instrucción pass

Si por algún motivo no se quisiera ejecutar ninguna orden en alguno de los bloques, el bloque de órdenes debe contener al menos la instrucción `pass` (esta orden le dice a Python que no tiene que hacer nada).

``` python
edad = int(input("¿Cuántos años tiene? "))
if edad < 120:
    pass
else:
    print("¡No me lo creo!")
print(f"Usted dice que tiene {edad} años.")
```

Evidentemente este programa podría simplificarse cambiando la desigualdad.

``` python
edad = int(input("¿Cuántos años tiene? "))
if edad >= 120:
    print("¡No me lo creo!")
print(f"Usted dice que tiene {edad} años.")
```

El primer ejemplo era sólo un ejemplo para mostrar cómo se utiliza la instrucción `pass`. Normalmente, la instrucción `pass` se utiliza para "rellenar" un bloque de instrucciones que todavía no hemos escrito y poder ejecutar el programa, ya que Python requiere que se escriba alguna instrucción en cualquier bloque.

#### Sentencias condicionales anidadas

Una sentencia condicional puede contener a su vez otra sentencia anidada. Por ejemplo, el programa siguiente muestra el color obtenido al mezclar dos colores en la pantalla:

Ejemplo de sentencias condicionales anidadas

``` python
print("Este programa mezcla dos colores.")
print("  r. Rojo      a. Azul")
primera = input("  Elija un color (r o a): ")
if primera == "r":
    print("  a. Azul      v. Verde")
    segunda = input("  Elija otro color (a o v): ")
    if segunda == "a":
        print("La mezcla de Rojo y Azul produce Magenta.")
    else:
        print("La mezcla de Rojo y Verde produce Amarillo.")
else:
    print("  v. Verde    r. Rojo")
    segunda = input("  Elija otro color (v o r): ")
    if segunda == "v":
        print("La mezcla de Azul y Verde produce Cian.")
    else:
        print("La mezcla de Azul y Rojo produce Magenta.")
print("¡Hasta la próxima!")
```

#### Sentencia if ... elif ... else ...

La estructura de control `if` ... `elif` ... `else` ... permite encadenar varias condiciones. `elif` es una contracción de `else if`. La orden en Python se escribe así:

``` python
if condición_1:
    bloque 1
elif condición_2:
    bloque 2
else:
    bloque 3
```

* Si se cumple la condición 1, se ejecuta el bloque 1
* Si no se cumple la condición 1 pero sí que se cumple la condición 2, se ejecuta el bloque 2
* Si no se cumplen ni la condición 1 ni la condición 2, se ejecuta el bloque 3.

Esta estructura es equivalente a la siguiente estructura de `if` ... `else` ... anidados:

``` python
if condición_1:
    bloque 1
else:
    if condición_2:
        bloque 2
    else:
        bloque 3
```

Se pueden escribir tantos bloques `elif` como sean necesarios. El bloque `else` (que es opcional) se ejecuta si no se cumple ninguna de las condiciones anteriores. En la figura 3 se muestra de una manera mas gráfica el funcionamiento de este ciclo.

<p align="center"><img src="images/f3_condicional_if_elif_else.png" /></p>

<p align="center"><b>Figura 3</b>. Ciclo condicional If elif else

En las estructuras `if` ... `elif` ... `else` ... el orden en que se escriben los casos es importante y, a menudo, se pueden simplificar las condiciones ordenando adecuadamente los casos.

Podemos distinguir dos tipos de situaciones:

**1. Cuando los casos son mutuamente excluyentes.**

Consideremos un programa que pide la edad y en función del valor recibido da un mensaje diferente. Podemos distinguir, por ejemplo, tres situaciones:

* Si el valor es negativo, se trata de un error
* Si el valor está entre 0 y 17, se trata de un menor de edad
* Si el valor es superior o igual a 18, se trata de un mayor de edad

Los casos son mutuamente excluyentes, ya que un valor sólo puede estar en uno de los casos.

Un posible programa es el siguiente:

``` python
edad = int(input("¿Cuántos años tiene? "))
if edad < 0:
    print("No se puede tener una edad negativa")
elif edad < 18:
    print("Es usted menor de edad")
else:
    print("Es usted mayor de edad")
```

Hay que tener cuidado, porque si los casos del programa anterior se ordenan al revés manteniendo las condiciones, el programa no funcionará como se espera, puesto que al escribir un valor negativo mostraría el mensaje "Es usted menor de edad".

``` python
# Este programa no funciona correctamente
edad = int(input("¿Cuántos años tiene? "))
if edad < 18:
    print("Es usted menor de edad")
elif edad < 0:
    print("No se puede tener una edad negativa")
else:
    print("Es usted mayor de edad")
```

**2. Cuando unos casos incluyen a otros**.

Consideremos un programa que pide un valor y nos dice:
    1. si es múltiplo de dos,
    2. si es múltiplo de cuatro (y de dos)
    3. si no es múltiplo de dos

>**Nota**: El valor 0 se considerará múltiplo de 4 y de 2.
Los casos no son mutuamente excluyentes, puesto que los múltiplos de cuatro son también múltiplos de dos. El siguiente programa no sería correcto:

``` python
# Este programa no funciona correctamente
numero = int(input("Escriba un número: "))
if numero % 2 == 0:
    print(f"{numero} es múltiplo de dos")
elif numero % 4 == 0:
    print(f"{numero} es múltiplo de cuatro y de dos")
else:
    print(f"{numero} no es múltiplo de dos")
```

El error de este programa es que si un número cumple la segunda condición, cumple también la primera. Es decir, si un número es un múltiplo de cuatro, como también es múltiplo de dos, cumple la primera condición y el programa ejecuta el primer bloque de instrucciones, sin llegar a comprobar el resto de condiciones.

Una manera de corregir ese error es añadir en la primera condición (la de if) que número no sea múltiplo de cuatro.

``` python
numero = int(input("Escriba un número: "))
if numero % 2 == 0 and numero % 4 != 0:
    print(f"{numero} es múltiplo de dos")
elif numero % 2 == 0:
    print(f"{numero} es múltiplo de cuatro y de dos")
else:
    print(f"{numero} no es múltiplo de dos")
```

Este programa funciona porque los múltiplos de cuatro también son múltiplos de dos y el programa sólo evalúa la segunda condición (la de `elif`) si no se ha cumplido la primera.

Pero podemos simplificar más el programa, ordenando de otra manera los casos:

``` python
numero = int(input("Escriba un número: "))
if numero % 4 == 0:
    print(f"{numero} es múltiplo de cuatro y de dos")
elif numero % 2 == 0:
    print(f"{numero} es múltiplo de dos")
else:
    print(f"{numero} no es múltiplo de dos")
```

Este programa funciona correctamente ya que aunque la segunda condición (la de elif) no distingue entre múltiplos de dos y de cuatro, si número es un múltiplo de cuatro, el programa no llega a evaluar la segunda condición porque se cumple la primera (la de if).

En general, el orden que permite simplificar más las expresiones suele ser considerar primero los casos particulares y después los casos generales.

Si los condicionales `if` ... `elif` ... cubren todas las posibilidades, se puede sustituir el último bloque `elif` ... por un bloque `else`:

``` python
numero = int(input("Escriba un número: "))
if numero >= 0:
    print("Ha escrito un número positivo")
elif numero < 0:
    print("Ha escrito un número negativo")
```

``` python
numero = int(input("Escriba un número: "))
if numero >= 0:
    print("Ha escrito un número positivo")
else:
    print("Ha escrito un número negativo")
```

### Estructuras de control de repetición o iterativas

#### Ciclo for

En general, un ciclo es una estructura de control que repite un bloque de instrucciones. Un ciclo `for` es un ciclo que repite el bloque de instrucciones un número predeterminado de veces. El bloque de instrucciones que se repite se suele llamar cuerpo del ciclo y cada repetición se suele llamar iteración.

La sintaxis de un ciclo `for` es la siguiente:

``` python
for variable in elemento_iterable (lista, cadena, range, etc.):
    cuerpo del ciclo
```

>**Nota:** La función `range()` retorna una secuencia de números, empezando por 0 por defecto e incrementando de a 1 dígito hasta un número dado
Ejemplo:  `range(6)` retorna los valores de 0 a 5, es decir el último dígito no se incluye.
`range(2, 6)` retorna los valores de 2 a 5.

No es necesario definir la variable de control antes del ciclo, aunque se puede utilizar como variable de control una variable ya definida en el programa.

**Ejemplo**

``` python
print("Comienzo")
for i in [0, 2, 1]:
    print("Hola ", end="")
print()
print("Final")
```

>**Nota**: Si se quiere que Python no añada un salto de línea al final de un `print()`, se debe añadir al final el argumento `end=""`.

Los valores que toma la variable no son importantes, lo que importa es que la lista tiene tres elementos y por tanto el ciclo se ejecuta tres veces. El resultado será el siguiente:

``` python
Comienzo
Hola Hola Hola
Final
```

**Ejemplo**

``` python
print("Comienzo")
for i in []:
    print("Hola ", end="")
print()
print("Final")
```

Si la lista está vacía, el ciclo no se ejecuta ninguna vez:

``` python
Comienzo

Final
```

Si la variable de control no se va a utilizar en el cuerpo del ciclo, como en los ejemplos anteriores, se puede utilizar el guión (_) en vez de un nombre de variable. Esta notación no tiene ninguna consecuencia con respecto al funcionamiento del programa, pero sirve de ayuda a la persona que esté leyendo el código fuente, que sabe así que los valores no se van a utilizar. Por ejemplo:

**Ejemplo**

``` python
print("Comienzo")
for _ in [0, 1, 2]:
    print("Hola ", end="")
print()
print("Final")
```

El resultado no cambia:

``` python
Comienzo
Hola Hola Hola
Final
```

El indicador puede incluir cualquier número de guiones bajos (_, __, ___, ____, etc). Los más utilizados son uno o dos guiones (_ o __).

En los ejemplos anteriores, la variable de control `i` no se utilizaba en el bloque de instrucciones, pero en muchos casos sí que se utiliza. Cuando se utiliza, hay que tener en cuenta que la variable de control va tomando los valores del elemento recorrible. Por ejemplo:

**Ejemplo**

``` python
print("Comienzo")
for i in [3, 4, 5]:
    print(f"Hola. Ahora i vale {i} y su cuadrado {i ** 2}")
print("Final")
```

Su resultado será el siguiente:

``` python
Comienzo
Hola. Ahora i vale 3 y su cuadrado 9
Hola. Ahora i vale 4 y su cuadrado 16
Hola. Ahora i vale 5 y su cuadrado 25
Final
```

##### Contadores

Se entiende por contador una variable que lleva la cuenta del número de veces que se ha cumplido una condición. El ejemplo siguiente es un ejemplo de programa con contador (en este caso, la variable que hace de contador es la variable cuenta):

Ejemplo de contador

``` python
print("Comienzo")
cuenta = 0
for i in range(1, 6):
    if i % 2 == 0:
        cuenta = cuenta + 1
print(f"Desde 1 hasta 5 hay {cuenta} múltiplos de 2")
```

* En cada iteración, el programa comprueba si `i` es múltiplo de 2.
* El contador se modifica sólo si la variable de control `i` es múltiplo de 2.
* El contador va aumentando de uno en uno.
* Antes del ciclo se debe dar un valor inicial al contador (en este caso, 0)

``` python
Comienzo
Desde 1 hasta 5 hay 2 múltiplos de 2
```

##### Testigos

Se entiende por testigo una variable que indica simplemente si una condición se ha cumplido o no. Es un caso particular de contador, pero se suele hacer con variables lógicas en vez de numéricas (en este caso, la variable que hace de testigo es la variable `encontrado`):

Ejemplo de testigo

``` python
print("Comienzo")
encontrado = False
for i in range(1, 6):
    if i % 2 == 0:
        encontrado = True
if encontrado:
    print(f"Entre 1 y 5 hay al menos un múltiplo de 2.")
else:
    print(f"Entre 1 y 5 no hay ningún múltiplo de 2.")
```

* En cada iteración, el programa comprueba si `i` es múltiplo de 2.
* El testigo se modifica la primera vez que la variable de control `i` es múltiplo de 2.
* El testigo no cambia una vez ha cambiado.
* Antes del ciclo se debe dar un valor inicial al testigo (en este caso, False)

``` python
Comienzo
Entre 1 y 5 hay al menos un múltiplo de 2.
```

##### Acumulador

Se entiende por acumulador una variable que acumula el resultado de una operación. El ejemplo siguiente es un ejemplo de programa con acumulador (en este caso, la variable que hace de acumulador es la variable `suma`):

Ejemplo de acumulador

``` python
print("Comienzo")
suma = 0
for i in [1, 2, 3, 4]:
    suma = suma + i
print(f"La suma de los números de 1 a 4 es {suma}")
```

El acumulador se modifica en cada iteración del ciclo (en este caso, el valor de `i` se añade al acumulador suma).

Antes del ciclo se debe dar un valor inicial al acumulador (en este caso, 0)

``` python
Comienzo
La suma de los números de 1 a 4 es 10
```

#### Ciclos for anidados

Se habla de ciclos anidados cuando un ciclo se encuentra en el bloque de instrucciones de otro ciclo. Al ciclo que se encuentra dentro del otro se le puede denominar ciclo interior o ciclo interno. El otro ciclo sería el ciclo exterior o ciclo externo. Los ciclos pueden tener cualquier nivel de anidamiento (un ciclo dentro de otro ciclo dentro de un tercero, etc.).

Aunque en Python no es necesario, se recomienda que los nombres de las variables de control de los ciclos anidados no coincidan, para evitar ambigüedades.

##### Ciclos anidados (variables independientes)

Se dice que las variables de los ciclos son independientes cuando los valores que toma la variable de control del ciclo interno no dependen del valor de la variable de control del ciclo externo. Por ejemplo:

Ejemplo de ciclo anidado (variables independientes)

``` python
for i in [0, 1, 2]:
    for j in [0, 1]:
        print(f"i vale {i} y j vale {j}")
```

El ciclo externo (el controlado por `i`) se ejecuta 3 veces y el ciclo interno (el controlado por `j`) se ejecuta dos veces por cada valor de `i`. Por ello la instrucción print() se ejecuta en total 6 veces (3 veces que se ejecuta el ciclo externo x 2 veces que se ejecuta cada vez el ciclo interno = 6 veces).

En general, el número de veces que se ejecuta el bloque de instrucciones del ciclo interno es el producto de las veces que se ejecuta cada ciclo.

``` python
i vale 0 y j vale 0
i vale 0 y j vale 1
i vale 1 y j vale 0
i vale 1 y j vale 1
i vale 2 y j vale 0
i vale 2 y j vale 1
```

En el ejemplo anterior se han utilizado listas para facilitar la comprensión del funcionamiento del ciclo pero, si es posible hacerlo, se recomienda utilizar tipos `range()`, entre otros motivos porque durante la ejecución del programa ocupan menos memoria en el ordenador y se pueden hacer depender del desarrollo del programa.

El siguiente programa es equivalente al ejemplo anterior:

``` python
for i in range(3):
    for j in range(2):
        print(f"i vale {i} y j vale {j}")
```

Al escribir ciclos anidados, hay que prestar atención al indentado de las instrucciones, ya que esa indentación indica a Python si una instrucción forma parte de un bloque u otro. Verifique los resultados de los siguientes programas:

Ejemplo de indentación en ciclo anidado (1)

``` python
for i in [1, 2, 3]:
    for j in [11, 12]:
        print(j, end=" ")
        print(i, end=" ")
```

Resultado:

``` python
11 1 12 1 11 2 12 2 11 3 12 3
```

Ejemplo de indentación en ciclo anidado (2)

``` python
for i in [1, 2, 3]:
    for j in [11, 12]:
        print(j, end=" ")
    print(i, end=" ")
```

Resultado:

``` python
11 12 1 11 12 2 11 12 3
```

Ejemplo de indentación en ciclo anidado (3)

``` python
for i in [1, 2, 3]:
    for j in [11, 12]:
        print(j, end=" ")
print(i, end=" ")
```

Resultado

``` python
11 12 11 12 11 12 3
```

La más común es utilizar la letra `i` como nombre de la variable de control del ciclo externo y la letra `j` como nombre de la variable de control del ciclo interno (o `k` si hay un tercer nivel de anidamiento), pero se puede utilizar cualquier otro nombre válido.

##### Ciclos anidados (variables dependientes)

Se dice que las variables de los ciclos son dependientes cuando los valores que toma la variable de control del ciclo interno dependen del valor de la variable de control del ciclo externo. Por ejemplo:

Ejemplo de ciclo anidado (variables dependientes) 1

``` python
for i in [1, 2, 3]:
    for j in range(i):
        print(f"i vale {i} y j vale {j}")
```

En el ejemplo anterior, el ciclo externo (el controlado por `i`) se ejecuta 3 veces y el ciclo interno (el controlado por `j`) se ejecuta 1, 2 y 3 veces. Por ello la instrucción `print()` se ejecuta en total 6 veces. La variable `i` toma los valores de 1 a 3 y la variable `j` toma los valores de 0 a `i`, por lo que cada vez el ciclo interno se ejecuta un número diferente de veces:

* Cuando `i` vale 1, `range(i)` devuelve la lista [0] y por tanto el ciclo interno se ejecuta una sola vez y el programa escribe una sola línea en la que `i` vale 1 (y `j` vale 0).
* Cuando `i` vale 2,`range(i)` devuelve la lista [0, 1] y por tanto el ciclo interno se ejecuta dos veces y el programa escribe dos líneas en la que i vale 2 (y `j` vale 0 o 1 en cada una de ellas).
* Cuando `i` vale 3, `range(i)` devuelve la lista [0, 1, 2] y por tanto el ciclo interno se ejecuta tres veces y el programa escribe tres líneas en la que i vale 3 (y `j` vale 0, 1 o 2 en cada una de ellas).

``` python
i vale 1 y j vale 0
i vale 2 y j vale 0
i vale 2 y j vale 1
i vale 3 y j vale 0
i vale 3 y j vale 1
i vale 3 y j vale 2
```

#### Ciclos while

Un ciclo `while` permite repetir la ejecución de un grupo de instrucciones mientras se cumpla una condición (es decir, mientras la condición tenga el valor True). La sintaxis del ciclo `while` es la siguiente:

``` python
while condicion:
    cuerpo del ciclo
```

La ejecución de esta estructura de control `while` es la siguiente:

Python evalúa la condición:

* Si el resultado es True se ejecuta el cuerpo del ciclo. Una vez ejecutado el cuerpo del ciclo, se repite el proceso (se evalúa de nuevo la condición y, si es cierta, se ejecuta de nuevo el cuerpo del ciclo) una y otra vez mientras la condición sea cierta.
* Si el resultado es False, el cuerpo del ciclo no se ejecuta y continúa la ejecución del resto del programa.

La variable o las variables que aparezcan en la condición se suelen llamar variables de control. Las variables de control deben definirse antes del ciclo `while` y modificarse en el ciclo `while`.

En la figura 4 se ejemplifica de manera general el control de un ciclo `while`, donde el bloque se ejecutará siempre que la condición sea verdadera en caso contrario el ciclo llegará a su fin.

<p align="center"><img src="images/f4_ciclo_while.png" /></p>

<p align="center"><b>Figura 4</b>. Ciclo while

Por ejemplo, el siguiente programa escribe los números del 1 al 3:

``` python
i = 1
while i <= 3:
    print(i)
    i += 1
print("Programa terminado")
```

El resultado es el siguiente:

``` python
1
2
3
Programa terminado
```

El ejemplo anterior se podría haber programado con un ciclo `for`. La ventaja de un ciclo `while` es que la variable de control se puede modificar con mayor flexibilidad, como en el ejemplo siguiente:

``` python
i = 1
while i <= 50:
    print(i)
    i = 3 * i + 1
print("Programa terminado")
```

El resultado es el siguiente:

``` python
1
4
13
40
Programa terminado
```

Otra ventaja del ciclo `while` es que el número de iteraciones no está definida antes de empezar el ciclo, puede ser porque los datos los proporciona el usuario. Por ejemplo, el siguiente ejemplo pide un número positivo al usuario una y otra vez hasta que el usuario lo haga correctamente:

Ejemplo de ciclo `while` 3

``` python
numero = int(input("Escriba un número positivo: "))
while numero < 0:
    print("¡Ha escrito un número negativo! Inténtelo de nuevo")
    numero = int(input("Escriba un número positivo: "))
print("Gracias por su colaboración")
```

Observemos el siguiente resultado:

``` python
Escriba un número positivo: -4
¡Ha escrito un número negativo! Inténtelo de nuevo
Escriba un número positivo: -8
¡Ha escrito un número negativo! Inténtelo de nuevo
Escriba un número positivo: 9
Gracias por su colaboración
```

#### Ciclos infinitos

Si la condición del ciclo se cumple siempre, el ciclo no terminará nunca de ejecutarse y tendremos lo que se denomina un ciclo infinito. Aunque a veces es necesario utilizar ciclos infinitos en un programa, normalmente se deben a errores que se deben corregir.

Los ciclos infinitos no intencionados deben evitarse pues significan perder el control del programa. Para interrumpir un ciclo infinito, hay que pulsar la combinación de teclas Ctrl+C. Al interrumpir un programa se mostrará un mensaje de error similar a éste:

``` python
Traceback (most recent call last):
  File "ejemplo.py", line 3, in <module>
    print(i)
KeyboardInterrupt
```

Por desgracia, es fácil programar involuntariamente un ciclo infinito, por lo que es inevitable hacerlo de vez en cuando, sobre todo cuando se está aprendiendo a programar. A continuación se muestran algunos ejemplos de ciclos infinitos:

* El programador ha olvidado modificar la variable de control dentro del ciclo y el programa imprimirá números 1 indefinidamente:

``` python
i = 1
while i <= 10:
    print(i, end=" ")
```

* El programador ha escrito una condición que se cumplirá siempre y el programa imprimirá números consecutivos indefinidamente:

``` python
i = 1
while i > 0:
    print(i, end=" ")
    i += 1
```

Resultado

``` python
1 2 3 4 5 6 7 8 9 10 11 ...
```

* Se aconseja expresar las condiciones como desigualdades en vez de comparar valores. En el ejemplo siguiente, el programador ha escrito una condición que se cumplirá siempre y el programa imprimirá números consecutivos indefinidamente:

``` python
i = 1
while i != 100:
    print(i, end=" ")
    i += 2
```

Resultado:

``` python
1 3 5 7 9 11 ... 97 99 101 ...
```

## Manejo de errores y excepciones

Existen dos tipos diferentes de errores: errores de sintaxis y excepciones.

* **Errores de sintaxis**: también conocidos como errores de interpretación.

``` python
> while True print('Hello world')
  File "<stdin>", line 1
    while True print('Hello world')
               ^
SyntaxError: invalid syntax
```

El intérprete reproduce la línea responsable del error y muestra una pequeña “flecha” que apunta al primer lugar donde se detectó el error. El error ha sido provocado (o al menos detectado) en el elemento que precede a la flecha: en el ejemplo, el error se detecta en la función print(), ya que faltan dos puntos (`:`) antes del mismo. Se muestran el nombre del archivo y el número de línea para que sepas dónde mirar en caso de que la entrada venga de un programa.

* **Excepciones**: Incluso si una declaración o expresión es sintácticamente correcta, puede generar un error cuando se intenta ejecutar. Los errores detectados durante la ejecución se llaman excepciones, y no son incondicionalmente fatales.

``` python
> 10 * (1/0)
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
ZeroDivisionError: division by zero
> 4 + spam*3
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
NameError: name 'spam' is not defined
> '2' + 2
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
TypeError: Can't convert 'int' object to str implicitly
```

La última línea de los mensajes de error indica qué ha sucedido. Hay excepciones de diferentes tipos, y el tipo se imprime como parte del mensaje. Los tipos en el ejemplo son `ZeroDivisionError`, `NameError` y `TypeError`.

La cadena mostrada como tipo de la excepción es el nombre de la excepción predefinida que ha ocurrido. Esto es válido para todas las excepciones predefinidas del intérprete, pero no tiene por que ser así para excepciones definidas por el usuario (aunque es una convención útil). Los nombres de las excepciones estándar son identificadores incorporados al intérprete (no son palabras clave reservadas). El resto de la línea provee información basado en el tipo de la excepción y qué la causó.

### Gestionando excepciones

Es posible escribir programas que gestionen determinadas excepciones. Véase el siguiente ejemplo, que le pide al usuario una entrada hasta que ingrese un entero válido:

``` python
while True:
 try:
   x = int(input("Por favor ingrese un número: "))
   break
 except ValueError:
   print("Oops!  Ese no es un número válido, intenta de nuevo...")
```

Si ingresamos una letra por ejemplo, se ejecutará el bloque de código que hace parte del `except`.

La declaración `try` funciona de la siguiente manera:

* Primero, se ejecuta la cláusula `try` (la(s) línea(s) entre las palabras reservadas `try` y la `except`).
* Si no ocurre ninguna excepción, la cláusula `except` se omite y la ejecución de la cláusula `try` finaliza.
* Si ocurre una excepción durante la ejecución del bloque `try`, el resto del bloque se omite. Luego, si su tipo coincide con la excepción nombrada luego de la palabra reservada `except`, se ejecuta el bloque `except`, y la ejecución continúa luego de la sentencia `try`.
* Si ocurre una excepción que no coincide con la excepción nombrada en el `except`, esta se pasa a declaraciones `try` de más afuera; si no se encuentra nada que la maneje, es una excepción no manejada, y la ejecución se frena con un mensaje como los mostrados arriba.

En el caso de que el usuario interrumpa el programa (usando Control+C o lo que soporte el sistema operativo); nótese que la interrupción generada por el usuario es señalizada generando la excepción KeyboardInterrupt.

``` python
Please enter a number: Traceback (most recent call last):
  File "main.py", line 3, in <module>
    x = int(input("Por favor ingrese un número: "))
KeyboardInterrupt
```

Una sentencia `try` puede tener más de un `except`, para especificar manejadores para distintas excepciones. A lo sumo un manejador será ejecutado. Sólo se manejan excepciones que ocurren en el correspondiente `try`, no en otros manejadores del mismo try. Un `except` puede nombrar múltiples excepciones usando paréntesis, por ejemplo:

``` python
while True:
  try:
    x = int(input("Por favor ingrese un número: "))
    break
  except ValueError:
    print("Oops!  Ese no es un número válido, intenta de nuevo...")
  except (RuntimeError, TypeError,NameError,KeyboardInterrupt):
    print("Error inesperado")
    break
```

El último `except` puede omitir nombrar qué excepción captura, para servir como comodín. Use esto con extremo cuidado, ya que de esta manera es fácil ocultar un error real de programación. También puede usarse para mostrar un mensaje de error y luego re-generar la excepción (permitiéndole al que llama, manejar también la excepción):

``` python
while True:
 try:
   x = int(input("Por favor ingrese un número: "))
   break
 except ValueError:
   print("Oops!  Ese no es un número válido, intenta de nuevo...")
 except:
   print("Error inesperado")
   break
```

### Levantando excepciones

La sentencia `raise` permite al programador forzar a que ocurra una excepción intencional. Por ejemplo:

``` python
$ raise NameError('Hola')
Traceback (most recent call last):
  File "<stdin>", line 1, in ?
NameError: Hola
```

A `raise`  se le puede especificar una **clase**  de excepción definida donde se le indica la excepción a generarse, este parametro puede ser opcional.

Si se requiere determinar cuando una excepción fue lanzada pero no se quiere manejarla, una forma simplificada de la sentencia `raise` permite relanzarla:

``` python
number = int(input("Por favor ingrese un número menor a 10: "))
try:
   if number < 10:
       print('Número válido')
   elif number==10:
       raise ValueError('Número no valido')
   else:
       raise KeyError('dígitos no válidos')
except KeyError as error:
   print('estoy en el error', error)
except ValueError as  error:
   print('ValueError', error)
```

## Funciones

### ¿Qué son las funciones en Python?

Una función, es la forma de agrupar expresiones y sentencias (algoritmos) que realicen determinadas acciones, pero que éstas, solo se ejecuten cuando son llamadas. Es decir, al colocar un algoritmo dentro de una función, al correr el archivo, el algoritmo no será ejecutado si no se ha hecho una referencia a la función que lo contiene.

Las funciones en Python constituyen unidades lógicas de un programa y tienen un doble objetivo:

* Dividir y organizar el código en partes más sencillas.

* Encapsular el código que se repite a lo largo de un programa para ser reutilizado.

Python ya define de serie un conjunto de funciones que se pueden utilizar directamente en las aplicaciones en construcción. Algunas de ellas son: la función `len()`, que obtiene el número de elementos de un **objeto** contenedor como una lista, una tupla, un diccionario o un conjunto. También la función `print()`, que muestra por consola un texto.

Sin embargo, se pueden definir funciones propias para estructurar el código de manera que sea más legible y para reutilizar aquellas partes que se repiten a lo largo de una aplicación. Esto es una tarea fundamental a medida que va creciendo el número de líneas de un programa.

En principio, un programa es una secuencia ordenada de instrucciones que se ejecutan una a continuación de la otra. Sin embargo, cuando se utilizan funciones, puedes agrupar parte de esas instrucciones como una unidad más pequeña que ejecuta dichas instrucciones y suele devolver un resultado.

### Estructura de una función

Para definir una función en python se debe seguir la estructura expuesta en la figura 5:

<p align="center"><img src="images/f5_estructura_funcion.png" /></p>

<p align="center"><b>Figura 5</b>. Estructura de una función

Para definir una función en Python se utiliza la palabra reservada `def`. A continuación viene el nombre o identificador de la función que es el que se utiliza para invocarla. Después del nombre hay que incluir los paréntesis y una lista opcional de parámetros. Por último, la cabecera o definición de la función termina con dos puntos.

Tras los dos puntos se incluye el cuerpo de la función (con un indentado mayor) que no es más que el conjunto de instrucciones que se encapsulan en dicha función y que le dan significado.

En último lugar y de manera opcional, se añade la instrucción con la palabra reservada `return` para devolver un resultado.

Un ejemplo sencillo de una función puede ser la siguiente, donde se define y se utiliza la función “multiplicación”:

``` python
#!/usr/bin/env python
# -*- coding: utf-8 -*-

num1 = int(input("Introduce un número: "))
num2 = int(input("Introduce otro número: "))

def multiplicacion(a,b):
   return a*b

resultado = multiplicacion(num1,num2)
print(str(resultado))
```

## Clases

Una **clase**  en Python es una plantilla o molde el cual es el punto de partida para la creación de objetos. Esta plantilla contiene la información, características y capacidades que el **objeto** tendrá cuando sea creado a partir de ella. Además, todos los objetos creados a partir de esta plantilla estarán agrupados a la misma clase.

Ahora bien, para lograr comprender de una manera más clara la estructura y funcionamiento de una clase, se deben tener claros los siguientes conceptos:

Un **objeto** puede definirse como un elemento que tiene un comportamiento, un estado, puede realizar acciones y almacena información. Además, pueden existir objetos que no hayan sido creados de una clase. En pocas palabras, todo es un **objeto** en el sentido de que se puede asignar a una variable o ser pasado como argumento a una función.

La acción de crear un **objeto** a partir de una **clase**  se denomina instanciar. En este punto se inicializa el método `__init__`que es un  **constructor**, el cual tiene como objetivo recibir unos valores iniciales para establecerlos en sus posibles atributos internos.

Las características que se crean para describir un **objeto** se llaman atributos y las tareas que se pueden llegar a realizar se denominan métodos (que también se puede definir como funciones).

Un ejemplo, de manera general, puede ser el siguiente:

* Se denomina la **clase**  Estudiante

* El **objeto** instanciado a partir de la **clase**  es: Pepito Perez

* Tiene los siguientes atributos:

  * Altura= 1.75 metros

  * Peso= 80 Kilos

  * Color de cabello= Negro

* Tiene los siguientes métodos:

  * `Corre (run)`

  * `Salta ()`

  * `Come (desayuno, almuerzo, cena)`

  * `Duerme ()`

Cabe recordar que estos atributos y métodos son definidos dentro de la clase, al momento de crear la **clase**  y luego al momento de instanciar se definen los argumentos para estos atributos y métodos.

Para comenzar a definir una **clase**  en Python se debe iniciar por la palabra `class` seguido del nombre que se le va a asignar a la clase, luego se describen los atributos y los métodos de esa clase.

Un ejemplo puede ser el siguiente:

``` python
#!/usr/bin/env python
# -*- coding: utf-8 -*-

class Estudiante:

    # Constructor de la clase
    def __init__(self, nombre, altura, peso, color_cabello):
        self.nombre = nombre
        self.altura = altura
        self.peso = peso
        self.color_cabello = color_cabello

    def correr(self):
        print('Estoy corriendo')
    def saltar(self):
        print('Estoy Saltando')
    def dormir(self):
        print('zzz zzz zzz')

#instancia de la clase Estudiante
estudiante = Estudiante(nombre="Pepito Perez", altura=1.75, peso=80,color_cabello= "negro")
# Imprimir = Atributos
print(estudiante.nombre)
print(estudiante.altura)
print(estudiante.peso)
print(estudiante.color_cabello)
# Llamar Métodos
estudiante.correr()
estudiante.saltar()
estudiante.dormir()
```

## Módulos y Paquetes

En Python, la estructuración y división de código es muy frecuente, es por ello que hay dos tipos de división importantes: Módulos y Paquetes.

>**Nota**: Antes de importar los modulos y paquetes se debe crear el sistema de archivos que esta en las graficas anteriores para que los ejemplos funcionen adecuadamente.

### Módulos

Son porciones de código que están contenidos en archivos con extensión py (ejem. main.py), los cuales pueden tener funciones, variables o clases. Generalmente, estos módulos son llamados por otros módulos o por otros paquetes, sin embargo, se pueden llamar desde otras aplicaciones usando una ruta relativa o ruta absoluta.

<p align="center"><img src="images/Salida_modulos.png" /></p>

### Paquetes

Un paquete es un conjunto de varios módulos contenidos en un mismo lugar (Carpeta). Similar a los módulos, los paquetes pueden ser llamados por otros módulos o por otros paquetes, sin embargo, se pueden llamar desde otras aplicaciones usando una ruta relativa o ruta absoluta.

<p align="center"><img src="images/Salida_paquetes.png" /></p>

### Importar módulos o paquetes

El llamado de módulos y paquetes, es distinto, para llamar un módulo que no está contenido en un paquete se puede llamar de la siguiente forma:

``` python
# Forma 1 de importar un módulo
import module_two

print(module_two.message())

# Forma 2 de importar una función de un módulo
from module_two import message as hello_message

print(hello_message())
```

Donde el `modulo_two` es el módulo que se quiere importar. En la **forma 1** de importar, se importa todo el contenido (clases, funciones, código, etc) del `modulo_two`. En la **forma 2** de importar, se usa para importar SOLO el método de `message.`

>**Nota:** en la `forma 2` se usa una palabra clave `as`, la cual se usa para crear un alias al método que se llama, en este caso `message` se renombra a `hello_message` sin afectar su funcionalidad.

Para importar un módulo que está contenido en un paquete, se puede llamar de la siguiente forma:

``` python
# Forma importar una función contenida en un módulo
# que está en un paquete
from Package.module_three import good_bye as bye

print(bye())

# Forma de importar un módulo con todas sus funciones
# que está contenido en un paquete
from Package import module_three

print(module_three.good_bye())
```

>**Nota:** las librerías nativas en Python como `base64` se importan de la misma forma que un módulo o un paquete, es decir, se puede importar una función específica de la librería o todo el conjunto de funcionalidades.

## Manejo De Archivos

El manejo de archivos es un tema muy común a la hora de programar en Python, ya sea para leer un conjunto de datos, para crear logs, modificar el contenido de un archivo, entre otros. Pues bien, para el enfoque que se le quiere dar (creación de scripts o exploits), se necesitan algunos conocimientos básicos del manejo de archivos.

Para la lectura de archivos, es necesario utilizar la función `open()`, la cual recibe dos parámetros principales que son `file` y `mode`, esta función procesa los archivos como un **objeto** tipo `file` (No confundir con el parámetro). El parámetro `file` recibe el nombre del archivo (si el archivo se encuentra en el mismo directorio del archivo ejecutable .py) o la ruta absoluta y/o relativa asociada a este, por otro lado, `mode` permite definir en qué modo se va a ejecutar el archivo (Lectura, escritura, binario, etc.). Los modos se presentan en la siguiente tabla:

<table>
  <tr>
   <td><strong>Modo</strong>
   </td>
   <td><strong>Notas</strong>
   </td>
  </tr>
  <tr>
   <td>r
   </td>
   <td>el archivo se abre en modo de solo lectura, no se puede escribir (argumento por defecto).
   </td>
  </tr>
  <tr>
   <td>w
   </td>
   <td>modo de sólo escritura (si existe un archivo con el mismo nombre, se borra).
   </td>
  </tr>
  <tr>
   <td>a
   </td>
   <td>modo de agregado (append), los datos escritos se agregan al final del archivo.
   </td>
  </tr>
  <tr>
   <td>r+
   </td>
   <td>el archivo se abre para lectura y escritura al mismo tiempo.
   </td>
  </tr>
  <tr>
   <td>b
   </td>
   <td>el archivo se abre en modo binario, para almacenar cualquier cosa que no sea texto.
   </td>
  </tr>
  <tr>
   <td>U
   </td>
   <td>el archivo se abre con soporte a nueva línea universal, cualquier fin de línea ingresada será como un \n en Python.
   </td>
  </tr>
</table>

**_Tabla 5. Opciones de apertura de archivos_**

Un ejemplo de apertura del archivo, indicando solo el modo lectura (r), es el siguiente:

``` python
text_file = open('file.txt', 'r')
```

Además de la apertura de archivos, también está el cierre de estos, la cual se hace con `close().` Debido a que la apertura y cierre de los archivos se está manejando manualmente, es importante recordar cerrar los archivos después de manejarlos, para evitar posibles errores como dañar el contenido. Para cerrar el archivo se hace de la siguiente manera:

``` python
text_file.close()
```

### Lectura de archivos

Una vez abierto el archivo, asignado uno de los modos que incluye es la lectura y obteniendo el **objeto** tipo `file` se le pueden invocar métodos propios de de esta **clase**  que son asociados a la visualización del contenido como: `read(), seek(), readline(), readlines()`, entre otros. Para probar las 3 funciones mencionadas se toma el archivo file.txt del código anterior y se le ingresa el siguiente texto:

``` bash
Este es un texto de ejemplo.
Utilizaría lorem ipsum,
pero se vuelve muy extenso.
```

* `read()`: se encarga de leer la totalidad del archivo, además tiene el ingreso de un parámetro opcional. Si se le ingresa un número entero como parámetro, la función leerá el número de posiciones ó caracteres indicadas en el argumento a partir de la posición en la que se encuentre. Si no se ingresa ningún argumento, leerá todo el contenido del archivo.

``` python
print(text_file.read(3))
```

Al ejecutarlo se mostrará el String “Est”.

* `readline():` esta función se encarga de leer el contenido de un archivo hasta encontrar un salto de línea (\n).

``` python
print(text_file.readline())
```

Al ejecutarlo se mostrará el  String “Este es un texto de ejemplo.”

* `readlines():` esta función es similar a la anterior, sin embargo, esta función lee todas las líneas del archivo y retorna una lista, donde cada elemento de la lista contiene un **objeto** string con el contenido de cada línea.

``` python
print(text_file.readlines())
```

Al ejecutarlo se mostrará la lista así: ['Este es un texto de ejemplo.\n', 'Utilizaría lorem ipsum,\n', 'pero se vuelve muy extenso.']

* `seek():` es una función que sirve para posicionar un puntero en una posición específica del contenido del archivo, necesita como parámetro un número entero, el cual indica en qué posición del documento se desea partir. Si todo salió bien, la función retornará el mismo número ingresado como parámetro.

``` python
print(text_file.seek(3))
print(text_file.read())
```

Al ejecutarse, la salida  de seek es `3`, y la salida de la función read es:

``` bash
e es un texto de ejemplo.
Utilizaría lorem ipsum,
pero se vuelve muy extenso.
```

>**Nota:** estas 4 funciones son las básicas para el enfoque, sin embargo, existen otras como `seekable()` o `readable()`, las cuales, indican si un archivo es legible o se puede recorrer su contenido.

### Escritura de archivos

Similar a la lectura de archivos,  la escritura también tiene dos métodos importantes, como son: `write()` y `writelines()`. Los cuales permiten crear texto dentro de los archivos previamente abiertos con `open()` y asignado el modo de escritura (w). Adicionalmente, si un archivo no existe, con la escritura se crea el archivo y se le agrega el contenido que se requiere dentro de este. Para ejemplos explicativos se utilizará como base el siguiente código:

``` python
new_file = open('new_file.txt', 'w')

```

* `write():` se encarga de escribir/sobrescribir el string que se le ingrese como parámetro, en el archivo asignado como parámetro de la **clase**  `open()` (Para este caso es el archivo que no existe `new_file.txt`, por ende no sobreescribe contenido). Si todo sale bien, la respuesta será el número de caracteres escritos en el archivo.

``` python
print(new_file.write('Hola mundo'))
```

Al ejecutarse, la salida es 10, lo que indica que si se abre el archivo `new_file.txt` se encontrará el texto “Hola mundo”.

* `writelines():` semejante a write(), esta función se encarga de escribir/sobrescribir el contenido de un archivo, pero a partir de una lista que se ingresa como parámetro. Si todo sale bien, la respuesta será `None`.

``` python
my_txt_lines = ['Hola ','de nuevo, ','mundo']
print(new_file.writelines(my_txt_lines))
```

  Al ejecutarse, la salida es `None`, lo que indica que si se abre el archivo `new_file.txt` se encontrará el texto “Hola de nuevo, mundo”.

### Modificar contenido de  archivos

La modificación es una combinación de lectura y escritura de archivos, lo que se quiere es agregar cadenas de caracteres para cambiar el contenido dentro de un archivo, ya sea modificando el existente o adicionando nuevo contenido al que ya existe.  Para uso demostrativo, se creará un archivo que se llame `mod_file.txt`, el cual tendrá como contenido inicial “¡Saludos, terrícolas!”. Además, se usará el modo `append` (a), tal como se muestra en el siguiente código:

``` python
modifiable_file = open('mod_file.txt', 'a')
```

* `write():` esta función es la misma que la descrita en escritura de archivos, sin embargo, dejará el contenido intacto en el archivo y escribirá el nuevo contenido al final de este.

``` python
print(modifiable_file.write('Mi texto'))
```

   Al ejecutarse, la salida es 8 (indicando el número de caracteres que se agregaron), lo que indica que si se abre el archivo `mod_file.txt` se encontrará el texto “¡Saludos, terrícolas!Mi texto”.

* `writelines():` similar a `write()`, esta función es la misma que en Escritura de archivos, con la diferencia de que dejará el contenido intacto en el archivo y escribirá el nuevo contenido al final de este.

``` python
nuevo = [' nuevo ',' texto',' here']
print(modifiable_file.writelines(nuevo))
```

   Al ejecutarse, la salida es `None`, lo que indica que si se abre el archivo `mod_file.txt` se encontrará el texto “¡Saludos, terrícolas! nuevo  texto here”.

>**Nota**: con el modo `append` asignado, se puede invocar los métodos de Lectura de archivos.

## Peticiones HTTP

Las peticiones HTTP son muy usadas al momento de consumir servicios, ya sea para una comunicación B2B (Servidor a Servidor) ó para crear scripts y exploits de ataque, como es el caso. Es por ello que el  siguiente paso consiste en aprender a realizar peticiones HTTP a un servidor web por medio de la librería `Request`, para ello es necesario instalarla, lo cual se hace de forma muy sencilla, solo sería ejecutar el siguiente comando en el entorno virtual:

``` python
$pip install requests
```

Esta librería es muy completa pero sencilla de usar, ya que facilita la implementación de los métodos con un llamado directo a las funciones que se encargan de realizar los distintos métodos HTTP como GET, POST, PUT, DELETE, etc., sin embargo, por ser más para uso demostrativo sólo se emplearán los más conocidos y usados que son GET y POST, teniendo en cuenta que la forma de uso de estos, sirve para el uso de los demás métodos.

### Peticiones GET

Las peticiones GET, son una de  las más usadas en en el consumo servicios de aplicaciones y con Requests, este consumo se hace de manera muy sencilla, solo se debe invocar la función `get` y definir la `url`, los `parámetros` (si son necesarios), así como otros parámetros opcionales como headers. Un ejemplo de uso de la función `get` con la url de un recurso documental de **postman** el siguiente:

Primero se empieza por importar la librería `request`:

``` python
import requests
```

También se puede importar solo la función `get`:

``` python
from requests import get
```

Después se invoca la función `get` de la librería `requests`, donde se le pasa al parámetro `url` la dirección del recurso de **postman** y el resultado de esta operación se almacena en la variable `response`:

``` python
response = requests.get(url='https://postman-echo.com/get')
```

La variable response es un **objeto** de la **clase**  `requests.models.Response,` es decir que no se puede mostrar directamente, es por ello que se usan métodos asociados a esa clase, para manejar y mostrar la información contenida como lo son `text` y `json()`. La función `text`(nótese que no necesita paréntesis) sirve para mostrar cómo String cualquier respuesta que obtenga de la petición como lo sería un html, xml, json, etc., por otro lado, la función `json()` se usa para mostrar una respuesta que es de tipo json y la convertirá a un **objeto** tipo diccionario, es decir, `dict`, lo cual haría que se ahorrará mucho tiempo en el manejo de la información.

En este caso, como la respuesta de postman es un json, se utiliza la función `json()` y se muestra con el clásico `print()`:

``` python
print(response.json())
```

Entonces, todo el código completo es el siguiente:

``` python
import request
response = requests.get(url='https://postman-echo.com/get')
print(response.json())
```

Por respuesta a la petición anterior se obtiene lo siguiente:

``` python
{'args': {}, 'headers': {'x-forwarded-proto': 'https', 'x-forwarded-port': '443', 'host': 'postman-echo.com', 'x-amzn-trace-id': 'Root=1-5fb8a65f-3a3253b051fea3db388a89a1', 'user-agent': 'python-requests/2.22.0', 'accept-encoding': 'gzip, deflate', 'accept': '*/*'}, 'url': 'https://postman-echo.com/get'}
```

Este mismo recurso de postman recibe parámetros http en los parametros del metodo `get`, esto lo hace usando `params`, para ello solo sería agregar un par de líneas  adicionales donde incluiremos una variable `parameters` a la cual le asignaremos un diccionario con los parámetros que se enviaran. El código queda así:

``` python
import requests
parameters = {
   'foo1': 'bar1',
   'foo2': 'bar2'
 }
response = requests.get(url='https://postman-echo.com/get', params=parameters)
print(response.json())
```

Donde, por respuesta se obtiene:

``` python
{'args': {'foo1': 'bar1', 'foo2': 'bar2'}, 'headers': {'x-forwarded-proto': 'https', 'x-forwarded-port': '443', 'host': 'postman-echo.com', 'x-amzn-trace-id': 'Root=1-5fb8a7e4-76964b5e4f906c8c4cebefc0', 'user-agent': 'python-requests/2.22.0', 'accept-encoding': 'gzip, deflate', 'accept': '*/*'}, 'url': 'https://postman-echo.com/get?foo1=bar1&foo2=bar2'}
```

>**Nota:** si se necesitan usar `headers`, la forma de definirlo sería como un diccionario (`dict`), semejante a como se definieron los parámetros en este ejercicio. Además, cabe recordar que para el ejemplo anterior, también se puede usar la función `text` en la respuesta y mostrará el mismo contenido pero como una cadena de texto (`str`).

### Peticiones POST

Los POST son peticiones GET pero con esteroides, pues se usa de la misma forma que las funciones `get` de la librería `requests` (parámetros de la url, asignación de url, misma forma de mostrar la respuesta, etc. ), con la condición de que se tiene que enviar datos en en un body, los cuales se pueden pasar a dos parámetros: `data` y `json`. El parámetro `data` recibe la información en forma de cadena de texto (`str`), ideal cuando se manda la información en forma de urlencoded, xml o texto, aunque también acepta json. El parámetro `json` recibe un diccionario con toda la información a enviar. Un ejemplo de uso de la función `post` con la url de un recurso de pruebas llamado `jsonplaceholder` el siguiente:

Se importa la librería request, igual como si se estuviera realizando una petición GET, pero en este caso, por variar, usaremos la segunda opción que se describió en **Peticiones GET**, donde solo se importa la función `post`:

``` python
from request import post
```

Después, se definirá un body en una variable súper creativa que se llamará `body`, el cual será un diccionario con los valores de `title, body` (valor que se requiere en la petición, no confundir con la variable) y `userId.` Adicional a ello, para ejemplificar el uso de las cabeceras, se definirá una cabecera en una variable `my_headers,` a la que también se le asigna un diccionario y su contenido es la cabecera de `Content-Type`:

``` python
body = {
   'title': 'new_title',
   'body': 'new_body',
   'userId': 'userid'
}

my_headers = {
   'Content-Type': 'application/json'
}
```

Ahora se usa la función `post`, la cual ya no necesita ser invocada por medio de un **objeto** de `requests`, pues fue importada directamente. A la función se le pasa los parámetros de función que son `url`, `json` (recuerde que puede usar data en su lugar) y `headers.` El resultado de la función `post` (**objeto** de la **clase**  `requests.models.Response`) se asigna a una variable llamada `response`:

``` python
response = post(url='https://jsonplaceholder.typicode.com/posts', json=body, headers=my_headers)
```

Como una vez más la respuesta es un json, se utiliza la función `json()` y se muestra con `print()`:

``` python
print(response.json())
```

Donde, por respuesta se obtiene:

``` python
{'title': 'new_title', 'body': 'new_body', 'userId': 'userid', 'id': 101}
```

Uniendo todas las porciones de código se obtiene los siguiente:

``` python
from requests import post

body = {
   'title': 'new_title',
   'body': 'new_body',
   'userId': 'userid'
}

my_headers = {
   'Content-Type': 'application/json'
}

response = post(url='https://jsonplaceholder.typicode.com/posts',json=body, headers=my_headers)
print(response.json())
```

>**Nota:** Para implementar los otros métodos HTTP disponibles en la librería `requests`, se usan cada función de la misma forma que `post` y `get`, pues todos reciben los mismos parámetros en su respectiva función.

## Criptografía

La palabra Criptografía proviene del griego "kryptos" que significa oculto, y "graphia", que significa escritura, y su definición según el diccionario es "Arte de escribir con clave secreta o de un modo enigmático". La criptografía es una técnica que originalmente trata sobre la protección o el ocultamiento de la información frente a observadores no autorizados.

Criptografía es la ciencia encargada de proveer seguridad y protección a la información. Esta se utiliza en todo lo que consumimos y utilizamos en nuestro día a día digital, abrir un correo, conectarse a una red WiFi, entrar a un sitio web, etc.

Dentro de los principales objetivos de la criptografìa encontramos:

* Autentificar la identidad de los usuarios.
* Con el uso de los algoritmos específicos se puede garantizar confidencialidad de la información.
* Proteger la integridad de la información.

#### Ventajas

* Proveer seguridad y protección de la información.
* Fácil de aplicar.
* Continuamente se están desarrollando algoritmos más robustos y a su vez más seguros que tienden a ser imposibles de vulnerar, lo cual genera mayor confianza en el momento de utilizar estos métodos.

#### Desventajas

* La criptografía puede incluso considerarse como un arma de doble filo ya que los atacantes pueden usarla para esconder sus acciones.
* El tiempo y el consumo de CPU de  algunas tareas puede verse afectado por el procesamiento que se requiere  tanto para la encriptación y posteriormente la obtención de la información en su estado original.
* Si el método se ve comprometido la informaciòn puede caer en manos de usuarios no autorizados.

Desde el punto de vista del desarrollo de aplicaciones y seguridad de la  información, es necesario conocer los conceptos básicos de confidencialidad, integridad, autenticidad, criptografía, hashing, cifrado simétrico, cifrado asimétrico, firmas digitales, etc.

* **Confidencialidad**: Es la capacidad de un mensaje para mantener oculto su contenido de tal forma que si una tercera persona ve el mensaje no pueda interpretar su contenido.
* **Integridad**: Se refiere a que los elementos mensajes, datos, documentos, y otros formas de contenido no han sido modificados en tránsito o en reposo.
* **Autenticidad**: Se refiere a garantizar que el mensaje ha sido enviado por quien dice ser.

### Codificación

Consiste en modificar el mensaje original, cambiando caracteres por otros según una representación definida.  El objetivo de la codificación no es mantener la información en secreto, sino que sea legible por otros sistemas o que pueda ser utilizado por cualquier proceso externo. Las reglas de transformación entre un sistema y otro van a estar definidas para poder entender el contenido del mensaje.

Dentro de los sistemas de codificación más conocidos se encuentran: códigos Morse, EUC (Extended Unix Code), UTF-8 (Unicode transformation format), código ASCII, etc.

### Cifrado  

Es el proceso por el cual el mensaje es modificado por medio de algunos algoritmos para que la información sólo puede ser accedida por los usuarios autorizados, para garantizar esta confidencialidad se hace uso de llaves tanto para cifrar como descifrar el mensaje.

#### Cifrado Simétrico

En esencia el cifrado simétrico funciona de la siguiente manera:

1. El emisor procede a ocultar la información utilizando un algoritmo de cifrado, el cual requiere una llave o clave  para cifrar el mensaje
2. Una vez se tenga el mensaje cifrado se envía al destinatario que está autorizado para recibir dicha información
3. El receptor procede a descifrar el mensaje con la misma llave utilizada por el emisor.  

<p align="center"><img src="images/f6_Cifrado_Simetrico.png" /></p>

<p align="center"><b>Figura 6</b>. Cifrado simétrico  

**Características:**

* El emisor y el receptor utilizan la misma llave criptográfica para cifrar y descifrar la información.
* Es más rápido de procesar que el cifrado asimétrico.
* Los algoritmos más populares en este método de cifrado son: DES (Data Encryption Standard), 3DES (utiliza el mismo algoritmo DES pero aumenta la longitud de las llaves, lo cual lo hace más difícil de vulnerar), AES (Advanced Encryption Standard).

#### Cifrado Asimétrico

Este cifrado se ejecuta de la siguiente manera:

1. El emisor genera una llave privada, la cual va a ser secreta y custodiada. Esta llave no se puede compartir con nadie.
2. A partir de la llave privada generada, el emisor genera la llave pública. Esta llave pública se envía a las partes con que se desee establecer la comunicación.
3. El emisor procede a cifrar el mensaje utilizando la llave pública del receptor al cual va dirigido el mensaje.
4. El emisor envía el mensaje cifrado.
5. El receptor procede a descifrar el mensaje recibido utilizando su llave privada.

<p align="center"><img src="images/f7_Cifrado_Asimetrico.png" /></p>

<p align="center"><b>Figura 7</b>. Cifrado asimétrico

**Características:**

* Se trabaja con un par de llaves, una pública y una privada. La llave pública se utiliza en el proceso de cifrado mientras que la llave privada se utiliza para descifrar la información recibida. 
* La llave privada debe ser custodiada y nunca debe ser compartida con nadie.
* La llave pública debe ser enviada a las partes con las cuales se desea efectuar la comunicación cifrada.
* Los algoritmos más populares en este método son: RSA (Rivest–Shamir–Adleman) y ECC (Elliptic-curve cryptography).

### Hashing

Transforma mensajes de entrada a mensajes de salida de longitud fija, los cuales no pueden ser devueltos al mensaje original e identifica de manera única el mensajes de entrada. cuando se presentan dos salidas iguales a dos entradas diferentes se habla de un fenómeno denominado colisión.

La finalidad de este método es garantizar la integridad de los datos.  

**En resumen:**

<table>
  <tr>
   <td><strong>Método </strong>
   </td>
   <td><strong>Característica</strong>
   </td>
   <td><strong>Uso de llaves</strong>
   </td>
   <td><strong>Garantiza (Confidencialidad - Integridad - Autenticidad)</strong>
   </td>
   <td><strong>Ejemplos más comunes</strong>
   </td>
  </tr>
  <tr>
   <td>Codificación
   </td>
   <td>Reversible
   </td>
   <td>No requiere clave (llave)
   </td>
   <td>No logra garantizar ninguno
   </td>
   <td>ASCII, Morse, base64
   </td>
  </tr>
  <tr>
   <td>Cifrado
   </td>
   <td>Reversible
   </td>
   <td>Simétrico:
<p>
Requiere una llave pública
<p>
Asimétrico: requiere una llave pública y una privada.
   </td>
   <td>Confidencialidad
   </td>
   <td>Simétrico: DES, 3DES, AES
<p>
Asimétrico: RSA, ECC
   </td>
  </tr>
  <tr>
   <td>Hashing
   </td>
   <td>No reversible
   </td>
   <td>NA
   </td>
   <td>Integridad
   </td>
   <td>MD5, SHA
   </td>
  </tr>
</table>

**_Tabla 6.  Criptografía._**

# REFERENCIAS

* Programación en Python - Nivel básico [https://entrenamiento-python-basico.readthedocs.io/es/latest/index.html](https://entrenamiento-python-basico.readthedocs.io/es/latest/index.html)