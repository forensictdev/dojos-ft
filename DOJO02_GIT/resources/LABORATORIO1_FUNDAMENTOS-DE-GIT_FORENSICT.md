#LABORATORIO 1 - FUNDAMENTOS EN GIT


**Consideraciones**: La presente guía de laboratorio fue desarrollada para un entorno Linux, sin embargo puede ser llevada a cabo en ambientes windows. Para ello sugerimos usar la consola de comandos que provee GIT después de su instalación. 
 
1. Crear una carpeta para alojar el proyecto al cual se le hará un control de versión con GIT, en este caso crear una carpeta llamada **GIT_01** e ingresar a ella desde el editor de comandos.


![alt_text](images/Lab1_image1.png "Figura 1. Carpeta del proyecto.")



_Figura 1. Carpeta del proyecto._

2. Iniciar el proyecto con el comando **git init**.


![alt_text](images/Lab1_image2.png "Figura 2. Inicio del proyecto con GIT.")



_Figura 2. Inicio del proyecto con GIT._



3. Configurar el nombre de usuario y correo con el que quedarán registrados las modificaciones en el proyecto de git, en este caso se configurará un usuario y correo local para este proyecto ejecutando lo siguientes comandos:
    1. git config --local user.name “nombre_usuario”
    2. git config --local user.email “correo”


![alt_text](images/Lab1_image3.png "Figura 3. Configuración local de los datos del usuario en GIT para el proyecto.")


_Figura 3. Configuración local de los datos del usuario en GIT para el proyecto._



4. Crear un archivo llamado **Saludo.py** con el siguiente contenido:
    
    print (“Bienvenido al dojo de GIT”)
    
5. Revisar el estado de GIT para conocer el estado del proyecto con el comando:
    1. git status

![alt_text](images/Lab1_image4.png "Figura 4. Estado del proyecto en GIT.")

_Figura 4. Estado del proyecto en GIT._

6. Como se puede observar en la imagen anterior, GIT ha detectado que un archivo fue modificado, en este caso se debe a la creación del archivo Saludo.py, primero se debe pasar al **staging area** para que posterior a esto se pueda hacer su respectivo **commit.**
    1. Pasar el archivo al staging area con el comando: **git add Saludo.py**.
    2. Realizar el primer commit con el comando: **git commit -m "commit inicial"**

![alt_text](images/Lab1_image5.png "Figura 5. Preparación del archivo Saludo.py.")

_Figura 5. Preparación del archivo Saludo.py._

![alt_text](images/Lab1_image6.png "Figura 6. Confirmación del cambio en el archivo Saludo.py.")


_Figura 6. Confirmación del cambio en el archivo Saludo.py._

## RAMA (BRANCH)

Normalmente en un proyecto cuando se desea hacer algún cambio o crear una nueva característica de algo, se hace creando una rama en algún punto del proyecto, en la cual se hará el desarrollo de esa característica y tras finalizar este desarrollo se mezcla en la rama que mantiene el proyecto actualizado o el proyecto en su última versión.

1. Crear una nueva rama para incluir una nueva característica al archivo **Saludo.py** y posterior a la creación se debe mover a la rama creada, con los siguientes comandos:
    1. git branch caracteristica_a
    2. git checkout caracteristica_a 

    O también se puede hacer con un solo comando, el cual creará la nueva rama y se moverá a la misma, con el siguiente comando:

    1. git checkout -b caracteristica_a

![alt_text](images/Lab1_image7.png "Figura 7. Creación de ramas a partir de la rama master.")

_Figura 7. Creación de ramas a partir de la rama master._

2. Listar las ramas del proyecto y saber cuál es la rama actual donde se está trabajando en el proyecto con el comando: **git branch.**

![alt_text](images/Lab1_image8.png "Figura 8. Listar las ramas del proyecto.")

_Figura 8. Listar las ramas del proyecto._



3. Editar el archivo Saludo.py, incluyendo otro línea para imprimir el texto “**característica a**”, al finalizar el archivo debe quedar de la siguiente forma:


![alt_text](images/Lab1_image9.png "Figura 9. Contenido del archivo “Saludo.py”.")


_Figura 9. Contenido del archivo “Saludo.py”._



4. Confirmar este cambio, pasando el archivo al área de preparación (staging area) y luego realizando su respectiva confirmación (commit) con los comandos:
    1. git add Saludo.py
    2. git commit -m “agregando la característica a”


![alt_text](images/Lab1_image10.png "Figura 10. Confirmación del cambio en el archivo “Saludo.py”.")


_Figura 10. Confirmación del cambio en el archivo “Saludo.py”._



5. Verificar que el contenido del archivo Saludo.py es diferente en las dos ramas (master y caracterisitca_a)
6. Dado que ya se terminó de desarrollar la **característica a**, es necesario que esta característica se incluya en la última versión del proyecto o su versión estable, la cual en este caso es la rama **master. **Para hacer esto seguimos los siguientes pasos:
    1. Moverse a la rama principal (la que recibirá la nueva característica) con el comando: **git checkout master**
    2. Realizar la mezcla con la rama que tiene la nueva característica con el comando: **git merge caracteristica_a**

![alt_text](images/Lab1_image11.png "Figura 11. Fusión de la rama caracteristica_a a la rama master.")


_Figura 11. Fusión de la rama caracteristica_a a la rama master._

En este punto se ha logrado efectuar todo un flujo de trabajo en GIT, donde se inició un repositorio local, se crearon dos ramas, una de ella es la rama estable del proyecto llamada master y otra rama que fue utilizada para crear una característica nueva al proyecto, la cual al estar terminada se fusionó a la rama estable del proyecto. Sin embargo, no siempre al trabajar con GIT será de esta forma, porque cuando se trabaja con más personas que aportan al proyecto, cada colaborador se puede topar con situaciones particulares donde deba ejecutar acciones adicionales como: solucionar conflictos, revertir un cambio, entre otros.

Para este siguiente escenario vamos a crear dos nuevas ramas llamadas **caracteristica_b** y **catacteristica_c** y modificamos el contenido de **Saludo.py** para que imprima el texto **“característica b”** en la rama **característica b** y el texto **“característica c”** en la rama **caracteristica_c.**


![alt_text](images/Lab1_image12.png "Figura 12a. Estado del proyecto.")


_Figura 12a. Estado del proyecto._

![alt_text](images/Lab1_image13.png "Figura 12b. Contenido de Saludo.py en rama caracteristica_b.")


_Figura 12b. Contenido de Saludo.py en rama caracteristica_b._

![alt_text](images/Lab1_image14.png "Figura 13. Contenido de Saludo.py en rama caracteristica_c.")


_Figura 13. Contenido de Saludo.py en rama caracteristica_c._

Cuando se trate de fusionar las dos ramas (caracteristica_b y caracteristica_c) a la rama master se tendrá un conflicto al intentar fusionar la última rama, dado que como se observa en las imágenes anteriores el archivo **Saludo.py** en su línea número 3, tendrá contenidos diferentes y GIT no puede determinar cual debe quedar, por lo tanto lo marcará para que algún colaborador del proyecto lo resuelva evitando así la pérdida de información.

![alt_text](images/Lab1_image15.png "Figura 14. Fusión de la rama caracteristica_b a la rama master.")


_Figura 14. Fusión de la rama caracteristica_b a la rama master._

![alt_text](images/Lab1_image16.png "Figura 15. Conflicto al intentar fusionar la rama caracteristica_c a la rama master.")


_Figura 15. Conflicto al intentar fusionar la rama caracteristica_c a la rama master._

Al presentarse un conflicto GIT realizará unas marcas especiales donde indicará donde detectó el conflicto para que un colaborador lo pueda solucionar manualmente.


![alt_text](images/Lab1_image17.png "Figura 16. Conflicto marcado por GIT en el archivo “Saludo.py”.")


_Figura 16. Conflicto marcado por GIT en el archivo “Saludo.py”._

Para solucionar el conflicto es necesario editar el archivo Saludo.py, borrando las marcas **<&lt;<&lt;<&lt;< HEAD, ========** y **>>>>>>>>> caracteristica_c,** al borrar estas marcas el colaborador deberá determinar qué texto debe quedar, según la imagen anterior el texto **print (“característica b”)** o el texto **print (“característica c”)**, para este caso deben quedar los dos.

Al terminar la edición del archivo, será necesario realizar un flujo completo para la preparación (staging) y la confirmación (commit) del cambio en el archivo.

![alt_text](images/Lab1_image18.png "Figura 17. Solución del conflicto en el archivo “Saludo.py”.")


_Figura 17. Solución del conflicto en el archivo “Saludo.py”._

## RETO

Actualmente en una organización se ha trabajado un documento ofimático sobre políticas de seguridad que debe tener la compañía, como es de esperarse estas políticas van cambiando a lo largo del tiempo, es decir, se van actualizando respecto a las nuevas recomendaciones de seguridad que tiene la compañía. Esto ha generado que se tengan múltiples archivos de las políticas con diferentes versiones, lo que ha generado confusiones en ciertas circunstancias al no conocer cuál es la versión actual y en muchos casos al tratar de determinar cuáles fueron los cambios entre una versión y otra sea una tarea tediosa.

Implemente GIT para controlar las versiones en este documento de políticas y muestre cómo puede optimizar el tiempo a la hora de determinar qué cambios ocurrieron entre una versión y otra.
