# LABORATORIO 2 - FUNDAMENTOS EN GIT

**Consideraciones**: La presente guía de laboratorio fue desarrollada para un entorno Linux, sin embargo puede ser llevada a cabo en ambientes windows. Para ello sugerimos usar la consola de comandos que provee GIT después de su instalación. 

Antes de comenzar con el segundo laboratorio debemos conocer algunos aspectos particulares que nos ofrece la plataforma de GitLab,  como algunos términos de GitLab, los permisos y la opción de agregar miembros al repositorio.

Algunos términos a tener presente cuando estemos en la plataforma de GitLab son los siguientes:



*   **Issues:** Son los bugs, tareas o ideas sobre el proyecto, pueden ser visualizados de varias maneras como listas o un tablero (tipo canvas), además pueden ser asociados a un commit en específico.
*   **Merge Request:** Son las fusiones a nivel de la plataforma, estas requieren revisión y la confirmación de un tercero (dependiendo de las características del proyecto puede ser uno mismo).
*   **Milestone:** Son franjas de tiempo donde se pueden asignar varios issues a trabajar, comúnmente se asocian a un release o sprint cuando se trabaja con metodologías ágiles.

Otros términos como **commit**, **branch**, **tag**, son términos propios de Git y por lo tanto poseen la misma connotación en la plataforma.

La primera opción que nos brinda GitLab es cuando se crea un nuevo proyecto, este nos ofrece la particularidad de otorgarle una opción de visibilidad, _público (public)_, que permite acceder a la información del proyecto sin autenticarse, es decir, está expuesto a todo el mundo; _privado (private)_, que solo permite ser visualizado por los miembro o usuarios agregados (figura 1).


![alt_text](images/Lab2_image1.png "Figura 1. Opciones de visibilidad de un proyecto en GitLab.")
 

_Figura 1. Opciones de visibilidad de un proyecto en GitLab._

Ahora, cuando estamos en un proyecto creado, como se mencionó anteriormente, se puede agregar uno o varios usuarios para que puedan ver (si el repositorio es privado) y agregar contenido. Esta última opción dependerá del rol asignado a ese usuario. GitLab maneja 4 roles Guest, Reporter, Developer y Maintainer, a continuación se detalla las funciones de algunos:



*   **Guest (Invitado):** Solo posee acceso a la lectura del repositorio y algunas opciones de edición como agregar comentarios o crear issues.
*   **Developer (Desarrollador):** Permite control sobre la edición del proyecto, dando la facilidad de realizar cambios sobre el mismo, algunas funciones que posee son: crear ramas, realizar commit, gestión de los releases e issues, etc.
*   **Maintainer (Mantenedor):** Es un rol administrador dentro del proyecto, pues puede realizar las funciones de los demás roles y además realizar configuraciones extras, una muy útil es solicitar que los _merge request_ (fusión de ramas en GitLab) sean revisados y aprobados por otra persona, en el  momento que el developer lo realiza.

Existe un quinto rol asignado al creador del proyecto, Owner (Propietario), y como no puede ser asignado a algún otro usuario no se tiene en consideración en el documento, eso sí, el Owner tiene acceso total en el repositorio, teniendo todos los permisos posibles sobre el mismo.

A modo informativo, cuando se tiene un repositorio público, solo aquellos que son miembros del repositorio pueden realizar cambios sobre el mismo, sin embargo, hay ciertos proyectos Open Source que permiten la contribución por parte de la comunidad.


## Guía Práctica

Para el ejercicio a realizar, haremos equipos de 3 y 4 personas, con el objetivo de simular los roles que brinda GitLab y tener un flujo de trabajo adecuado en proyectos con muchas personas, en este caso tendremos 2 personas con el rol de _maintainer_ y el resto serán developer.

Para efectos prácticos, utilizaremos el proyecto creado en el laboratorio 1, por ende, es necesario que una persona del equipo tome el liderazgo, tome el proyecto, crea un archivo de python (figura 2) y lo aloja en una nueva carpeta, luego crea un repositorio en GitLab (en caso de no tener una cuenta allí, esta debe ser creada) y asocia el proyecto en cuestión a dicho repositorio (figura 3), además, debe agregar a sus compañeros de equipo al proyecto con los roles previamente definidos (figura 4). 


![alt_text](images/Lab2_image2.png "Figura 2. Script de python.")


_Figura 2. Script de python._


![alt_text](images/Lab2_image3.png "Figura 3. Asociando a repositorio remoto.")


_Figura 3. Asociando a repositorio remoto._


![alt_text](images/Lab2_image4.png "Figura 4. Agregar miembros a un proyecto.")


_Figura 4. Agregar miembros a un proyecto._

Ahora, con el proyecto en GitLab, cada miembro del equipo agregado allí, debe clonar (figura 5) el repositorio para realizar la actividad propuesta a continuación.


![alt_text](images/Lab2_image5.png "Figura 5. Clonando el repositorio.")


_Figura 5. Clonando el repositorio._

Creamos una nueva rama en el proyecto, modificamos el archivo _main.py_ agregando una linea para escribir nuestro nombre, realizamos _commit_ (figura 6) y luego realizamos _push_ (figura 7)

![alt_text](images/Lab2_image6.png "Figura 6. Realizando un commit.")


_Figura 6. Realizando un commit._


![alt_text](images/Lab2_image7.png "Figura 7. Realizando un push.")


_Figura 7. Realizando un push._

Se debe tener presente que cada miembro del equipo debe realizar dicho procedimiento, por ende, también deben crear una nueva rama y esta debe tener un nombre diferente al de las demás (figura 8).


![alt_text](images/Lab2_image8.png "Figura 8. Proceso anterior, otra persona.")


_Figura 8. Proceso anterior, otra persona._

Como ya sabemos, realizar un push implica actualizar los cambios que tengo en mi equipo en el repositorio remoto, en este caso es GitLab, por ende, podemos revisar en la plataforma que todos los cambios se han efectuado en sus respectivas ramas. Una vez observado los cambios en GitLab, procedemos a fusionar el contenido de varias ramas y llevarlas a la rama principal _Master_ donde quedarán los cambios finales y el contenido principal e importante del proyecto, para realizar esta fusión, nos dirigimos a la opción de _Merge Requests_, seleccionamos las dos ramas a fusionar (figura 9), ingresamos información relevante sobre esta fusión (figura 10) como una persona a quien se le asigna y/o revisa el contenido a fusionar,  y por último, te muestra la información correspondiente a dicha fusión, la cual queda a la espera por aprobar.

**Nota:** Para simular un proceso adecuado sobre este procedimiento, se recomienda que sea un developer quien realice el _Merge Request_ (fusionando dos ramas distintas a la de _Master_) y este le asigne a un maintainer las opciones de asignar y revisar (assignee y reviewer).


![alt_text](images/Lab2_image9.png "Figura 9. Merge Request.")


_Figura 9. Merge Request._


![alt_text](images/Lab2_image10.png "Figura 10. Agregar información del Merge Request.")


_Figura 10. Agregar información del Merge Request._


![alt_text](images/Lab2_image11.png "Figura 11. Información del Merge Request.")
 

_Figura 11. Información del Merge Request._

Después de realizado el procedimiento anterior, se procede a revisar la información del Merge Request, ya sea ingresando por las notificaciones de la plataforma (figura 12) o ingresando directamente por el menú lateral. 


![alt_text](images/Lab2_image12.png "Figura 12. Información del Merge Request desde notificaciones.")


_Figura 12. Información del Merge Request desde notificaciones._

Antes de aceptar el _Merge Request_, debemos comprobar que no existan conflictos en la fusión, esta información se manifiesta en el mismo lugar donde se realiza la fusión, si no es el caso simplemente revisamos que los cambios estén correctos y realizamos la fusión, pero si tenemos algún conflicto se debe solucionar antes de de realizar la fusión.

Como ya se observó anteriormente, los conflictos poseen una estructura bastante simple, en este caso en particular, observamos en la figura 13 donde y como se presenta el conflicto, y para solucionarlo podemos hacerlo desde el editor de texto propio de GitLab, eliminando las líneas que no se requieren (figura 14) y guardando los cambios.


![alt_text](images/Lab2_image13.png "Figura 13. Identificación del conflicto.")


_Figura 13. Identificación del conflicto._


![alt_text](images/Lab2_image14.png "Figura 14. Editor de texto de GitLab.")


_Figura 14. Editor de texto de GitLab._

Y una vez solucionado el conflicto, solo procedemos a realizar la fusión dando click en el botón _Merge_ (figura 15). 

**Nota:** se debe tener muy claro si se desea mantener la rama fuente, generalmente dicha rama corresponde a algunas características “simples” que ya no son necesarias, en dichos casos se puede prescindir de ella; pero hay algunas que son de vital importancia y por ende se deben mantener, por ello se debe deshabilitar la opción de _Delete source branch_.


![alt_text](images/Lab2_image15.png "Figura 15. Editor de texto de GitLab.")


_Figura 15. Editor de texto de GitLab._

Y ya confirmados los cambios (figura 16) podemos revisar el proyecto y las ramas donde se corrobora la fusión realizada previamente.


![alt_text](images/Lab2_image16.png "Figura 16. Editor de texto de GitLab.")


_Figura 16. Editor de texto de GitLab._

El proceso anterior fue realizado en la plataforma GitLab, ahora nos interesa que los cambios realizados allí los podamos actualizar en nuestro equipo, y para ello hay dos caminos a seguir.

El primer camino consiste en simplemente realizar un _pull_ (figura 17), este comando de Git nos traerá todo el contenido actualizado a la rama en la que me encuentro situado en mi equipo, por ello es importante cambiar con anterioridad la rama donde quiero encontrar los cambios.


![alt_text](images/Lab2_image17.png "Figura 17. Realizando un pull.")


_Figura 17. Realizando un pull._

La segunda opción es realizando un _fetch_ (figura 18) y luego un _merge_ (figura 19) de las ramas en cuestión.

![alt_text](images/Lab2_image18.png "Figura 18. Realizando un fetch de una rama.")


_Figura 18. Realizando un fetch de una rama._

![alt_text](images/Lab2_image19.png "Figura 19. Realizando el merge.")


_Figura 19. Realizando el merge._

**Nota:** No existe ninguna diferencia entre realizar uno u otro procedimiento, ya que en esencia al realizar un _pull_, estas realizando conjuntamente un _fetch_ y un _merge_, solo elige el procedimiento que te parezca más cómodo.

**Nota 2:** A pesar de que un _pull_ realizar un _fetch_, es adecuado realizar primero un _fetch_ (figura 20), esto con el objetivo de conocer qué cambios se han realizado en el repositorio remoto y evitar posible pérdida del contenido en el equipo. Luego cambiamos de rama para evidenciar que estamos desactualizados (figura 21) y posteriormente se realizar el pull o el merge de la rama. 


![alt_text](images/Lab2_image20.png "Figura 20. Realizando un fetch.")


_Figura 20. Realizando un fetch._


![alt_text](images/Lab2_image21.png "Figura 21. Cambiando a rama Master.")


_Figura 21. Cambiando a rama Master._
