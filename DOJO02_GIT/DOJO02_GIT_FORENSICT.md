
# FUNDAMENTOS DE GIT                                                           <img src="images/ft_technology-dojos.png" alt="FT_TECHNOLOGY_DOJOS" style="zoom:150%;" />

## **¿Qué es GIT?** 

GIt es una herramienta de control de versiones distribuido. Inicialmente fue diseñado para operar en un Linux pero ahora ya es compatible con otros sistemas operativos como Microsoft y MacOS. Tiene unas características que lo hacen una herramienta muy potente:


*   Software libre.
*   No depende de un repositorio central.
*   Antes del almacenamiento Git realiza un checksum (suma de comprobación) de la información para validar si existen cambios o alteración de datos y de esta forma asegurarse de la integridad de los datos.
*   Es posible tener un historial completo de las versiones.
*   El sistema de trabajo se define con ramas paralelas lo que permite que tenga soporte para desarrollo no lineal, es decir, este método de ramificación permite tener líneas de progreso diferentes a la rama principal lo cual permite que se hagan pruebas de funcionalidades o mejoras sin afectar el proyecto principal, con esto se fusionan los cambios de una forma más controlada y eficiente.
*   La mayoría de las operaciones en Git sólo necesitan archivos y recursos locales para funcionar, por lo tanto esto hace que Git sea mucho más veloz que otros sistemas de control de versiones, que en la mayoría de los casos deben ir a consultar otros servidores para ejecutar las operaciones o responder las solicitudes.


## **¿Qué es una rama?**

Para entender cómo GIT utiliza el método de ramificación, es necesario entender cómo almacena la información, la particularidad de GIT es que no almacena los datos de forma incremental (guardando sólo diferencias), lo que hace es que luego de cada confirmación de cambios (commit) almacena el snapshot (copias puntuales de los archivos completos, tal y como se encuentran en ese momento) lo que al final termina haciendo es guardar una serie snapshots.

Una rama Git es simplemente un apuntador móvil apuntando a una de esas confirmaciones. La rama por defecto de Git es la rama master. Con la primera confirmación de cambios que realicemos, se creará esta rama principal master apuntando a dicha confirmación. En cada confirmación de cambios que realicemos, la rama irá avanzando automáticamente. La rama “master” en Git, no es una rama especial. Es como cualquier otra rama. La única razón por la cual aparece en casi todos los repositorios es porque es la que crea por defecto el comando `git init` pero si el usuario lo desea puede renombrar la rama sin generar ningún cambio en el repositorio.

Si se quiere crear una rama nueva denominada “testing", se puede usar el comando:


```
$ git branch testing
```


Luego de tener varias ramas y que se quiera cambiar de una rama a otra, en este caso por ejemplo se quiere mover el apuntador HEAD a la rama testing:


```
$ git checkout testing
```


Si se quiere crear una nueva rama y cambiar automáticamente a ella


```
$ git checkout -b new-name
```



## **¿Qué problema soluciona GIT?**

Para hablar sobre los problemas que soluciona GIT se debe comenzar con la historia de su creación. El kernel de linux es un proyecto de software de código abierto con un alcance bastante amplio, en el 2002 comenzaron a hacer uso de un sistema de control de versiones distribuido (DVCS) pero debido algunos inconvenientes entre ambas compañías, la herramienta dejó de ser ofrecida de manera gratuita por lo que la comunidad de desarrollo de Linux decidió desarrollar su propio DVCS, el cual debía: 



*   Ser veloz.
*   Tener un diseño sencillo.
*   Contar con un gran soporte para desarrollo no lineal (miles de ramas paralelas).
*   Ser capaz de manejar grandes proyectos (como el kernel de Linux) eficientemente en cuanto a velocidad y tamaño de los datos.

Por lo tanto, desde sus comienzos ha brindado una solución de trabajo colaborativo,que compartir código de una manera fácil y ágil, ordenado, rápido, distribuido, permite llevar seguimiento de errores.


## **Instalación de GIT en Windows, Linux, MacOS**

La instalación de Git es bastante sencilla, no importa que sistema operativo uses, al final se obtendrá una versión de git compatible con los demás para su interoperabilidad. A continuación se detalla la instalación en cada uno de los tres sistemas operativos principales.


### **Instalación en Windows**

Lo primero que se debe hacer es descargar el ejecutable desde la página oficial de Git ([https://git-scm.com/download/win](https://git-scm.com/download/win)), una vez descargado se ejecuta y se realiza procede con el proceso de instalación


![alt_text](images/image1.png "Instalación de Git en Windows")


_Figura 1a. Instalación de Git en Windows._


![alt_text](images/image2.png "Instalación de Git en Windows")


_Figura 1b. Instalación de Git en Windows._


### Instalación MacOS

Al igual que en Windows, la instalación de Git por medio de un ejecutable que descargamos de la página oficial de Git ([http://git-scm.com/download/mac](http://git-scm.com/download/mac)) e incluso los pasos de su instalación son más sencillos que en windows, debido a que es un entorno relativamente similar a Linux (uso de comando de la terminal igual) que no requiere instalación de paquetes adicional para su adecuado funcionamiento.

**Nota:** en algunas ocasiones Git se encuentra instalado por defecto en MacOS, para evitar los pasos de su instalación, verificar previamente en la terminal con el comando `git` o `git --version`


![alt_text](images/image3.png "Instalación de Git en MacOS")


_Figura 2. Instalación de Git en MacOS._


### Instalación Linux

La instalación de Git es Linux es muy simple, basta con ingresar un comando en la terminal para que se efectúe la instalación.


```
$ apt-get install git
```


![alt_text](images/image4.png "Instalación de Git en Linux.")


_Figura 3. Instalación de Git en Linux._

**Notas:**



1. Por efectos prácticos, se realiza la instalación de Git en una distribución de linux basado en Debian, si usted posee una distribución de Linux que no está basada en Debian, puede ingresar a la pagina oficial de Git ([https://git-scm.com/download/linux](https://git-scm.com/download/linux)) y buscar la distribución de su preferencia.
2. Puede presentarse un error durante la instalación por problemas de permisos denegados, si es el caso, debe realizar la instalación de Git con permisos de _sudo_ o _superusuario._


## **Configuración de GIT**

GIT cuenta con una herramienta llamada `git config`, la cual permite obtener y establecer variables de configuración de GIT, estas configuraciones se almacenan en tres sitios distintos:



1. Archivo **/etc/gitconfig:** Cuenta con la configuración para todos los usuarios del sistema y todos los repositorios. Se accede a esta configuración con la opción **--system** para leer o escribir esta configuración.
2. Archivo **~/.gitconfig** o **~/.config/git/config:** En este archivo se hace una configuración particular para cada usuario del sistema, por lo tanto la ruta tiene el símbolo “~”. Se accede a esta configuración con la opción **--global** para leer o escribir esta configuración.
3. Archivo **config** en el directorio de Git (**.git/config**) del repositorio en el que se está trabajando actualmente.

Cada nivel sobrescribe los valores del nivel superior, es decir, las configuraciones de **.git/config** tienen preferencia sobre los de **/etc/gitconfig**.

Por ejemplo para configurar el nombre de usuario y correo con el que quedarán registrados nuestros cambios se haría lo siguiente:


```
git config --global user.name "usuario"
git config --global user.email "correo" 
```


Las demás opciones que tiene git config se puede conocer con el comando de ayuda, ejecutando el siguiente comando:


```
git config --help 
```



## Listado de comandos básicos con su explicación

Antes de usar Git en un proyecto se debe configurar las variables globales para el uso de git, esta configuración debe ser realizada antes de realizar un commit. Puede omitir la sentencia --global y solo quedará dicha configuración para el proyecto en cuestión.


```
git config --global user.name "nombre"
git config --global user.email "correo en git" 
```


Para comenzar un proyecto nuevo en git, lo primero que se debe realizar es la asociación del proyecto local al proyecto en git, para ello se realiza los siguientes comando en la terminal



*   Para inicializar el archivo de git sin configuraciones asociadas

```
git init
```


*   En caso de que se haya clonado un repositorio y se quiera asignar a otro origen

```
git remote rename origin old-origin
```


*   Para asociar a un origen de repositorio en específico de un usuario

```
git remote add origin https://gitlab.com/usuario/proyecto.git
```


*   Para ver el estado del árbol de trabajo

```
git status
```


*   Para agregar todos los cambios del proyecto a repositorio local


```
git add .
```
```
git add -A
```


*   Para confirmar los cambios del proyecto en el repositorio local

```
git commit -m "Initial commit"
```


*   Para enviar los cambios realizados al repositorio remoto

```
git push -u origin branch (siendo branch, el nombre de una rama del proyecto)
```


*   Si se desea clonar un repositorio de git se realiza el siguiente comando.

```
git clone https://gitlab.com/usuario/proyecto.git
```


*   Para actualizar los cambios de un repositorio remoto al repositorio local

```
git fetch origin origin-branch:new-name
```


*   Para actualizar los cambios de una rama del repositorio remota a una rama del repositorio local 

```
git pull origin branch
```


*   Para cambiar entre las ramas del proyecto.

```
git checkout branch
```


*   Para crear una nueva rama y cambiar automáticamente a ella

```
git checkout -b new-name
```


*   Para enviar los cambios al repositorio remoto junto con todas las ramas existentes localmente

```
git push -u origin --all
```


*   Para enviar los cambios al repositorio remoto junto con todos los tags existentes localmente

```
git push -u origin --tags
```


*   Para ver los cambios del archivo que se está trabajando respecto a una rama en específico, en este caso una rama master

```
git diff origin/master
```


*   Para ver las acciones realizadas en un proyecto (commits, push, merge, etc)

 ```
git log
```




## Control de versiones

Un control de versiones registra los cambios realizados en un archivo o un grupo de archivos a lo largo del tiempo, gracias a esto, es posible moverse entre las diferentes versiones de un archivo. 

Estos sistemas de control de versiones permiten regresar a versiones anteriores de un archivo, un proyecto completo, comparar cambios a lo largo del tiempo, ver los cambios realizados y quien realizó estos cambios, identificar una última modificación que pueda estar causando un problema. En resumen usar un control de versiones significa que si se dañan o se pierden archivos es posible recuperarlos fácilmente.

Los controles de versiones han evolucionado a lo largo del tiempo, inicialmente se comenzó con controles de versiones locales, luego evolucionó a controles de versiones centralizados y ahora se cuenta con controles de versiones distribuidos.


### Sistema de control de versiones locales

Es un sistema que consta de una base de datos local (en cada computador), la cual lleva el registro de todos los cambios realizados a los archivos.


![alt_text](images/image5.png "Control de versiones local.")


_Figura 4. Control de versiones local._


### Sistema de control de versiones centralizado

El sistema de control de versiones centralizado soluciona el problema de trabajo colaborativo, es decir, permite que varios colaboradores puedan trabajar en un mismo proyecto, algunos sistemas de control de versiones centralizados son: CVS, Subversion y Perforce, los cuales tienen un único servidor que contiene todos los archivos versionados y varios clientes que descargan los archivos desde ese lugar central. 

Este tipo de sistemas de control de versiones centralizado tiene ciertas desventajas, la principal desventaja es el mismo servidor, dado que se convierte en un punto único de falla, es decir, si el servidor se afecta y deja de operar por un par de horas, en ese mismo par de horas ningún colaborador podrá colaborar o guardar cambios en archivos en los que hayan estado trabajando, si el disco duro del servidor se corrompe y no se cuentan con respaldos del mismo, se perderá toda la información del proyecto, con excepción de las copias que tengan los colaboradores en sus computadores.


![alt_text](images/image6.png "Control de versiones centralizado.")


_Figura 5. Control de versiones centralizado._


### Sistema de control de versiones distribuidos

Los sistemas de control de versiones distribuidos solucionan los problemas antes mencionados, es decir, permite trabajar de forma colaborativa y los datos del proyecto se encuentran distribuidos en el servidor y los colaboradores del proyecto, dado que los colaboradores no solo descargan la última copia instantánea de los archivos, sino que se replica completamente el repositorio.


![alt_text](images/image7.png "Control de versiones distribuido.")


_Figura 6. Control de versiones distribuido._


## **Estados en GIT (committed, modified, staged)**

Git tiene tres estados principales en los que se pueden encontrar tus archivos: committed (confirmado), modified (modificado), y staged (preparado). 

**Committed**: indica que los datos se encuentran almacenados de manera segura en tu base de datos local. 

**Modified**: significa que se ha modificado el archivo pero todavía no lo has confirmado (committed) a tu base de datos.

**Staged**: indica que se ha marcado un archivo modificado en su versión actual para que sea parte de la próxima confirmación (commit).


## **Secciones principales (Working directory, staging area, git directory o repository)**

Un proyecto de Git presenta tres secciones principales: El Git directory (directorio de Git), el working directory (directorio de trabajo), y el staging area (área de preparación).

![alt_text](images/image8.png "Directorio de trabajo, área de almacenamiento y el directorio Git.")


_Figure 7. Directorio de trabajo, área de almacenamiento y el directorio Git._

El **directorio de Git** es donde se almacenan los metadatos y la base de datos de objetos para el proyecto. Es la parte más importante de Git, y es lo que se copia cuando se clona un repositorio desde otro computador.

El **directorio de trabajo** es una copia de una versión del proyecto. Estos archivos se sacan de la base de datos comprimida en el directorio de Git, y se colocan en disco para que los puedas usar o modificar.

El **área de preparación** es un archivo, generalmente situado en el directorio de Git, que almacena información acerca de lo que va a ir en el próximo commit (confirmación). 

Entonces el flujo de trabajo básico en Git es:

**Modificar** una serie de archivos en el **directorio de trabajo**.

**Preparar** los archivos, añadiendolos al **área de preparación**.

**Confirmar** los cambios, lo que toma los archivos tal y como están en el área de preparación y almacena esa copia instantánea de manera permanente en el **directorio de Git**.

En resumen, si una versión concreta de un archivo está en el directorio de Git, se considera confirmada (committed). Si ha sufrido cambios desde que se obtuvo del repositorio, pero ha sido añadida al área de preparación, está preparada (staged). Y si ha sufrido cambios desde que se obtuvo del repositorio, pero no se ha preparado, está modificada (modified).


## Crear repositorio público en GitLab  

Hay dos maneras de proceder al momento de crear, o en su defecto asociar un repositorio en Git. La primera opción es crear el repositorio en la plataforma y trabajarlo desde el principio; la segunda opción es agregar un _proyecto_ a la plataforma; se tiene una tercera opción que es importar un proyecto pero no se explorará en el presente documento. Veremos cómo realizar ambas opciones, pero antes de comenzar debemos crear una cuenta en la plataforma, y a pesar de usar GitLab durante la creación o asociación de un repositorio, debemos tener presente que en GitHub los pasos son muy similares.

Una vez se inicia sesión en GitLab, nos mostrará una página principal similar a la de la **_figura 8_**, donde también tenemos la facilidad de visualizar los proyectos, tener opciones de configuración, pero nos enfocaremos en la opción de **New project** (Nuevo proyecto).


![alt_text](images/image9.png "Página principal de usuario.")


_Figura 8. Página principal de usuario._

Una vez dado click en _New project_ tendremos una vista similar a la de la **_figura 9_**, donde además de poder crear un proyecto simple, tenemos otras opciones como crear el proyecto desde una plantilla o importarlo. Enfocándonos en la sección por defecto para crear un nuevo proyecto, comenzamos asignado un nombre al mismo, por defecto completara otro campo, _Project Slug_, que puede ser modificado posteriormente, también se puede agregar una descripción al proyecto, asignarle una visibilidad e inicializar el proyecto con un archivo llamado README (archivo en markdown que generalmente contiene información específica del proyecto), a modo de recomendación es preferible no crear el archivo si se planea asociar un proyecto al repositorio, además ofrece información de ayuda adicional de interés.


![alt_text](images/image10.png "Nuevo proyecto.")


_Figura 9. Nuevo proyecto._

Le damos click en en _Create Project_ y tendremos un dashboard para el manejo del proyecto (**_figura 10_** y figura 11), que nos brindará información relacionada al repositorio. Al lado derecho habrá información o estará visualizado el proyecto en cuestión, tal como en la figura 11, donde tenemos el archivo README.md. En la sección izquierda, podremos desplazarnos entre las diferentes opciones que brinda GitLab para el manejo de un repositorio, entre estas tenemos los issues, merge request, CI/CD, etc., cada una de estas opciones ofrece un manejo avanzado de un repositorio, ya que permite realizar flujos adicionales de un proyecto para tener un manejo adecuado y con la menor cantidad de inconvenientes posibles.


![alt_text](images/image11.png "Repositorio creado sin archivo README.")


_Figura 10. Repositorio creado sin archivo README._


![alt_text](images/image12.png "Repositorio creado con archivo README.")


_Figura 11. Repositorio creado con archivo README._

Como se mencionó al inicio de la sección, hay dos formas de proseguir cuando ya se ha creado un repositorio en GitLab, trabajar con el repositorio creado y asociar un proyecto al repositorio.



**1. Trabajar con repositorio creado.**

Una vez creado el repositorio, nos disponemos a descargarlo en nuestra máquina local. Hay dos maneras de realizar la descarga, descargando un archivo comprimido (.zip, .tar, etc.) o clonando el repositorio por medio de comandos de Git, lo trabajaremos de la segunda manera.

Para clonar el repositorio, solo se realiza la siguiente instrucción en la terminal, si es necesario se inicia sesión con la cuenta de la plataforma y tendremos algo similar a la **_figura 12_**.


```
git clone <URL_Repository>.git
```


![alt_text](images/image13.png "Ejecutando git clone.")


_Figura 12. Ejecutando git clone._

Una vez descargado (clonado) el repositorio, ingresamos a la carpeta y comprobamos el contenido de este. Como se observa en la **_figura 13_**, tenemos un archivo y un directorio oculto.


![alt_text](images/image14.png "Contenido del repositorio.")


_Figura 13. Contenido del repositorio._

Los archivos visibles son aquellos que se encuentra en la plataforma de GitLab, en este caso el solo disponemos de un README.md, el cual puede ser modificado o eliminado, además se puede empezar a trabajar la estructura del proyecto o colocar la información de interés en ese directorio para almacenarla en el repositorio remoto (GitLab). También se tiene un directorio oculto con el nombre de _.git_, el cual contiene toda la información referente para el manejo de git, conteniendo los estados, los branches (ramas), logs, entre otros; esta carpeta es de suma importancia por que es la conexión entre el repositorio remoto y el repositorio local, por tal motivo **no** debe ser borrada.



**2. Asociar proyecto a repositorio.**

Si ya tenemos un proyecto en nuestro equipo y lo que se desea es enlazar o asociarlo con un repositorio remoto, se debe tener presente que el repositorio remoto ya debe de estar creado con antelación y solo se debe seguir los siguientes comandos.


```
cd <project_name or folder>
git init
git remote add origin https://gitlab.com/<user>/<name_repository>.git
git add .
git commit -m "Initial commit"
git push -u origin master
```


En el caso ideal, no debería tener ningún error al escribir los comando, pero es posible que aparezca un error similar al de la **_figura 14_**, el cual nos pide información sobre la identidad de quien realiza el commit; en este caso, solo es necesario realizar los comando que se ilustran allí. En la sección de Configuración de Git se detallan estas configuraciones.


![alt_text](images/image15.png "Error al realizar commit.")


_Figura 14. Error al realizar commit._

Una vez agregada los datos que requiere Git, procedemos a realizar el commit y posteriormente el push, es muy probable que se requiera la validación de la identidad del usuario de GitLab, con sus respectivos permisos en el repositorio, si es así, basta con ingresar la credenciales de autenticación de GitLab. El resultado es similar al mostrado en la **_figura 15_**.


![alt_text](images/image16.png "Asociación con el repositorio remoto.")


_Figura 15. Asociación con el repositorio remoto._

Finalmente, corroboramos con GitLab (**_figura 16_**) que efectivamente están los archivos allí.


![alt_text](images/image17.png "Repositorio remoto actualizado.")


_Figura 16. Repositorio remoto actualizado._


## **Fusiones (merge)**

La fusión entre ramas que existe en Git, es la manera de unir nuevamente un historial bifurcado, es decir, unir la separación realizada de una rama a su rama principal. Para ello, hacemos uso del comando `git merge`, el cual permite la unión de las diferentes líneas de desarrollo creadas en el repositorio.

Una fusión en un repositorio de Git de hace de la siguiente manera:


```
## Nos dirigimos a la rama principal donde está el contenido a fusionar
git checkout <main_branch> 
## Realizamos la fusión con la rama destino
git merge <destiny_branch>
```


Se debe prestar bastante atención antes de realizar una fusión, ya que si se pueden generar conflictos si hay incompatibilidad entre el contenido de las ramas a fusionar.


## **Tag y versiones**

Git otorga la posibilidad de etiquetar estados relevantes en la vida de un repositorio, lo cual es usado comúnmente para el manejo de releases de un proyecto. Mediante el comando `git tag` podemos crear etiquetas, esto habitualmente se conoce con el nombre de "tagging". 

El etiquetado es una herramienta fundamental para que otros sistemas sepan cuándo un proyecto ha cambiado y se efectúen procesos a ejecutar cada vez que esto ocurra.


### Numeración de las versiones

Git es un sistema de control de versiones, por tanto permite almacenar todos los estados por los que ha pasado cualquiera de los archivos. Cuando se habla de Git tag no se hace referencia a la versión de un archivo específico, sino de todo el proyecto de forma global. Sirve para etiquetar con un tag el estado del repositorio completo, algo que se suele hacer cada vez que se libera una nueva versión del software.

Aunque no tiene nada que ver con Git, es bueno saber que las versiones de los proyectos las define el desarrollador, pero no se recomienda crearlas de manera arbitraria. Generalmente los cambios se pueden dividir en tres niveles de "importancia": Mayor, menor y pequeño ajuste. Si tu versión de proyecto estaba en la 0.0.1 y haces un cambio que no altera la funcionalidad ni la interfaz de trabajo entonces lo adecuado es versionar tu aplicación como 0.0.2. Si el proyecto ya tiene alguna ampliación en funcionalidad, pero sigue manteniendo completa compatibilidad con la versión anterior, entonces tendremos que aumentar el número de la mitad, por ejemplo pasar de la 1.0.0 a la 1.1.0. Ahora bien, si los cambios introducidos en el proyecto son tales que impliquen una alteración sobre cómo se usará esa aplicación, haciendo que no sea completamente retrocompatible con versiones anteriores, entonces habría que aumentar en 1 la versión en su número más relevante, por ejemplo pasar de la 1.1.5 a la 2.0.0.


### Crear un tag con Git

Se supone que cuando se inicia con un repositorio no se cuenta con ninguna numeración de versión y ningún tag, por lo que empezaremos viendo cómo se crean.

Supongamos que empezamos por el número de versión 0.0.1. Entonces para crear la correspondiente etiqueta se ejecuta el subcomando de Git `git tag`:


```
git tag v0.0.1 -m "Primera versión"
```


Como se observa, es una manera de etiquetar estados del repositorio, en este caso para definir números de versión. Se acompañan con un mensaje, de la misma forma que se envían mensajes en el commit.

Generalmente, después de hacer cambios en tu repositorio y subirlos al sistema (después de hacer el commit), podrás generar otro número de versión etiquetando el estado actual.


```
git tag v0.0.2 -m "Segunda versión, cambios menores" 
```



### Ver los estados de versión en el repositorio con Git tag

Después de haber creado tu primer tag, podrás lanzar el comando `git tag`, sin más parámetros, y te informará sobre las versiones que has etiquetado hasta el momento.


```
git tag 
```


Otro comando interesante en el tema de versionado es "show" que te permite ver cómo estaba el repositorio en cada estado que has etiquetado anteriormente, es decir, en cada versión.


```
git show v0.0.2 
```


**Enviar tags a GitLab/GitHub**

Si quieres que tus tags creados en local se puedan enviar al repositorio en GitHub, puedes ejecutar el push con la opción --tags. Si no se coloca, el comando push no va a enviar los nuevos tags creados.


```
git push --tags 
```


En concreto la opción --tags, tal cual la hemos usado, envía todas las nuevas tag creadas, aunque podrías también enviar una en concreto especificando la que quieres enviar, tal como se puede ver en el siguiente comando.


```
git push origin v0.0.4 
```


En este caso se debe especificar qué repositorio remoto es el destino de las tags que acabamos de crear ("origin" en este caso), pues si no se especifica el comando entenderá que el nombre de nuestro tag es el nombre del repositorio remoto que estamos queriendo usar para enviar los cambios locales, lo cual nos dará un error.

**Nota:** Aparentemente, la opción --tag hace el mismo efecto que --tags. Las dos envían los tags que tengamos en local al repositorio remoto. Por eso puedes probar usar ambas, aunque en la documentación de Git usan siempre --tags.


```
git push --tag 
```


**Enviar tags y hacer push de los commits al mismo tiempo**

Solo un pequeño detalle relativo al comando push cuando lo usamos para enviar tags. Cuando en el comando push usamos la opción --tags en principio no se mandan los cambios que tengamos en el repositorio. Es decir, aunque hayamos hecho cambios en la rama master y se hayan realizado los correspondientes commits en local, si lanzamos "git push --tags", únicamente los nuevos tags se van a enviar a remoto. No se enviarán los commits que se hayan podido realizar en cualquier rama.

Si queremos hacer un push de una rama en concreto, por ejemplo la rama master, y enviar los tags al mismo tiempo, entonces podríamos lanzar el siguiente comando:


```
git push origin master --tags
```



## **Git clean**

El uso del comando git clean hace referencia a deshacer y/o eliminar contenido que no se tiene contemplado para el repositorio, este actúa directamente sobre los archivos que no están agregada para un commit, para visualizar esta información basta con dar un `git status` y aquel contenido que aparece _modificado_ (_modified_), generalmente en rojo (**_figura 17_**), será descartado; por esta razón se debe tener mucha precaución al momento de usarlo pues involucra la eliminación de contenido y este NO puede ser recuperado.


![alt_text](images/image18.png "Cambios confirmados y no confirmados y archivos sin seguimiento.")


_Figura 17. Cambios confirmados y no confirmados y archivos sin seguimiento._

El uso del comando git clean va acompañado de algunos opciones para su funcionamiento, entre estas tenemos:



*   `-f` o `--force` que permite forzar el comando para su ejecución
*   `-n` que simula y muestra qué archivos se eliminarán, se recomienda usarlo antes de eliminar cualquier contenido
*   `-d` que indica a Git que incluya los directorios que aún no están incluidos o confirmados, puede usarse en combinación de los dos anteriores.


## Restablecer Estado


### Checkout

El comando `git checkout` permite cambiar entre diferentes versiones de una entidad. El comando `git checkout` opera sobre tres entidades distintas: archivos, confirmaciones y ramas. Git checkout actualiza el HEAD para que apunte a la rama o la confirmación especificada

Cuando el comando `git checkout` se hace sobre el nombre de las ramas se actualizan los archivos en el directorio de trabajo para reflejar la versión almacenada en esa rama y se indica a Git que registre todas las confirmaciones nuevas en esa rama en particular.


![alt_text](images/image19.png "Proyecto en rama master.")

_Figura 18. Proyecto en rama master._

En la imagen anterior se listan 3 ramas que son: **master**, **rama_1** y **rama_2**, actualmente el directorio de trabajo refleja los archivos que se encuentran en la rama **master**, en dicha rama se observan dos archivos **test.txt** y **test2.txt**, al cambiar de rama se actualizarán los archivos del directorio de trabajo que se encuentran en la rama particular.


![alt_text](images/image20.png "Proyecto en rama rama_1.")

_Figura 19. Proyecto en rama rama_1._

Como se puede observar en la imagen al cambiar de rama de la rama **master** a la rama **rama_1** se observa que en el directorio de trabajo solo se encuentra un archivo llamado **test.txt**, esto quiere decir que nos estamos moviendo en diferentes instantes de tiempo (estados/versiones) del proyecto.

Como se mencionó anteriormente es posible moverse entre confirmaciones (commits), sin embargo se debe hacer con mucha precaución dado que al moverse entre un commit se pasa a un estado desacoplado (DETACHED HEAD) lo que quiere decir que ese commit no se encuentra conectado a ninguna rama, entonces si se hacen cambios allí, confirmaciones y demás estos cambios se perderán al moverse a alguna rama.

GIT notificará cuando estemos en este modo y hará las advertencias para que no se pierdan de forma accidental los cambios.


![alt_text](images/image21.png "Proyecto en rama test_head.")


_Figura 20. Proyecto en rama test_head._

Para moverse a un commit particular basta con pasar el hash del commit al comando checkout de la siguiente manera:


```
git checkout 3e74d012e9737d14a23b95d134635df130437703
```


Tras ejecutar este comando estaremos situados en el commit 2 y saldrá la alerta del estado desacoplado.

![alt_text](images/image22.png "Proyecto desacoplado en un commit particular.")


_Figura 21. Proyecto desacoplado en un commit particular._

En caso de requerir volver a un commit anterior y seguir trabajando a partir de este estando en un estado **detached head**, lo más recomendable es crear una nueva rama a partir de este estado con el comando que la misma ayuda de git proporciona


![alt_text](images/image23.png "Ramas en un estado desacoplado.")


_Figura 22. Ramas en un estado desacoplado._

Cabe destacar que cuando se encuentra en un estado desacoplado (detached head) y se hace un git branch para listar todas las ramas, se observa una rama diferente a las que se han creado (HEAD desacoplado en 3e74d01), esto se debe al estado desacoplado, entonces si se crea una nueva rama a partir de este estado, se mantendrán los cambios que se tienen en ese instante de tiempo en la nueva rama, esto se puede hacer con los siguientes comandos:


```
git switch -c nombre_nueva_rama
```

```
git branch -b nombre_nueva_rama
```


![alt_text](images/image24.png "Nueva rama a partir de un estado desacoplado.")


_Figura 23. Nueva rama a partir de un estado desacoplado._

Después de crear la rama notamos que al listar todas la ramas con el comando `git branch` ya no se encuentra la rama HEAD... en su lugar tenemos la nueva que se creó, en este caso llamada **test.**

El comando checkout también se puede usar con archivos como se mencionó anteriormente y se hace específicamente para deshacer los cambios realizados en el archivo, para hacer esto se hace uso del comando `git checkout -- archivo`


![alt_text](images/image25.png "Archivo modificado.")


_Figura 24. Archivo modificado._


![alt_text](images/image26.png "Archivo modificado.")


_Figura 25. Archivo modificado._

Existen otros comandos importantes para movernos en los diferentes estados y tiempos de un proyecto, incluso en algunos casos cuando se requiere reorganizar alguna rama en el proyecto, entre estos comandos detallaremos el uso de **reset, revert y rebase**. Para lograr aclarar estos conceptos partiremos de un estado del proyecto y crearemos tres ramas llamadas reset, revert y rebase las cuales tendrán en un principio el mismo contenido y estado.


![alt_text](images/image27.png "Archivo inicial y su contenido.")


_Figura 26. Archivo inicial y su contenido._


![alt_text](images/image28.png "Cantidad de confirmaciones (commits).")


_Figura 27. Cantidad de confirmaciones (commits)._


### Reset

Git reset sirve para deshacer cambios a nivel de confirmaciones (commits) y a nivel de archivos agregados al área de preparación (staging area).

Cuando se aplica a nivel de commits lo que hará es devolver la rama actual a un estado anterior según la confirmación (commit) seleccionada, es decir, se mueve la rama entre las diferentes confirmaciones sin quedar en un estado desacoplado. Por defecto el git reset se hace con la opción **mixed** la cual actualiza el apuntador (HEAD) a la confirmación seleccionada y actualiza el área de preparación, pero deja intacto el directorio de trabajo, por lo que se tendrá que decidir qué hacer con los archivos que están en el área de preparación. Si se hace con la opción **hard** se actualiza el apuntador (HEAD) a la confirmación seleccionadas, se actualiza el área de preparación y se actualiza el directorio de trabajo.


![alt_text](images/image29.png "Reset a una confirmación (commit) anterior.")


_Figura 28. Reset a una confirmación (commit) anterior._

![alt_text](images/image30.png "Reset mixed.")

_Figura 29. Reset mixed._

Tras hacer el reset a la confirmación anterior, se debe decir qué hacer con los archivos que se encuentran en el área de preparación, en este caso se descartarán los cambios.


![alt_text](images/image31.png "Mixed reset.")

_Figura 30. Mixed reset._

Cuando se hace un hard reset los archivos se actualizarán al estado de esa confirmación, deshaciendo los cambios en esos archivos que están fuera de la confirmación seleccionada. El comando para hacer un hard reset es: `git reset --hard <hash del commit>`

![alt_text](images/image32.png "Hard reset.")

_Figura 31. Hard reset._

Reset también puede deshacer el paso de un archivo al área de preparación (git add), con el comando `git reset nombre_archivo`

![alt_text](images/image33.png "Deshacer la inclusión al staging area.")


_Figura 32. Deshacer la inclusión al staging area._


### Revert

Revert es similar a reset en cuanto a que permite devolver el proyecto a un estado anterior, sin embargo se diferencia a reset dado que revert no elimina el historial de los cambios del proyecto sino que invierte los cambios realizados por una confirmación y crea una nueva confirmación con el resultado de esa operación.


![alt_text](images/image34.png "Estado del proyecto antes del revert.")


_Figura 33. Estado del proyecto antes del revert._

Al aplicar el comando revert se abrirá el editor de texto configurado, en este caso vim, dado que lo que se está realizando es una nueva confirmación, guardan los comentarios de la confirmación y al finalizar se podrá observar la nueva confirmación.

El comando para hacer el revert puede ser al hash de la confirmación o a través del apuntador HEAD.


```
git revert 8ddf3d7
```

```
git revert HEAD
```

![alt_text](images/image35.png "Estado del proyecto después del revert.")


_Figura 34. Estado del proyecto después del revert._


### Reconstruir commits

La posibilidad de poder deshacer algunos cambios en GIT es posible, pero se debe ser muy cuidadoso, porque aunque se ha explicado que es muy poco probable que con GIT se pierdan los datos, en este momento si no se realizan bien las cosas se puede llegar a perder la información. Uno de los casos donde se puede querer deshacer los cambios es por ejemplo cuando luego de confirmar los cambios (commit) y se olvidaron agregar algunos archivos  o  cuando hay algún error en la confirmación de commit, para rehacer los cambios y/o adicionar los archivos que se desean en la última confirmación se hace uso de la opción amend.


```
$ git commit --amend
```


Este comando toma la staging area y abre el editor en la última confirmación (commit) que se hizo. Cuando se guarde y cierre el editor, éste escribe una nueva confirmación conteniendo el mensaje y lo asigna a la última confirmación.

Si lo que se quiere es cambiar la instantánea (snapshot) que se confirmó, para  agregar o cambiar archivos, el proceso es prácticamente el mismo. Los cambios se harían editando el archivo y utilizando las opciones `git add` en éste o `git rm` a un archivo adjunto, y luego  `git commit --amend` toma el área de trabajo (working area)  actual y la vuelve una instantánea para la nueva confirmación. Un ejemplo para adicionar un archivo luego de la confirmación sería:


```
$ git commit -m 'initial commit'
$ git add archivo_olvidado
$ git commit --amend
```



## **Resolución de conflictos**

Los conflictos en Git son relativamente comunes, por ende, no es necesario entrar en pánico cuando se encuentre alguno, la mayoría (casi todos) de estos poseen solución que no afecta lo realizado hasta el momento. El escenario más común donde se encuentran conflictos es durante una fusión, pues el contenido en las ramas que se van a fusionar puede contener sutiles diferencias que generan un conflicto en el archivo. El caso más común se debe a la manipulación de un archivo por parte de varias personas, lo cual genera que Git no pueda identificar cual contenido es el que realmente debe dejar para continuar trabajando, y a pesar de que Git permita realizar la fusión de ambas ramas, te dejará una sección donde se genera el conflicto, evidenciando el contenido de ambas ramas por el cual no se decide a dejar, en dicho caso debe editar dicho archivo dejando solo la sección de contenido de interés para solucionar el conflicto. En la **_figura 35_** se evidencia como Git manifiesta que se presenta un conflicto y además se puede ver como maneja el conflicto en cuestión en un archivo.

![alt_text](images/image36.png "Conflicto en Git.")


_Figura 35. Conflicto en Git._

Como se ve en la **_figura 35_**, Git muestra el contenido del archivo y además muestra que está generando un conflicto. Es muy fácil de identificar, solo es reconocer tres líneas en el archivo, la primera _“<<<<<<< HEAD”_ que indica el comienzo del conflicto, además también indica la sección de código que poseía la rama antes de la fusión; _“=======”_, indica el final de la sección del _HEAD_ y el comienzo de la siguiente; _“<<<<<<<  any-branch”_  indica el final del conflicto con el nombre de la rama con la que se realiza la fusión que señala el origen de la sección que genera conflicto.

**Nota:** Hay editores de texto que visualizan estos conflictos de manera más amigable, de tal manera que se identifica de manera más sencilla el lugar del conflicto, lo cual también facilita la solución del mismo.

También es común que el conflicto se presente por el uso de uno o varios archivos en la rama destino que fueron eliminados en la rama principal.

Hay otras maneras para la solución de conflictos, y es con el uso de la combinación de algunos comando de Git, como `git log`, `git reset`, `git status`, `git checkout`, entre otros, esta forma de solucionar el conflicto se realiza en la consola y por ende se debe tener un conocimiento más avanzado del funcionamiento interno de Git.

## Listado de comandos básicos

|                                      Comando                                        |                                                                   Descripción                                                                     |
|-------------------------------------------------------------------------------------|---------------------------------------------------------------------------------------------------------------------------------------------------|
|`git config --global user.name "nombre"`   &   `git config --global user.email "correo en git"`|              Configura las variables globales para el uso de git, esta configuración debe ser realizada antes de realizar un commit.              |
|                                  `git config --help`                                  |                                                    Para ver las opciones que tiene git config                                                     |
|                                      `git init`                                       |                                            Inicializa el archivo de git sin configuraciones asociadas                                             |
|                           `git clone <URL_Repository>.git`                            |                                                           Clona un repositorio de git                                                             |
|                         `git remote rename origin old-origin`                         |                                         Cuando se clona un repositorio y se quiera asignar a otro origen                                          |
|        `git remote add origin https://gitlab.com/<user>/<name_repository>.git`        |                                            Asocia un origen de repositorio en específico de un usuario                                            |
|                                     `git status`                                      |                                                        Ver el estado del árbol de trabajo                                                         |
|                               `git add .`  o   `git add -A`                                |                                             Agrega todos los cambios del proyecto a repositorio local                                             |
|                           `git commit -m "Initial commit"`                            |                                             Confirma los cambios del proyecto en el repositorio local                                             |
|                              `git push -u origin branch`                              |                       Envia los cambios realizados al repositorio remoto, siendo branch el nombre de una rama del projecto                        |
|                       `git fetch origin origin-branch:new-name`                       |                                        Actualiza los cambios de un repositorio remoto al repositorio local                                        |
|                               `git pull origin branch`                                |                             Actualiza los cambios de una rama del repositorio remota a una rama del repositorio local                             |
|                                 `git branch testing`                                  |                                                      Crea una rama nueva denominada testing                                                       |
|                                 `git branch --list.`                                  |                                                      Enumera todas las ramas del repositorio                                                      |
|                                 `git checkout branch`                                 |                                                    Para cambiar entre las ramas del proyecto                                                      |
|                              `git checkout -b new-name`                               |                                               Crea una nueva rama y cambiar automáticamente a ella                                                |
|                             `git checkout <Hash-commit>`                              |                                              Para moverse a un commit particular indicándole su hash                                              |
|                              `git reset nombre_archivo`                               |                                          Deshace el paso de un archivo al área de preparación (git add)                                           |
|                         `git reset --hard <hash del commit>`                          |Los archivos se actualizan al estado de esa confirmación, deshaciendo los cambios en esos archivos que están fuera de la confirmación seleccionada.|
|                      `git revert <Hash-commit> git revert HEAD`                       |                                  Devuelve al proyecto a un estado anterior sin eliminar el historial de cambios                                   |
|                                 `git commit --amend`                                  |                                                    Rehacer los cambios la última confirmación                                                     |
|                             `git merge <destiny_branch>`                              |                                         Une las diferentes líneas de desarrollo creadas en el repositorio                                         |
|                              `git push -u origin --all`                               |                              Envia los cambios al repositorio remoto junto con todas las ramas existentes localmente                              |
|                              `git push -u origin --tags`                              |                              Envia los cambios al repositorio remoto junto con todos los tags existentes localmente                               |
|                               `git diff origin/master`                                |              Para ver los cambios del archivo que se está trabajando respecto a una rama en específico, en este caso una rama master              |
|                                     `git status`                                      |                                   Muestra el estado del directorio de trabajo y del área del entorno de ensayo                                    |
|                                      `git clean`                                      |                                  Deshacer y/o eliminar contenido que no se tiene contemplado para el repositorio                                  |
|                                       `git log`                                       |                                    Para ver las acciones realizadas en un proyecto (commits, push, merge, etc)                                    |

## LABORATORIOS

- [LABORATORIO 1](resources/LABORATORIO1_FUNDAMENTOS-DE-GIT_FORENSICT.md)
- [LABORATORIO 2](resources/LABORATORIO2_FUNDAMENTOS-DE-GIT_FORENSICT.md)

## REFERENCIAS


[https://git-scm.com/book/en/v2](https://git-scm.com/book/en/v2)

[https://www.atlassian.com/es/git/tutorials](https://www.atlassian.com/es/git/tutorials)
