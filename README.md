# <img src="./DOJO01_FUNDAMENTOS-DE-HTTP/images/ft_technology-dojos.png" alt="FT_TECHNOLOGY_DOJOS" width="150" /> DOJOS FT 

>En Japón, un dojo es un entorno seguro donde alguien puede practicar nuevas habilidades. En el desarrollo de software y la gestión de operaciones, el dojo es un lugar donde los miembros de un equipo de DevOps van para recibir capacitación práctica. 

***ForensicT*** adoptó la metodología de dojos para generar espacios de aprendizaje en el ámbito de la ciberseguridad.

### Dojos

- [Fundamentos de HTTP](DOJO01_FUNDAMENTOS-DE-HTTP/DOJO01_FUNDAMENTOS-DE-HTTP_FORENSICT.md)
- [Fundamentos de GIT](DOJO02_GIT/DOJO02_GIT_FORENSICT.md)
- [Fundamentos de Docker](DOJO03_DOCKER/DOJO03_DOCKER_FORENSICT.md)