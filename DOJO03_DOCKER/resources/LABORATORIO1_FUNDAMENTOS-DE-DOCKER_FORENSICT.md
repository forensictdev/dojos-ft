## **LABORATORIO 1**								

La ejecución de comandos se puede realizar de 2 maneras: Instalando Docker en el equipo o haciendo el procedimiento mediante el registro en Docker Hub.

Luego de realizado el proceso se procede a validar el correcto funcionamiento de docker ejecutando el comando: <code>docker run hello-world</code> (hello-world es una imagen que trae docker por defecto para realizar pruebas).

Si es la primera vez que ejecuta esta imagen, docker no la va a encontrar alojada en la máquina del usuario, por eso realizará el procedimiento de descargarla del registro que tiene configurado por defecto (Docker Hub) y si la instalación fue exitosa, al final mostrará el mensaje “**Hello from Docker!**” indicando que todo funciona correctamente.

**Comandos:**

Ejecutar la imagen hello-world en un contenedor y que le asigne un nombre o etiqueta (NAME) aleatoria.

```bash
docker run hello-world
```

Ejecutar la misma imagen con una etiqueta personalizada.

```bash
docker run --name hello-world_personalizada hello-world
```

Solicitar  a Docker que reenvíe todo el tráfico entrante por el puerto 81 del equipo de usuario, al puerto 80 del contenedor, ej: Si se desea visualizar una aplicación en el equipo de usuario, se escribe en el navegador **localhost:81.** (La opción <code>-d</code> hace que el contenedor se ejecute en segundo plano.)

```bash
docker run -p 81:80 -dit raesene/bwapp
```

Listar las imágenes que han sido descargadas en el equipo.

```bash
docker image ls
```

Eliminar una imagen.

```bash
docker image rm <IMAGE ID>
```

Listar los contenedores donde se están ejecutando las imágenes que han sido iniciadas.

```bash
docker ps -a
```

Iniciar un contenedor.

```bash
docker start <NAMES>
```

Ejecuta un comando (/bin/bash) desde la terminal del usuario hacia la terminal del contenedor, en este caso se va a abrir una terminal interactiva del contenedor, para finalizar se escribe el comando **exit.**

```bash
docker exec -it <NAMES> /bin/bash
```

Enlazar con la terminal del contenedor.

```bash
docker attach <NAMES>
```

Finalizar un contenedor.

```bash
docker stop <NAMES>
```

Eliminar un contenedor

```bash
docker rm <CONTAINER ID o NAMES>
```



**Registro en docker hub:**

1. Ingresar a: [https://hub.docker.com/](https://hub.docker.com/)

2. Hacer clic en el botón **sign up for Docker Hub.**

   ​                                         ![](../images/image4.png)

   ​										 	_Figura 1. Login en Docker Hub_

3. Ingresar los campos solicitados para crear la cuenta.

   ​     ![](../images/image5.png)
   
   ​															_Figura 1a. Login en Docker Hub_

**Ejecución de un contenedor:**

1. Ingresar a: [https://labs.play-with-docker.com/](https://labs.play-with-docker.com/)

   ​           ![](../images/image6.png)

   ​															_Figura 2. Página "play with Docker"_

2. Hacer clic en el botón **Login**, e ingresar con la cuenta de docker hub.

3. Al ingresar a la plataforma se tendrá un tiempo de 4 horas para practicar con Docker.

   ​				![](../images/image7.png)		

   ​										_Figura 3. Nueva instancia en "play with Docker"_


4. Crear una nueva instancia haciendo clic en:  **ADD NEW INSTANCE,** para acceder a una terminal y ejecutar contenedores de docker en la plataforma web.

5. Al crear la instancia se tendrá una terminal de un sistema operativo Linux, donde se podrá ejecutar comandos Linux y comandos de Docker.

6. Revisar si se encuentra algún contenedor en ejecución, ejecutando el comando <code>docker ps</code>.

   ![](../images/image8.png)

   ​																_Figura 4. ejecución docker ps_

7. Ejecutar un primer contenedor llamado **hello-world**, ejecutando el siguiente comando: <code>docker run hello-world</code>.

![](../images/image9.png)

   ​																_Figura 5. Ejecución docker run hello-world_

    Al ejecutar el comando anterior, docker buscará localmente la imagen llamada hello-world, al no encontrarla la buscará en docker hub, desde allí se descarga y posteriormente se ejecuta un contenedor con la imagen hello-world.

8. Al ejecutarse el contenedor imprime en pantalla la siguiente salida:

   ![](../images/image10.png)

   ​																_Figura 6. Salida del contenedor_

9. Volver a ejecutar el comando anterior, notar que esta vez la imagen no se descarga de docker hub, sino que la toma localmente, dado que ya fue descargada anteriormente.

   ![](../images/image11.png)
   
   ​																_Figura 7. Docker hello-world localmente_


10. Listar los contenedores creados, ejecutando el comando: <code>docker container list -a</code>.

    ![](../images/image12.png)

    ​																_Figura 8. Listado de contenedores_

    Nótese que por cada ejecución del comando <code>docker run hello-world</code>, se crea un nuevo contenedor.

11. Eliminar un contenedor ejecutando el comando <code>docker container rm &lt;ID del contenedor></code>.

    ![](../images/image13.png)
    
    ​																_Figura 9. Eliminación de contenedor_


12. Cree un contenedor con una etiqueta y ejecutar n veces sin que se cree un contenedor adicional. Para este caso se ejecutará una línea de comandos de un contenedor que corre un ubuntu. Todo esto se logra ejecutando el comando: <code>docker run --name linux_ubuntu -ti ubuntu</code>.

    ![](../images/image14.png)
    
    ​																_Figura 10. Contenedor de ubuntu_


13. Al salir del contenedor se puede observar que el contenedor termina su ejecución, al ejecutar el comando: <code>docker ps -a</code> se puede observar el estado del contenedor.

    ![](../images/image15.png)
    
    ​																_Figura 11. Contenedores en ejecución_


14. Para ejecutar el contenedor sin que se cree otro adicional lo hacemos con el comando: <code>docker start &lt;nombre_del_contenedor></code>.

    ![](../images/image16.png)
    
    ​																_Figura 12. Ejecución de contenedor_


15. Al iniciar un contenedor existente, este se ejecuta en background, para entrar en la terminal del contenedor se ejecuta el comando: <code>docker exec -ti linux_ubuntu /bin/bash</code>.

    ​											![](../images/image17.png)
    
    ​													_Figura 13. ingresar al terminal del contenedor_		


16. Al salir de la ejecución de la terminal del contenedor, este no termina su ejecución, sigue estando en background, para detener el contenedor es necesario ejecutar el comando <code>docker stop &lt;nombre_del_contenedor></code>.

    ![](../images/image18.png)
    
    ​																_Figura 14. Detener un contenedor_



**Redes en Docker:**

Crear una red en el segmento 10.0.0.0/24:

```bash
docker network create --subnet=10.0.0.0/24 --gateway=10.0.0.1 red_prueba
```

![](../images/image24.png)

​																_Figura 15. Red en Docker_

Cree 4 contenedores basados en la imagen alpine, dos de ellos conectados a la red de pruebas y los otro dos conectados a la red por defecto:

```bash
docker run -dti --name linux_1 --network red_prueba alpine
```

```bash
docker run -dti --name linux_2 --network red_prueba alpine
```

```bash
docker run -dti --name linux_3 alpine
```

```bash
docker run -dti --name linux_4 alpine
```

![](../images/image25.png)

​																_Figura 16. Contenedores en red_

Revisar los contenedores asociados a una red y observar el objeto **Containers**

```bash
docker network inspect <nombre_red>
```

Conectar el contenedor linux_4 a la red de pruebas:

```bash
docker network connect red_prueba linux_4
```

Ingresar al contenedor linux_4 y hacer ping al nombre de los contenedores linux_1, linux_2 y linux_3:

![](../images/image26.png)

​														_Figura 17. Verificar conexión entre contenedores_


Haga ping desde el contenedor linux_4 al linux_3 por medio de la dirección IP:

![](../images/image27.png)

​														_Figura 17a.Verificar conexión entre contenedores_


Para salir use la combinación de teclas <code>ctrl+p ctrl+q</code>

Valide que no tiene comunicación desde el contenedor linux_1 al contenedor linux_3

![](../images/image28.png)

​													_Figura 17b.Verificar conexión entre contenedores_

Ejecutar un comando en un contenedor sin entrar en él:

```bash
docker exec -i linux_1 ping -c1 linux_2
```

![](../images/image29.png)

​													_Figura 18. Ejecución de comandos remota_

**Almacenamiento en Docker:**

**Lista de comandos:**

Crea un volumen.

```bash
docker volume create <name>
```

Lista los volúmenes creados.

```bash
docker volume ls
```

Observa la información del volumen.

```bash
docker volume inspect <name>
```

Elimina el volumen.

```bash
docker volume rm <name>
```

Elimina todos los volúmenes locales que no están siendo usados por al menos un contenedor.

```bash
docker volume prune
```

​               		    ![](../images/image31.png)			

​													_Figura 19. Comandos sobre volúmenes_

Ahora vamos a crear un contenedor con un volumen asociado:

```bash
docker run -dti --name ubuntu_custom -v ubuntu_vol:/ft ubuntu
```

Con el comando anterior creamos un contenedor con una imagen de ubuntu, le asignamos un nombre personalizado (ubuntu_custom) y creamos un volumen (ubuntu_vol) el cual se aloja en la ruta **/var/lib/docker/volumes/** de la máquina del usuario y se le asigna al directorio **/ft** del contenedor.

![](../images/image32.png)

​													_Figura 20. Contenedor con volumen asociado_

Ejecutamos una terminal del contenedor para validar que si se encuentra la carpeta **ft** del volumen creado:

```bash
docker exec -ti ubuntu_custom /bin/bash
```

![](../images/image33.png)

​													_Figura 21. Ejecución de contenedor_

Inspeccionamos el contenedor para validar que el volumen fue creado y montado de manera correcta:

```bash
docker inspect ubuntu_custom
```

![](../images/image34.png)

​													_Figura 22. Inspección de contenedor_

Adicional a esto podemos crear cualquier archivo o carpeta en la ruta **/var/lib/docker/volumes/ubuntu_vol/_data** y esto lo vamos a ver reflejado de manera automática en la terminal correspondiente al contenedor.

Para finalizar vamos a crear otro contenedor con la imagen de debian y le vamos a asignar el mismo volumen creado para ubuntu en la ruta **/ft_2** del contenedor.

```bash
docker run -dti --name debian_custom -v ubuntu_vol:/ft_2 debian
```


![](../images/image35.png)

​													_Figura 23. Contenedor con volumen creado_

Como conclusión podemos observar que el contenedor correspondiente a la imagen de **ubuntu** tiene asignado el volumen **ubuntu_vol** en la carpeta **/ft** y el contenedor de **debian** tiene asignado el mismo volumen en la carpeta **/ft_2**, y así podemos continuar asignándole el volumen a todos los contenedores que se requieran.

El volumen se encuentra alojado en el equipo donde se están ejecutando los contenedores, cualquier cambio que se realice en éste se verá reflejado en los contenedores y viceversa.

![](../images/image36.png)

​													_Figura 24. Ejecución de contenedor_

#### RETO PRÁCTICO:

Un par de personas deciden personalizar una aplicación open source con el fin de probar una pequeña funcionalidad, para hacer esto uno de ellos propone utilizar docker para evitar problemas compatibilidad en librerías y dependencias de la aplicación, además, para optimizar el tiempo y no tener que configurar todo el ambiente desde cero de todo lo que la app necesita para ejecutarse. El proyecto que se desea personalizar se llama bWAPP, el cual cuenta con una gran ventaja ya que en docker hub se cuenta con la imagen del mismo.


![](../images/Reto.gif)

##### SOLUCIÓN:

**Paso 1:**

Ejecutar un contenedor usando la imagen raesene/bwapp:


```
docker run -d -p 80:80 --name bwapp raesene/bwapp
```


**Paso 2:**

Extraer el código de la aplicación del contenedor en ejecución hacia la carpeta donde se está ejecutando el comando:


```
docker cp bwapp:/app/ .
```


**Paso 3:**

Conocer la ruta de la carpeta app/ en nuestro computador local:


```
pwd
```


**Paso 4:**

Crear un nuevo contenedor pasándole como volumen compartido la carpeta _app/_ que se encuentra en nuestro sistema.


```
docker run -d -p 81:80 -v <ruta del comando anterior>:/app/ --name bwap_personalizado raesene/bwapp
```


**Paso 5:**

Modificar la cadena de texto “bWAPP” por “bWAPP Personalizado” en la línea 154 del archivo _login.php_ situado en el directorio _app/_

El archivo debe quedar de la siguiente forma:


```html
152  <header>
153 
154  <h1>bWAPP Personalizado</h1>
155  
156  <h2>an extremely buggy web app !</h2>
157 
158  </header>
```


Guardamos el archivo.

**Paso 6:**

Abrir la URL [http://localhost:81/install.php?install=yes](http://localhost:81/install.php?install=yes), dar click en la pestaña login de la aplicación web para visualizar la modificación efectuada en el **paso 5**.

![img](../images/image47.png)


​													_Figura 25. Personalización bWAPP_

### REFERENCIAS:
- https://docs.docker.com/get-started/
