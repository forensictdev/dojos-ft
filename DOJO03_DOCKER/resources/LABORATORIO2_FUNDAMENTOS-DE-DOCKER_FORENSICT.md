## **Laboratorio 2**															

##### **Crear una imagen:**

1. Para crear una imagen en docker, se debe seguir una receta, la cual básicamente son una serie de instrucciones que se deben ejecutar para construir una imagen, estas recetas son ubicadas dentro de un archivo llamado **Dockerfile.**

2. Crear una carpeta llamada ubuntu_personalizado y dentro de ella crear un archivo llamado **Dockerfile** con el siguiente texto:

   ![](../images/image19.png)
   
   ​														_Figura 1. Creación Dockerfile._


3. Al finalizar la creación del archivo debe verse lo siguiente:

   ![](../images/image20.png)
   
   ​														_Figura 1a. Validación del Dockerfile creado._


4. Para crear la imagen personalizada de Ubuntu, se ejecuta el siguiente comando <code>docker build -t ubuntu:personalizado</code>.

   ![](../images/image21.png)

   ​										_Figura 2. Creación imagen personalizada Ubuntu._

5. Para visualizar la imagen creada se puede ejecutar el comando: <code>docker images</code>, allí se debe visualizar la imagen personalizada de Ubuntu.

   ![](../images/image22.png)

   ​													_Figura 3. Comando para listar imágenes._

6. Crear un contenedor de ubuntu a partir de la imagen ubuntu:personalizado y verificar que en la ruta **/usr/src/** se encuentre un archivo llamado **ubuntu_personalizado.**

   ![](../images/image23.png)
   
   ​											_Figura 4. Creación de contenedor a partir de una imagen._

Para ampliar el tema del contexto de build, a la hora de crear una imagen con un código de una aplicación, se puede hacer uso de la documentación de docker.

[https://docs.docker.com/get-started/part2/](https://docs.docker.com/get-started/part2/)



##### **Publicar mi imagen:**

1. Lo primero que se requiere es crear una cuenta en https://hub.docker.com. 
2. Una vez se crea la cuenta, nos dirigimos a la consola de comandos y ejecutamos el siguiente comando para iniciar sesión, Se usan las credenciales (usuario y contraseña) de la cuenta creada en el **paso 1**: 

```bash
docker login
```

En este paso es necesario adecuar la imagen para que sea aprobada. En Docker Hub requerimos que el nombre de nuestra imagen presente la siguiente estructura **nombre_de_usuario/nombre_del_repositorio:etiqueta**. En este ejemplo, el nombre de la imagen es “ubuntu_personalizado”, por tanto nos debe quedar algo similar a forensict/ubuntu_personalizado:v1. Ahora usaremos el comando docker tag para generar una versión de la imagen con ese nombre.

```bash
docker tag ubuntu:personalizado forensict/ubuntu_personalizado:v1
```

**Nota:** Podemos validar mediante el comando docker images, que se ha creado una especie de alias sobre la misma imagen. 

3. Con el nombre de nuestra imagen cumpliendo la estructura requerida, procedemos a ejecutar el comando docker push para subir la imagen:

```bash
docker push forensict/ubuntu_personalizado:v1
```

4. Una vez finalizado el proceso, se puede observar en el sitio web [hub.docker.com](https://hub.docker.com/) que la imagen acaba de ser publicada.

**Nota:** El objetivo de publicar las imágenes en un repositorio es hacer uso de otro equipo, que tenga el motor de docker instalado, para ejecutar de forma ágil y eficaz este mismo contenedor. En caso de que no sea posible, podemos ejecutar estos comandos para detener todos los contenedores:

```bash
docker kill $(docker ps -q)
```

Eliminarlos:

```bash
docker rm $(docker ps -a -q)
```

y eliminar las imágenes descargadas:

```bash
docker rmi -f $(docker images -q)
```



**Docker compose**

1. Descargamos el archivo *docker-compose.yml* ejecutando el siguiente comando por consola:

```bash
curl https://gitlab.com/forensictdev/dojos-ft/-/raw/master/DOJO03_DOCKER/resources/docker-compose.yml
```

**NOTA:** Si te encuentras trabajando desde un ambiente windows puedes dirigirte directamente a este [enlace](https://gitlab.com/forensictdev/dojos-ft/-/raw/master/DOJO03_DOCKER/resources/docker-compose.yml) desde tu navegador. Posteriormente presionas click derecho y luego te diriges a la opción “*guardar como*”.

![img](../images/image40.png)

​													_Figura 5. Estructura Docker compose._

2. Crea un directorio “mi_wordpress” y aloja allí el archivo descargado en el **paso 1**.

  

```bash
mkdir mi_wordpress mv docker-compose.yml mi_wordpress/
```

3. Desde la consola de comandos, sitúate en el directorio “mi_wordpress” y ejecuta la siguiente instrucción:

```bash
docker-compose up -d
```

![img](../images/image41.png)

​													_Figura 6. Extracción de imagenes de Docker_

Con el comando *docker-compose up* se extraen las imágenes de Docker necesarias e inicia los contenedores de wordpress y base de datos, como se ilustró en la anterior imagen. El parámetro <code>-d</code> ejecuta <code>docker-compose up</code> en modo detached (es decir, que los contenedores se ejecutan en modo background)

4. De esta forma ya podemos acceder a nuestro navegador y visitar la URL http://localhost:8000 para interactuar con la instancia de wordpress que ejecutamos:

   ![img](../images/image46.png)										*Figura 7. Ingreso al servicio web de la instancia WordPress.*

   

![img](../images/image43.png)

​							*Figura 8. Acceso al panel de administración web de la instancia WordPress.*



![img](../images/image44.png)

​													*Figura 9. Configuración del sitio web WordPress.*



![img](../images/image45.png)

​													*Figura 10. Sitio web de WordPress publicado.*


### **REFERENCIAS:**

- https://docs.docker.com/get-started/

