# FUNDAMENTOS DE DOCKER  ![](images/image39.png)

<h1>¿Qué es Docker?</h1> 


Es una plataforma diseñada para el desarrollo, envío y ejecución de aplicaciones. Con docker se puede administrar la infraestructura de la misma forma que se administra la aplicación, logrando reducir el tiempo entre la escritura del código (desarrollo) y el paso a producción. Docker provee la habilidad de empaquetar y ejecutar aplicaciones en un entorno aislado llamado contenedor.

##### **¿Cuál es la diferencia entre un contenedor y una máquina virtual?**

Un contenedor se ejecuta de manera nativa en Linux y comparte el kernel (aún si hay varios contenedores allí alojados) del equipo donde se ejecuta, básicamente se comporta como cualquier otro software instalado en la máquina.

Una máquina virtual ejecuta un sistema operativo completo y tiene acceso virtual a los recursos del equipo físico donde se encuentra instalado por medio de un hipervisor generando así una carga adicional a la consumida por la lógica de la aplicación.

![](images/image1.png)

​											_Figura 1. Comparación entre Docker y Máquina Virtual_

##### **¿Cómo funciona docker?**

Docker utiliza una arquitectura basada en cliente-servidor compuesta por los siguientes componentes:

*   Un servidor encargado de ejecutar el demonio de Docker, el cual es un proceso llamado daemon, se gestiona con el comando <code>dockerd</code>.
*   Una API REST la cual especifica una interfaz que los programas pueden usar para hablar (gestionar) al **daemon** e indicarle que debe hacer.
*   Una interfaz de línea de comandos (CLI) la cual actúa como cliente y se gestiona a través del comando <code>docker</code>.
*   Un registro encargado de almacenar las imágenes de Docker que van a ser usadas por el cliente.

El cliente interactúa o da instrucciones al servidor (demonio de Docker) por medio de la API haciendo uso de comandos, además el cliente y el servidor pueden ser ejecutados en el mismo sistema o se pueden comunicar de manera remota por medio de una interfaz de red.

![](images/image2.png)

​																_Figura 2. Infraestructura de Docker_



##### **Arquitectura de Docker:**

![](images/image3.png)

​																_Figura 3. Arquitectura de Docker_

**Docker Daemon (dockerd):** Se encuentra a la espera de peticiones de la API, administra objetos de docker como: imágenes, contenedores, redes y volúmenes. Un daemon puede comunicarse con otro daemon para administrar servicios de docker.

**Docker Client (docker)**: Usa la API para enviar comandos al daemon de docker, un cliente puede comunicarse con más de un daemon.

**Docker Registries:** Almacena imágenes de docker, por defecto se usa uno público llamado **Docker Hub.**

**¿Cuáles son los principales objetos manejados por Docker?**

**Imágenes:** Son plantillas de solo lectura con instrucciones para crear un contenedor. Son basadas en otras imágenes pero con personalizaciones adicionales, ej: Es posible construir una imagen basada en la imagen original de ubuntu pero instalando Apache y cualquier aplicación que desee.

**Contenedores:** Es la instancia donde se ejecuta una imagen. Es posible crear, iniciar, parar, mover o borrar un contenedor por medio de la API o por líneas de comandos. Además, un contenedor puede ser conectado a una o más redes, unirle un volumen de almacenamiento o crear una nueva imagen basada en el estado actual del contenedor.

**Servicios:** Son los que permiten escalar contenedores a través de múltiples servidores (daemons), los cuales operan todos juntos como un enjambre con gran cantidad de administradores y trabajadores. Cada miembro del enjambre es un demonio de Docker (servidor) y todos los demonios se comunican haciendo uso de la API de Docker.

Profundicemos un poco sobre el funcionamiento de las instrucciones (comandos) en docker:

Si aún no cuentas con Docker instalado en el equipo, dirígite a la [sección de instalación](#instalaci%C3%B3n-de-docker).

```bash
docker run -it ubuntu
```

*   Si no hay una imagen de ubuntu almacenada localmente en la máquina, Docker la descarga desde el registro configurado (Docker Hub por defecto), esto equivale a haber ejecutado el comando <code>docker pull ubuntu</code> manualmente.
*   Docker crea un nuevo contenedor para que se ejecute la imagen de ubuntu, equivalente a haber ejecutado el comando <code>docker container create</code>.
*   Docker le asigna al contenedor un sistema de archivos de lectura y escritura como capa final. Esto le permite al contenedor crear o modificar archivos y directorios en su propio sistema de archivos.
*   Docker crea una interfaz de red para conectar el contenedor a la red configurada por defecto (esto sucede si no se especifica ninguna opción de red), asignarle una dirección ip y así poderse conectar a redes externas por medio del equipo donde se está ejecutando.
*   Docker inicia el contenedor y ejecuta una terminal del docker en la terminal del usuario (/bin/bash), esto gracias a las opciones **-i (interactivo) y -t (vincular a la terminal tty del usuario).**



##### **Redes en Docker:**

Docker provee conectividad de red a través de drivers.

**Bridge:** Es el driver por defecto, si no especifica uno este es el driver usado al ejecutar un contenedor.

**Host:** Usa la red directamente del host donde corre el contenedor.

**Overlay:** Conectan múltiples demonios Docker (daemon) y habilita el servicio swarm para comunicarse con cada uno.

**Macvlan:** Permite asignar una dirección MAC al contenedor haciendo que esta aparezca como un dispositivo físico en la red.

**None:** Deshabilita toda la red del contenedor.

**Diferencias entre un bridge definido por el usuario y el bridge por defecto:**

Los bridge definidos por el usuario proveen resolución DNS entre contendores automáticamente (comunicación de red a través del nombre del contenedor), en los bridge por defecto se debe especificar la dirección IP o usar el atributo **--link** y configurar el archivo **/etc/hosts.**



##### **Almacenamiento en Docker:**

Por defecto, todos los archivos que se crean en un contenedor son almacenados en una capa grabable del mismo, lo que significa que:

*   Los datos se eliminan una vez el contenedor deja de existir y no es una tarea fácil sacar los datos almacenados dentro del contenedor.
*   La capa grabable de un contenedor está acoplada a la máquina del usuario y es complicado mover datos a otro lugar.
*   Escribir dentro de una capa grabable de un contenedor requiere el uso de un driver de almacenamiento para administrar el sistema de archivos.

Docker cuenta con opciones para que los contenedores almacenen archivos en el equipos donde se están ejecutando y así estos se conserven cuando el contenedor no esté en ejecución: Volúmenes y montajes de enlace.

* **Volúmenes:** Son almacenados en una parte del sistema de archivos del equipo que es administrado por Docker: **/var/lib/docker/volumes/.** Esta parte del sistema de archivos no puede ser modificada por otros procesos que no correspondan a Docker.

* **Montajes de enlace:** Pueden ser almacenados en cualquier ubicación del sistema de archivos y además pueden ser modificados por otros procesos que no correspondan a Docker.

* **tmpfs:** No es muy recomendado ya que se almacena únicamente en la memoria del dispositivo y no en el sistema de archivos del mismo.

    ![](images/image30.png) 
  
  ​	_Figura 4. Esquema de almacenamiento en Docker_

Un volumen siempre va a estar disponible inclusive si un contenedor no se está ejecutando, además un volumen puede ser montado en múltiples contenedores al tiempo.

##### **Dockerfile**

Docker puede crear imágenes automáticamente leyendo las instrucciones de un archivo Dockerfile. Un Dockerfile es un documento de texto que contiene todos los comandos que un usuario puede llamar en la línea de comandos para ensamblar una imagen. Usando docker build los usuarios pueden crear una compilación automatizada que ejecute varias instrucciones de línea de comandos en sucesión.

El formato de un Dockerfile es bastante sencillo, consta de lo siguiente estructura:

```bash
# Comment
INSTRUCTION arguments
```

Para comentar se hace uso del caracter numeral (#) al inicio de la línea y todo lo que va allí no será procesado por el intérprete. La parte de **INSTRUCTION**, hace referencia a las instrucciones propias que contiene y son interpretadas por docker, algunas de ellas son *FROM*, *RUN*, *COPY*, etc. Por último, la parte de **arguments**, hace mención al contenido específico que va a realizar la instrucción definida, puede ser la mención de la imagen a utilizar o la ejecución de un comando, hay que tener muy presente la instrucción indicada ya que de esta depende los argumentos a pasar.

A continuación se muestra un ejemplo de un archivo Dockerfile.

![](images/image37.png)

​															_Figura 5. Ejemplo de Dockerfile_

##### **Docker compose**

Compose es una herramienta para definir y correr aplicaciones de docker multicontainers, con esta herramienta, se configuran los servicios de la aplicación haciendo uso de un archivo YAML (el cual es un formato para guardar objetos de datos con estructura de árbol). Luego, solo con un comando, se crea y se inician todos los servicios desde la configuración.

Usar Compose es básicamente un proceso de tres pasos:

1. Defina el entorno de su aplicación con un Dockerfile para que pueda reproducirse en cualquier lugar.
2. Defina los servicios que componen su aplicación en docker-compose.yml para que puedan ejecutarse juntos en un entorno aislado.
3. Ejecute docker-compose up y Compose inicia y ejecuta toda su aplicación.



Un docker-compose.yml tiene la siguiente estructura:

![](images/image38.png)																

_Figura 5. Ejemplo de Dockercompose_



#### **Instalación de Docker**

**Linux**

Para la instalación de docker en linux procedemos a ejecutar los siguientes comandos por consola:

1. Desinstalación de versiones antiguas de docker.

   ```bash
   $ sudo apt-get remove docker docker-engine docker.io containerd runc
   ```

2. Agregar los repositorios oficiales para la instalación de docker.

   ```bash
   $ sudo apt-get update
   
   $ sudo apt-get install \
       apt-transport-https \
       ca-certificates \
       curl \
       gnupg-agent \
       software-properties-common
   ```

3. Agregar la clave GPG oficial de docker.

   ```bash
   $ curl -fsSL https://download.docker.com/linux/debian/gpg | sudo apt-key add -
   ```

4. Verificar que ahora se tiene la clave con la huella digital 9DC8 5822 9FC7 DD38 854A E2D8 8D81 803C 0EBF CD88, buscando los últimos 8 caracteres de la huella digital.

   ```bash
   $ sudo apt-key fingerprint 0EBFCD88
   ```

5. Agregar el repositorio estable de docker.

   ```bash
   $ sudo add-apt-repository \
      "deb [arch=amd64] https://download.docker.com/linux/debian buster stable"
   ```

6. Finalmente se hace la instalación de docker.

   ```bash
   $ sudo apt-get update
   $ sudo apt-get install docker-ce docker-ce-cli containerd.io
   ```

7. Verificar la instalación de docker.

   ```bash
   $ sudo docker run hello-world
   ```

**Windows**

Para la instalación de docker en Windows descargamos el instalador en el siguiente enlace:

[Docker Desktop Windows Installer](https://desktop.docker.com/win/stable/Docker%20Desktop%20Installer.exe) 

**Mac**

Para la instalación de docker en macOS descargamos el instalador en el siguiente enlace:

[Docker Desktop Mac Installer](https://desktop.docker.com/mac/stable/Docker.dmg) 


>**Nota:** La instalación de Docker Desktop tanto en Windows como Mac incluye Docker Engine, Docker CLI Client, Docker Compose, Notary, Kubernetes y Credential Helper.

### LABORATORIOS

- [LABORATORIO 1](resources/LABORATORIO1_FUNDAMENTOS-DE-DOCKER_FORENSICT.md)
- [LABORATORIO 2](resources/LABORATORIO2_FUNDAMENTOS-DE-DOCKER_FORENSICT.md)

### REFERENCIAS:
- https://docs.docker.com/get-started/